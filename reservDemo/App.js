import 'react-native-gesture-handler';
import 'react-native-get-random-values';
import React, {Fragment, useEffect} from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

import {Platform, StatusBar} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import HomeScreen from './component/HomeScreen';
import HomeScreenDecider from './component/HomeComponent/HomeScreenDecider';
import CloudScreenDecider from './component/CloudComponent/CloudScreenDecider';
import UsageScreen from './component/CloudComponent/UsageScreen';
import ActiveClientScreen from './component/CloudComponent/ActiveClientScreen';
import RealTimeDwnUpdScreen from './component/CloudComponent/RealTimeDwnUpdScreen';
import StatScreen from './component/CloudComponent/StatScreen';

const Stack = createStackNavigator();

const initialState = {
  action: 'closeMenu',
  name: '',
  service: '',
  modalName: '',
  modalNameCld: '',
};

const reducer = (state = initialState, action) => {
  console.log('reducer action = ', action);
  switch (action.type) {
    case 'OPEN_MENU':
      return {
        action: 'openMenu',
        name: action.mode,
      };

    case 'CLOSE_MENU':
      return {
        action: 'closeMenu',
      };

    case 'UPDATE_NAME':
      return {
        name: action.name,
      };

    case 'SERVICE_UPDATE':
      return {
        service: action.service,
      };

    case 'OPEN_MODAL':
      return {
        action: 'openModal',
        modalName: action.mode,
      };

    case 'CLOSE_MODAL':
      return {
        action: 'closeModal',
      };

    case 'OPEN_MODAL_CLD':
      return {
        action: 'openModalCld',
        modalNameCld: action.mode,
      };

    case 'CLOSE_MODAL_CLD':
      return {
        action: 'closeModalCld',
      };

    default:
      return state;
  }
};

const store = createStore(reducer);

const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Fragment>
      {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
      <Provider store={store}>
        {/* <HomeScreen></HomeScreen> */}
        {/* <HomeScreenDecider></HomeScreenDecider> */}
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name="Home" component={HomeScreenDecider} />
            <Stack.Screen name="Cloud" component={CloudScreenDecider} />
            <Stack.Screen name="Usage" component={UsageScreen} />
            <Stack.Screen name="ActiveClients" component={ActiveClientScreen} />
            <Stack.Screen name="RealTime" component={RealTimeDwnUpdScreen} />
            <Stack.Screen name="Stats" component={StatScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </Fragment>
  );
};

export default App;

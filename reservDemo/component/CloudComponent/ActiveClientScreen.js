import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  LayoutAnimation,
  ScrollView,
  TextInput,
  UIManager,
  RefreshControl,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import Modal from 'react-native-modal';

import {
  Grid,
  StackedBarChart,
  YAxis,
  XAxis,
  ProgressCircle,
} from 'react-native-svg-charts';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const {ServicesConst, logoConst} = require('../../constant/Model');

var activeClientDataObjArr = [];

class ActiveClientScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    const {token, deviceID, activeClients} = this.props.route.params;
    // console.log(
    //   'constructor ActiveClientScreen.js activeClients = ',
    //   activeClients,
    // );

    if (Platform.OS === 'android') {
      if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
    }

    this.state = {
      activeClientsDataIdentified: [],
      activeClientsDataUnIdentified: [],
      layoutHeightIdentified: null,
      layoutHeightUnIdentified: null,
      identifiedlayoutHeightarr: [],
      unidentifiedlayoutHeightarr: [],
      isloading: false,
      sqlData: {},
      sdnb: {},
      sunb: {},
      isModalVisible: false,
      modalName: '',
      modalValue: {},
      clientNameEditError: '',
      clientNameEdit: '',
      refreshing: false,
    };

    this.getdataFromAsyncStorage(activeClients);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount ActiveClientScreen.js');
  }

  refresh = () => {
    console.log('ActiveClientScreen refresh');
    this.setState({
      refreshing: true,
    });

    this.callGetUserAPI(this.props.route.params.token);
  };

  callGetUserAPI = (token) => {
    console.log('callGetUserAPI');
    var myHeaders = new Headers();
    myHeaders.append('jwt-token', token);
    var raw = '';
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    var timeOut;
    timeOut = setTimeout(() => {
      this.setState({
        refreshing: false,
      });
    }, 8000);

    fetch('https://rezrvwebapi.tantiv4.com/users', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log('callGetUser result = ', result);
        var resultJSON = JSON.parse(result);
        if (resultJSON && resultJSON.device) {
          console.log('callGetUserAPI resultJSON success ');
          clearTimeout(timeOut);
          this.setState({
            refreshing: false,
          });
          this.getdataFromAsyncStorage(resultJSON.device.activeClients);
        } else {
          clearTimeout(timeOut);
          this.setState({
            refreshing: false,
          });
        }
      })
      .catch((error) => {
        console.log('callGetUserAPI error', error);
        clearTimeout(timeOut);
        this.setState({
          refreshing: false,
        });
      });
  };

  getdataFromAsyncStorage = (activeClientList) => {
    (async () => {
      console.log(
        'getdataFromAsyncStorage activeClientList = ',
        activeClientList,
      );
      var jsonValue = await AsyncStorage.getItem('@storage_clients');

      if (jsonValue !== null) {
        // console.log('jsonValue = ', jsonValue);
        // await AsyncStorage.removeItem('@storage_clients');

        // console.log('jsonValue !== null');
        var asyncClient = JSON.parse(jsonValue);
        // console.log('asyncClient = ', asyncClient);
        // console.log('asyncClient = ', asyncClient.length);

        var actCliListObj = [];
        for (i = 0; i < activeClientList.length; i++) {
          actCliListObj.push({
            clientIP: activeClientList[i],
            found: false,
          });
        }

        // console.log('actCliListObj = ', actCliListObj);

        var filterFoundClient = asyncClient.filter((obj) => {
          for (i = 0; i < actCliListObj.length; i++) {
            if (actCliListObj[i].clientIP === obj.clientIP) {
              actCliListObj[i].found = true;
              return obj;
            }
          }
        });

        // console.log('filterFoundClient = ', filterFoundClient);
        // console.log('actCliListObj = ', actCliListObj);

        var filterIdentified = filterFoundClient.filter((obj) => {
          if (obj.identified === true) {
            return obj;
          }
        });

        var filterUnIdentified = filterFoundClient.filter((obj) => {
          if (obj.identified === false) {
            return obj;
          }
        });

        for (i = 0; i < actCliListObj.length; i++) {
          if (actCliListObj[i].found === false) {
            var objPush = {
              clientIP: actCliListObj[i].clientIP,
              name: actCliListObj[i].clientIP,
              identified: false,
              expanded: false,
            };
            filterUnIdentified.push(objPush);
            asyncClient.push(objPush);
          }
        }

        var heightidentified = [];
        for (i = 0; i < filterIdentified.length; i++) heightidentified[i] = 0;

        var heightunidentified = [];
        for (i = 0; i < filterUnIdentified.length; i++)
          heightunidentified[i] = 0;

        this.setState({
          activeClientsDataIdentified: filterIdentified,
          activeClientsDataUnIdentified: filterUnIdentified,
          identifiedlayoutHeightarr: heightidentified,
          unidentifiedlayoutHeightarr: heightunidentified,
        });

        var myJSON = JSON.stringify(asyncClient);
        // console.log('get is NOT null so myJSON = ', myJSON);
        await AsyncStorage.setItem('@storage_clients', myJSON);
      } else {
        console.log('jsonValue === null');
        var unidentified = [];
        var heightunidentified = [];
        for (i = 0; i < activeClientList.length; i++) {
          unidentified.push({
            clientIP: activeClientList[i],
            name: activeClientList[i],
            identified: false,
            expanded: false,
          });
          heightunidentified.push(0);
        }

        this.setState({
          activeClientsDataUnIdentified: unidentified,
          unidentifiedlayoutHeightarr: heightunidentified,
        });

        var myJSON = JSON.stringify(unidentified);
        // console.log('get is null so, myJSON = ', myJSON);
        await AsyncStorage.setItem('@storage_clients', myJSON);
      }
    })();
  };

  updateDataAyncStorage = (clientObj) => {
    (async () => {
      console.log('updateDataAyncStorage clientObj = ', clientObj);
      var jsonValue = await AsyncStorage.getItem('@storage_clients');
      if (jsonValue != null) {
        var asyncClient = JSON.parse(jsonValue);
        // console.log('updateDataAyncStorage asyncClient = ', asyncClient);

        var newState = asyncClient.map((obj) =>
          obj.clientIP === clientObj.clientIP ? clientObj : obj,
        );
        asyncClient = newState;

        // console.log('updateDataAyncStorage asyncClient after= ', asyncClient);
        var myJSON = JSON.stringify(asyncClient);
        await AsyncStorage.setItem('@storage_clients', myJSON);
      }
    })();
  };

  updateLayoutisIdentified = (value) => {
    LayoutAnimation.configureNext({
      duration: 300,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.easeInEaseOut,
      },
    });

    if (value === 'trusted') {
      if (this.state.layoutHeightIdentified === 0) {
        this.setState({
          layoutHeightIdentified: null,
        });
      } else {
        this.setState({
          layoutHeightIdentified: 0,
        });
      }
    } else {
      if (this.state.layoutHeightUnIdentified === 0) {
        this.setState({
          layoutHeightUnIdentified: null,
        });
      } else {
        this.setState({
          layoutHeightUnIdentified: 0,
        });
      }
    }
  };

  updateLayoutUnIdentifed = (index) => {
    LayoutAnimation.configureNext({
      duration: 300,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.easeInEaseOut,
      },
    });

    const array = [...this.state.activeClientsDataUnIdentified];
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['expanded'] = !array[placeindex]['expanded'])
        : (array[placeindex]['expanded'] = false),
    );

    this.setState(() => {
      return {
        activeClientsDataUnIdentified: array,
      };
    });
    // console.log('finallayoutheight = ', finallayoutheight);

    if (array[index].expanded) {
      var height = this.state.unidentifiedlayoutHeightarr;
      for (i = 0; i < height.length; i++) {
        if (i == index) {
          height[i] = null;
        } else {
          height[i] = 0;
        }
      }

      var height1 = this.state.identifiedlayoutHeightarr;
      for (i = 0; i < height1.length; i++) {
        height1[i] = 0;
      }

      this.setState({
        isloading: true,
        unidentifiedlayoutHeightarr: height,
        identifiedlayoutHeightarr: height1,
      });

      this.callUsageAPI(
        array[index].clientIP,
        this.props.route.params.deviceID,
        this.props.route.params.token,
      );
    } else {
      var height = this.state.unidentifiedlayoutHeightarr;
      for (i = 0; i < height.length; i++) {
        if (i == index) {
          height[i] = 0;
        }
      }
      this.setState({
        unidentifiedlayoutHeightarr: height,
      });
    }
  };

  updateLayoutIdentifed = (index) => {
    LayoutAnimation.configureNext({
      duration: 300,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity,
      },
      update: {
        type: LayoutAnimation.Types.easeInEaseOut,
      },
    });

    const array = [...this.state.activeClientsDataIdentified];
    array.map((value, placeindex) =>
      placeindex === index
        ? (array[placeindex]['expanded'] = !array[placeindex]['expanded'])
        : (array[placeindex]['expanded'] = false),
    );

    this.setState(() => {
      return {
        activeClientsDataIdentified: array,
      };
    });
    console.log('updateLayoutIdentifed = ');

    if (array[index].expanded) {
      var height = this.state.identifiedlayoutHeightarr;
      for (i = 0; i < height.length; i++) {
        if (i == index) {
          height[i] = null;
        } else {
          height[i] = 0;
        }
      }

      var height1 = this.state.unidentifiedlayoutHeightarr;
      for (i = 0; i < height1.length; i++) {
        height1[i] = 0;
      }

      this.setState({
        isloading: true,
        identifiedlayoutHeightarr: height,
        unidentifiedlayoutHeightarr: height1,
      });

      this.callUsageAPI(
        array[index].clientIP,
        this.props.route.params.deviceID,
        this.props.route.params.token,
      );
    } else {
      var height = this.state.identifiedlayoutHeightarr;
      for (i = 0; i < height.length; i++) {
        if (i == index) {
          height[i] = 0;
        }
      }

      this.setState({
        identifiedlayoutHeightarr: height,
      });
    }
  };

  callUsageAPI = (ClientIp, DeviceId, token) => {
    console.log('callUsageAPI');
    var myHeaders = new Headers();
    myHeaders.append('jwt-token', token);
    myHeaders.append('time-offset', new Date().getTimezoneOffset() * -1);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    };

    fetch(
      'https://rezrvwebapi.tantiv4.com/modes/usage-per-day?deviceId=' +
        DeviceId +
        '&clientIp=' +
        ClientIp,
      requestOptions,
    )
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var resultJSON = JSON.parse(result);
        // console.log('resultJSON = ', resultJSON.sdnb);
        if (resultJSON && resultJSON.sqlData) {
          // console.log('resultJSON = ', resultJSON.sqlData);
          this.setState({
            isloading: false,
            sqlData: resultJSON.sqlData,
            sdnb: resultJSON.sdnb,
            sunb: resultJSON.sunb,
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
        this.setState({
          isloading: false,
          sqlData: {},
          sdnb: {},
          sunb: {},
        });
      });
  };

  ArrayTotalCalc = (ObjectParse) => {
    // console.log('sunbArrayTotalCalc sunb = ', ObjectParse);
    var valuesxyz = Object.entries(ObjectParse);
    var keys = Object.keys(ObjectParse);
    var finalArryObj = [];
    for (i = 0; i < keys.length; i++) {
      var obj = valuesxyz[i][1];
      finalArryObj.push(obj);
    }

    var serviceArr_legends = [];
    serviceArr_legends = finalArryObj.map((a) => {
      return Object.keys(a);
    });
    var extra = [];
    for (i = 0; i < serviceArr_legends.length; i++) {
      for (j = 0; j < serviceArr_legends[i].length; j++) {
        extra.push(serviceArr_legends[i][j]);
      }
    }
    var uniqueArray = [];
    for (var value of extra) {
      if (uniqueArray.indexOf(value) === -1) {
        uniqueArray.push(value);
      }
    }
    serviceArr_legends = uniqueArray.filter(
      (a) => a !== 'Other' && a !== 'date',
    );

    var sendData = [];
    for (i = 0; i < serviceArr_legends.length; i++) {
      var values = finalArryObj.map((value) => {
        if (value[serviceArr_legends[i]] === undefined) {
          return 0;
        } else {
          return parseInt(value[serviceArr_legends[i]]);
        }
      });

      var totalval = values.reduce((total, num) => {
        return total + num;
      });

      sendData.push({
        name: serviceArr_legends[i],
        total: totalval,
      });
    }

    return sendData;
  };

  calculateDataForGraph = (isloading, sqlData, sdnb, sunb) => {
    console.log('isloading = ', isloading);
    // console.log('sunb = ', sunb);
    // console.log('sdnb = ', sdnb);

    var uploadData = this.ArrayTotalCalc(sunb);
    // console.log('uploadData = ', uploadData);
    var downloadData = this.ArrayTotalCalc(sdnb);
    // console.log('downloadData = ', downloadData);

    var valuesxyz = Object.entries(sqlData);
    var keys = Object.keys(sqlData);
    var finalArryObj = [];
    for (i = 0; i < keys.length; i++) {
      var obj = valuesxyz[i][1];
      obj.date = keys[i];
      finalArryObj.push(obj);
    }

    for (var x = 0; x < finalArryObj.length; x++) {
      for (i = 0; i < ServicesConst.HomeModeService.length; i++) {
        if (
          finalArryObj[x].hasOwnProperty(ServicesConst.HomeModeService[i].name)
        ) {
          finalArryObj[x][ServicesConst.HomeModeService[i].name] = parseFloat(
            finalArryObj[x][ServicesConst.HomeModeService[i].name],
          );
        }
      }

      for (i = 0; i < ServicesConst.OfficeModeService.length; i++) {
        if (
          finalArryObj[x].hasOwnProperty(
            ServicesConst.OfficeModeService[i].name,
          )
        ) {
          finalArryObj[x][ServicesConst.OfficeModeService[i].name] = parseFloat(
            finalArryObj[x][ServicesConst.OfficeModeService[i].name],
          );
        }
      }
    }

    var dateArr_label = [];
    var serviceArr_legends = [];
    var ArrayData2Dim = [];
    var barColorArray = [];

    if (finalArryObj.length > 0) {
      dateArr_label = finalArryObj.map((a) => a.date);
      dateArr_label = dateArr_label.map((str) => {
        return str.substring(5);
      });

      serviceArr_legends = finalArryObj.map((a) => {
        return Object.keys(a);
      });
      var extra = [];
      for (i = 0; i < serviceArr_legends.length; i++) {
        for (j = 0; j < serviceArr_legends[i].length; j++) {
          extra.push(serviceArr_legends[i][j]);
        }
      }
      var uniqueArray = [];
      // Loop through array values
      for (var value of extra) {
        if (uniqueArray.indexOf(value) === -1) {
          uniqueArray.push(value);
        }
      }
      serviceArr_legends = uniqueArray.filter(
        (a) =>
          a !== 'Other' &&
          a !== 'date' &&
          a !== 'Iperf3_Client_1' &&
          a !== 'Iperf3_Client_2',
      );

      barColorArray = serviceArr_legends.map((a) => {
        if (policyColors[a] === undefined) {
          return policyColors['Other'];
        } else {
          return policyColors[a];
        }
      });

      for (i = 0; i < finalArryObj.length; i++) {
        for (j = 0; j < serviceArr_legends.length; j++) {
          if (finalArryObj[i].hasOwnProperty(serviceArr_legends[j])) {
            //Nothing
          } else {
            finalArryObj[i][serviceArr_legends[j]] = 0;
          }
        }
      }

      for (i = 0; i < dateArr_label.length; i++) {
        var temp = [];
        for (j = 0; j < serviceArr_legends.length; j++) {
          var val;
          if (finalArryObj[i][serviceArr_legends[j]]) {
            val = finalArryObj[i][serviceArr_legends[j]];
            val = parseFloat(val);
          } else {
            val = 0;
          }

          temp.push(val);
        }
        ArrayData2Dim.push(temp);
      }
    }

    var totalTimeService = [];
    for (i = 0; i < serviceArr_legends.length; i++) {
      totalTimeService.push({
        name: serviceArr_legends[i],
        total: 0.0,
      });
    }

    for (i = 0; i < finalArryObj.length; i++) {
      for (j = 0; j < totalTimeService.length; j++) {
        if (finalArryObj[i].hasOwnProperty(totalTimeService[j].name)) {
          totalTimeService[j].total =
            totalTimeService[j].total +
            finalArryObj[i][totalTimeService[j].name];
        }
      }
    }

    var maxrowCheck = [];
    for (i = 0; i < ArrayData2Dim.length; i++) {
      maxrowCheck[i] = ArrayData2Dim[i].reduce((total, num) => total + num);
    }
    var max = Math.max.apply(null, maxrowCheck);

    var maxvaluearray = [0, max];

    return {
      finalArryObj: finalArryObj,
      barColorArray: barColorArray,
      serviceArr_legends: serviceArr_legends,
      totalTimeService: totalTimeService,
      dateArr_label: dateArr_label,
      maxvaluearray: maxvaluearray,
      uploadData: uploadData,
      downloadData: downloadData,
    };
  };

  openModalinClient = (val, cliObj) => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      modalName: val,
      modalValue: cliObj,
    });
  };

  updateClientinStateUnidentify = () => {
    var cliObj = this.state.modalValue;

    var unidentifiedclient = [];
    var identifiedclient = [];

    var unidentifiedclient = [...this.state.activeClientsDataUnIdentified];
    var identifiedclient = [...this.state.activeClientsDataIdentified];
    // console.log('unidentifiedclient = ', unidentifiedclient);
    // console.log('identifiedclient = ', identifiedclient);
    var obj = identifiedclient.find((obj) => {
      return obj.clientIP === cliObj.clientIP;
    });
    // console.log('obj = ', obj);
    obj.expanded = false;
    obj.identified = false;
    obj.name = obj.clientIP;
    identifiedclient = identifiedclient.filter(
      (item) => item.clientIP !== cliObj.clientIP,
    );
    unidentifiedclient.push(obj);

    // console.log('unidentifiedclient after= ', unidentifiedclient);
    // console.log('identifiedclient after= ', identifiedclient);

    var heightIdentified = [];
    for (i = 0; i < identifiedclient.length; i++) heightIdentified[i] = 0;
    var heightUnidentified = [];
    for (i = 0; i < unidentifiedclient.length; i++) heightUnidentified[i] = 0;

    this.setState({
      activeClientsDataUnIdentified: unidentifiedclient,
      activeClientsDataIdentified: identifiedclient,
      identifiedlayoutHeightarr: heightIdentified,
      unidentifiedlayoutHeightarr: heightUnidentified,
      // layoutHeightIdentified: 0,
      // layoutHeightUnIdentified: 0,
      isModalVisible: !this.state.isModalVisible,
      modalName: '',
      modalValue: {},
    });

    this.updateDataAyncStorage(obj);
  };

  updateClientinState = () => {
    // console.log('openModalinClient val = ', val, cliObj);

    var cliObj = this.state.modalValue;

    var unidentifiedclient = [];
    var identifiedclient = [];

    var unidentifiedclient = [...this.state.activeClientsDataUnIdentified];
    var identifiedclient = [...this.state.activeClientsDataIdentified];
    // console.log('unidentifiedclient = ', unidentifiedclient);
    // console.log('identifiedclient = ', identifiedclient);
    var obj = unidentifiedclient.find((obj) => {
      return obj.clientIP === cliObj.clientIP;
    });
    // console.log('obj = ', obj);
    obj.expanded = false;
    obj.identified = true;
    unidentifiedclient = unidentifiedclient.filter(
      (item) => item.clientIP !== cliObj.clientIP,
    );
    identifiedclient.push(obj);

    // console.log('unidentifiedclient after= ', unidentifiedclient);
    // console.log('identifiedclient after= ', identifiedclient);

    var heightIdentified = [];
    for (i = 0; i < identifiedclient.length; i++) heightIdentified[i] = 0;
    var heightUnidentified = [];
    for (i = 0; i < unidentifiedclient.length; i++) heightUnidentified[i] = 0;

    this.setState({
      activeClientsDataUnIdentified: unidentifiedclient,
      activeClientsDataIdentified: identifiedclient,
      identifiedlayoutHeightarr: heightIdentified,
      unidentifiedlayoutHeightarr: heightUnidentified,
      // layoutHeightIdentified: 0,
      // layoutHeightUnIdentified: 0,
      isModalVisible: !this.state.isModalVisible,
      modalName: '',
      modalValue: {},
    });

    this.updateDataAyncStorage(obj);
  };

  handlePressOut = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      modalName: '',
      modalValue: {},
    });
  };

  handlePressOutSub = () => {};

  clientNameCheck = (val) => {
    this.setState({clientNameEdit: val});
  };

  toggleModal = (value) => {
    if (value === 'cancel') {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
        modalName: '',
        modalValue: {},
      });
    }

    if (value === 'OK') {
      this.updateClientinState();
    }

    if (value === 'unidentify') {
      this.updateClientinStateUnidentify();
    }

    if (value === 'save') {
      if (
        this.state.clientNameEdit === '' ||
        this.state.clientNameEdit.length === 0
      ) {
        this.setState({
          clientNameEditError: 'Name is Empty',
        });
      } else if (this.state.clientNameEdit.length > 15) {
        this.setState({
          clientNameEditError: 'Name length must be less than 15',
        });
      } else {
        var identifiedclient = [...this.state.activeClientsDataIdentified];
        var cliObj = this.state.modalValue;

        var identifiedclient = identifiedclient.map((obj) =>
          obj.clientIP === cliObj.clientIP
            ? {...obj, name: this.state.clientNameEdit}
            : obj,
        );

        this.updateDataAyncStorage({
          clientIP: cliObj.clientIP,
          expanded: false,
          identified: cliObj.identified,
          name: this.state.clientNameEdit,
        });

        this.setState({
          isModalVisible: !this.state.isModalVisible,
          modalName: '',
          modalValue: {},
          activeClientsDataIdentified: identifiedclient,
          clientNameEdit: '',
          clientNameEditError: '',
        });
      }
    }
  };

  render() {
    var isloading = this.state.isloading;
    var sqlData = this.state.sqlData;
    var sdnb = this.state.sdnb;
    var sunb = this.state.sunb;
    // console.log('sqlData render = ', sqlData);

    var {
      finalArryObj,
      barColorArray,
      serviceArr_legends,
      totalTimeService,
      dateArr_label,
      maxvaluearray,
      uploadData,
      downloadData,
    } = this.calculateDataForGraph(isloading, sqlData, sdnb, sunb);

    return (
      <View style={styles.container}>
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            {this.state.modalName === 'Identify' ? (
              <View
                onStartShouldSetResponder={(evt) => true}
                onResponderGrant={this.handlePressOutSub}
                style={styles.settingModalSubView}>
                <Image
                  source={logoConst['box2_black']}
                  style={styles.logo_image}
                />
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>
                  Are you sure ?
                </Text>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.modalValue.clientIP}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Cancel
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.toggleModal('OK')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Trust
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View
                onStartShouldSetResponder={(evt) => true}
                onResponderGrant={this.handlePressOutSub}
                style={styles.settingModalSubView}>
                <View
                  style={{
                    position: 'absolute',
                    top: Math.round(screenHeight * 0.01),
                    right: Math.round(screenWidth * 0.01),
                  }}>
                  <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                    <Image
                      style={styles.logo_image_close}
                      source={logoConst['close']}
                    />
                  </TouchableOpacity>
                </View>

                <Image
                  source={logoConst['box2_black']}
                  style={styles.logo_image}
                />
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>
                  Edit name for client
                </Text>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  {this.state.modalValue.clientIP}
                </Text>
                <TextInput
                  style={styles.ModalInput}
                  underlineColorAndroid="transparent"
                  placeholder="Name"
                  placeholderTextColor="#9a73ef"
                  autoCapitalize="none"
                  textAlign={'center'}
                  keyboardType={'email-address'}
                  onChangeText={this.clientNameCheck}
                  value={this.state.clientNameEdit}
                />
                {this.state.clientNameEditError !== '' ? (
                  <Text style={{color: 'red'}}>
                    {this.state.clientNameEditError}
                  </Text>
                ) : (
                  <Text style={{display: 'none'}}></Text>
                )}
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity onPress={() => this.toggleModal('save')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Save
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.toggleModal('unidentify')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Don't Trust
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        </Modal>
        <View style={styles.headingView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Cloud')}>
            <Image
              source={logoConst['arrow']}
              style={styles.logo_image_header}></Image>
          </TouchableOpacity>
          <Text style={{fontSize: 32, color: 'white'}}>Active Clients</Text>
          {/* <View></View> */}
          <Image
            source={logoConst['blackArrow']}
            style={[styles.logo_image_header]}></Image>
        </View>

        <View style={styles.mainView}>
          {/* <Text style={{fontSize: 24, color: 'white'}}>
            Real Time Data Screen
          </Text> */}
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.refresh}
              />
            }
            style={{
              width: screenWidth,
              marginTop: Math.round(screenHeight * 0.05),
            }}>
            <View style={styles.header}>
              <TouchableOpacity
                onPress={() => this.updateLayoutisIdentified('trusted')}>
                <Text style={{fontSize: 26, color: 'white'}}>
                  Trusted Clients (
                  {this.state.activeClientsDataIdentified.length})
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  height: this.state.layoutHeightIdentified,
                  // height: null,
                  overflow: 'hidden',
                }}>
                {this.state.activeClientsDataIdentified.map((item, index) => (
                  <View key={index} style={styles.headerInner}>
                    {/*Header of the Expandable List Item*/}
                    <TouchableOpacity
                      onPress={() => this.updateLayoutIdentifed(index)}>
                      <View
                        style={{
                          // borderWidth: 1,
                          // borderColor: 'white',
                          width: Math.round(screenWidth * 0.94),
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Image
                            source={logoConst['laptop']}
                            style={[
                              styles.logo_image_header,
                              {marginRight: Math.round(screenWidth * 0.02)},
                            ]}></Image>
                          <Text
                            style={{
                              fontSize: 22,
                              color: 'white',
                              alignSelf: 'center',
                            }}>
                            {item.name}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <TouchableOpacity
                            onPress={() =>
                              this.openModalinClient('Edit', item)
                            }>
                            <View style={styles.buttonView}>
                              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                                Edit
                              </Text>
                            </View>
                          </TouchableOpacity>
                          {this.state.identifiedlayoutHeightarr[index] === 0 ? (
                            <Image
                              source={logoConst['right']}
                              style={styles.logo_image_arrow}></Image>
                          ) : (
                            <Image
                              source={logoConst['down']}
                              style={styles.logo_image_arrow}></Image>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        height: this.state.identifiedlayoutHeightarr[index],
                        overflow: 'hidden',
                      }}>
                      {isloading ? (
                        <View style={styles.displaytmpView}>
                          <Image
                            source={logoConst['spinner']}
                            style={styles.logo_image_header}></Image>
                        </View>
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'white',
                            width: Math.round(screenWidth * 0.95),
                            borderRadius: Math.round(screenHeight * 0.01),
                            marginVertical: Math.round(screenHeight * 0.01),
                          }}>
                          {serviceArr_legends.length === 0 ? (
                            <View style={styles.displaytmpView}>
                              <Text style={{color: 'black', fontSize: 20}}>
                                No data available
                              </Text>
                            </View>
                          ) : (
                            <StackedBarChartExample
                              data={finalArryObj}
                              colors={barColorArray}
                              keys={serviceArr_legends}
                              totalTime={totalTimeService}
                              xAxis={dateArr_label}
                              yAxis={maxvaluearray}
                              uploadTotal={uploadData}
                              downloadTotal={
                                downloadData
                              }></StackedBarChartExample>
                          )}
                        </View>
                      )}
                    </View>
                  </View>
                ))}
              </View>
            </View>

            <View style={styles.header}>
              <TouchableOpacity
                onPress={() => this.updateLayoutisIdentified('unknown')}>
                <Text style={{fontSize: 26, color: 'white'}}>
                  Unknown Clients (
                  {this.state.activeClientsDataUnIdentified.length})
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  height: this.state.layoutHeightUnIdentified,
                  // height: null,
                  overflow: 'hidden',
                }}>
                {this.state.activeClientsDataUnIdentified.map((item, index) => (
                  <View key={index} style={styles.headerInner}>
                    {/*Header of the Expandable List Item*/}
                    <TouchableOpacity
                      onPress={() => this.updateLayoutUnIdentifed(index)}>
                      <View
                        style={{
                          // borderWidth: 1,
                          // borderColor: 'white',
                          width: Math.round(screenWidth * 0.94),
                          justifyContent: 'space-between',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Image
                            source={logoConst['laptop']}
                            style={[
                              styles.logo_image_header,
                              {marginRight: Math.round(screenWidth * 0.02)},
                            ]}></Image>
                          <Text
                            style={{
                              fontSize: 22,
                              color: 'white',
                              alignSelf: 'center',
                            }}>
                            {item.name}
                          </Text>
                        </View>

                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <TouchableOpacity
                            onPress={() =>
                              this.openModalinClient('Identify', item)
                            }>
                            <View style={styles.buttonView}>
                              <Text style={{fontWeight: 'bold', fontSize: 16}}>
                                Trust
                              </Text>
                            </View>
                          </TouchableOpacity>
                          {this.state.unidentifiedlayoutHeightarr[index] ===
                          0 ? (
                            <Image
                              source={logoConst['right']}
                              style={styles.logo_image_arrow}></Image>
                          ) : (
                            <Image
                              source={logoConst['down']}
                              style={styles.logo_image_arrow}></Image>
                          )}
                        </View>
                      </View>
                    </TouchableOpacity>
                    <View
                      style={{
                        height: this.state.unidentifiedlayoutHeightarr[index],
                        overflow: 'hidden',
                      }}>
                      {isloading ? (
                        <View style={styles.displaytmpView}>
                          <Image
                            source={logoConst['spinner']}
                            style={styles.logo_image_header}></Image>
                        </View>
                      ) : (
                        <View
                          style={{
                            backgroundColor: 'white',
                            width: Math.round(screenWidth * 0.95),
                            borderRadius: Math.round(screenHeight * 0.01),
                            marginVertical: Math.round(screenHeight * 0.01),
                          }}>
                          {serviceArr_legends.length === 0 ? (
                            <View style={styles.displaytmpView}>
                              <Text style={{color: 'black', fontSize: 20}}>
                                No data available
                              </Text>
                            </View>
                          ) : (
                            <StackedBarChartExample
                              data={finalArryObj}
                              colors={barColorArray}
                              keys={serviceArr_legends}
                              totalTime={totalTimeService}
                              xAxis={dateArr_label}
                              yAxis={maxvaluearray}
                              uploadTotal={uploadData}
                              downloadTotal={
                                downloadData
                              }></StackedBarChartExample>
                          )}
                        </View>
                      )}
                    </View>
                  </View>
                ))}
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default ActiveClientScreen;

class StackedBarChartExample extends React.PureComponent {
  render() {
    // console.log(' StackedBarChartExample ');
    // console.log(' StackedBarChartExample ', this.props.data);
    // console.log(' StackedBarChartExample ', this.props.colors);
    // console.log(' StackedBarChartExample ', this.props.keys);
    // console.log(' StackedBarChartExample ', this.props.yAxis);

    data = this.props.data;
    XaxisData = this.props.xAxis;
    colors = this.props.colors;
    keys = this.props.keys;
    YaxisData = this.props.yAxis;
    totalTimeSer = this.props.totalTime;
    uploadtotaldata = this.props.uploadTotal;
    downloadtotaldata = this.props.downloadTotal;
    // console.log(' StackedBarChartExample totalTimeSer', totalTimeSer);
    // console.log(' StackedBarChartExample uploadtotaldata', uploadtotaldata);
    // console.log(' StackedBarChartExample downloadtotaldata', downloadtotaldata);

    for (i = 0; i < totalTimeSer.length; i++) {
      totalTimeSer[i].total = parseFloat(totalTimeSer[i].total);
    }

    var totalDownloadAll = 0;
    for (i = 0; i < downloadtotaldata.length; i++) {
      totalDownloadAll = totalDownloadAll + downloadtotaldata[i].total;
      // console.log(' StackedBarChartExample totalDownloadAll', totalDownloadAll);
    }

    var totalUploadAll = 0;
    for (i = 0; i < downloadtotaldata.length; i++) {
      totalUploadAll = totalUploadAll + uploadtotaldata[i].total;
    }

    var totalTimeAll = 0;
    for (i = 0; i < totalTimeSer.length; i++) {
      totalTimeAll = totalTimeAll + totalTimeSer[i].total;
    }

    for (i = 0; i < totalTimeSer.length; i++) {
      totalTimeSer[i].totalDownload = downloadtotaldata[i].total;
      totalTimeSer[i].totalUpload = uploadtotaldata[i].total;
      totalTimeSer[i].totalUploadAll = totalUploadAll;
      totalTimeSer[i].totalDownloadAll = totalDownloadAll;
      totalTimeSer[i].totalTimeAll = totalTimeAll;
    }

    // console.log(' StackedBarChartExample totalTimeSer', totalTimeSer);

    return (
      <View style={{flexDirection: 'row'}}>
        <YAxis
          data={YaxisData}
          style={{
            marginTop: Math.round(screenHeight * 0.01),
            height: Math.round(screenHeight * 0.34),
            width: Math.round(screenWidth * 0.05),
            paddingLeft: Math.round(screenWidth * 0.01),
            borderWidth: 1,
            borderRightColor: 'black',
            borderTopColor: 'white',
            borderBottomColor: 'white',
            borderLeftColor: 'white',
          }}
          contentInset={{
            top: Math.round(screenHeight * 0.01),
            bottom: Math.round(screenHeight * 0.01),
          }}
          svg={{fontSize: 10, fill: 'grey'}}
        />

        <View
          style={{
            // flex: 1,
            flexDirection: 'column',
            // marginBottom: Math.round(screenHeight * 0.02),
          }}>
          <StackedBarChart
            style={{height: Math.round(screenHeight * 0.35)}}
            keys={keys}
            colors={colors}
            data={data}
            showGrid={false}
            spacingInner={0.3}
            spacingOuter={0.3}
            contentInset={{
              left: Math.round(screenWidth * 0.02),
              right: Math.round(screenWidth * 0.02),
              top: Math.round(screenHeight * 0.01),
              // bottom: Math.round(screenHeight * 0.01),
            }}>
            {/* <Grid></Grid> */}
          </StackedBarChart>
          <XAxis
            style={{
              width: Math.round(screenWidth * 0.9),
              height: Math.round(screenHeight * 0.03),
              paddingTop: Math.round(screenHeight * 0.01),
              borderWidth: 1,
              borderTopColor: 'black',
              borderRightColor: 'white',
              borderBottomColor: 'white',
              borderLeftColor: 'white',
            }}
            numberOfTicks={XaxisData.length}
            data={data}
            formatLabel={(_, index) => {
              // console.log('value = ', index);
              return XaxisData[index];
            }}
            contentInset={{
              left: Math.round(screenWidth * 0.05),
              right: Math.round(screenWidth * 0.05),
            }}
            svg={{fontSize: 10, fill: 'grey'}}
          />

          <View style={{marginVertical: Math.round(screenHeight * 0.01)}}>
            {totalTimeSer.map((item, index) => (
              <View key={index}>
                <ColorInicatoronCharts dataValue={item}></ColorInicatoronCharts>
              </View>
            ))}
          </View>
        </View>
      </View>
    );
  }
}

class ColorInicatoronCharts extends React.PureComponent {
  render() {
    var data = this.props.dataValue;

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: Math.round(screenWidth * 0.9),
          alignItems: 'flex-start',
          // borderWidth: 1,
          // borderColor: 'black',
        }}>
        <ColorIndicator name={data}></ColorIndicator>
      </View>
    );
  }
}

class ColorIndicator extends React.PureComponent {
  convertBytestillMB = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else {
      whichBytes = 'MB';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      }
    }

    if (resp < 0.01) resp = 0.01;

    return {resp: resp, whichBytes: whichBytes};
  };

  convertBytes = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else if (Math.round(bytes / 1024 > 1024)) {
      whichBytes = 'MB';
    } else if (Math.round(bytes > 1024)) {
      whichBytes = 'KB';
    } else {
      whichBytes = 'B';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'MB') {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'KB') {
        resp = (bytes / 1024).toFixed(2);
      } else if (whichBytes == 'B') {
        resp = bytes;
      }
    }

    return resp + whichBytes;
  };

  render() {
    var eachData = this.props.name;
    var policyColorHandle;
    if (policyColors[eachData.name] === undefined) {
      policyColorHandle = policyColors['Other'];
    } else {
      policyColorHandle = policyColors[eachData.name];
    }
    return (
      <View
        style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          marginVertical: Math.round(screenHeight * 0.01),
        }}>
        {logoConst[eachData.name + '_usage'] === undefined ? (
          <Image
            source={logoConst['others_black']}
            style={styles.logo_image_usage}></Image>
        ) : (
          <Image
            source={logoConst[eachData.name + '_usage']}
            style={styles.logo_image_usage}></Image>
        )}
        <View
          style={{
            justifyContent: 'space-around',
            alignItems: 'center',
            flexDirection: 'column',
            width: Math.round(screenWidth * 0.7),
            // borderWidth: 1,
            // borderColor: 'white',
          }}>
          <View
            style={{
              width: Math.round(screenWidth * 0.8),
              marginBottom: Math.round(screenHeight * 0.005),
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              // borderWidth: 1,
              // borderColor: 'white',
            }}>
            <Text style={{fontSize: 18, color: 'black'}}>{eachData.name}</Text>

            <View
              style={{
                height: Math.round(screenHeight * 0.018),
                width: Math.round(screenHeight * 0.018),
                borderRadius: Math.round(screenHeight * 0.009),
                // backgroundColor: policyColors[eachData.name],
                backgroundColor: policyColorHandle,
                marginHorizontal: Math.round(screenWidth * 0.02),
                alignSelf: 'center',
              }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: Math.round(screenWidth * 0.7),
              marginHorizontal: Math.round(screenWidth * 0.01),
            }}>
            {/* thre items in col*/}

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                // width: Math.round(screenWidth * 0.2),
                height: Math.round(screenHeight * 0.14),
                // borderWidth: 1,
                // borderColor: 'white',
              }}>
              <Text style={{fontSize: 12, color: 'black'}}>Time</Text>
              <View>
                <ProgressCircle
                  style={{
                    height: Math.round(screenHeight * 0.08),
                    width: Math.round(screenWidth * 0.2),
                  }}
                  progress={parseFloat(eachData.total / eachData.totalTimeAll)
                    .toFixed(2)
                    .toString()}
                  progressColor={'green'}
                  startAngle={-Math.PI * 0.8}
                  endAngle={Math.PI * 0.8}
                />
                <Text
                  style={{
                    position: 'absolute',
                    top: Math.round(screenHeight * 0.03),
                    alignSelf: 'center',
                    fontSize: 12,
                    color: 'black',
                  }}>
                  {eachData.total.toFixed(2)}
                </Text>
              </View>
              <Text style={{fontSize: 12, color: 'black'}}>HH:MM</Text>
            </View>

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                // width: Math.round(screenWidth * 0.2),
                height: Math.round(screenHeight * 0.14),
                // borderWidth: 1,
                // borderColor: 'white',
              }}>
              <Text style={{fontSize: 12, color: 'black'}}>Download</Text>
              <View>
                <ProgressCircle
                  style={{
                    height: Math.round(screenHeight * 0.08),
                    width: Math.round(screenWidth * 0.2),
                  }}
                  progress={parseFloat(
                    eachData.totalDownload / eachData.totalDownloadAll,
                  )
                    .toFixed(2)
                    .toString()}
                  progressColor={'blue'}
                  startAngle={-Math.PI * 0.8}
                  endAngle={Math.PI * 0.8}
                />
                <Text
                  style={{
                    position: 'absolute',
                    top: Math.round(screenHeight * 0.03),
                    alignSelf: 'center',
                    fontSize: 12,
                    color: 'black',
                  }}>
                  {this.convertBytestillMB(eachData.totalDownload).resp}
                </Text>
              </View>
              <Text style={{fontSize: 12, color: 'black'}}>
                {this.convertBytestillMB(eachData.totalDownload).whichBytes}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
                alignItems: 'center',
                // width: Math.round(screenWidth * 0.2),
                height: Math.round(screenHeight * 0.14),
                // borderWidth: 1,
                // borderColor: 'white',
              }}>
              <Text style={{fontSize: 12, color: 'black'}}>Upload</Text>
              <View>
                <ProgressCircle
                  style={{
                    height: Math.round(screenHeight * 0.08),
                    width: Math.round(screenWidth * 0.2),
                  }}
                  progress={parseFloat(
                    eachData.totalUpload / eachData.totalUploadAll,
                  )
                    .toFixed(2)
                    .toString()}
                  progressColor={'red'}
                  startAngle={-Math.PI * 0.8}
                  endAngle={Math.PI * 0.8}
                />
                <Text
                  style={{
                    position: 'absolute',
                    top: Math.round(screenHeight * 0.03),
                    alignSelf: 'center',
                    fontSize: 12,
                    color: 'black',
                  }}>
                  {this.convertBytestillMB(eachData.totalUpload).resp}
                </Text>
              </View>
              <Text style={{fontSize: 12, color: 'black'}}>
                {this.convertBytestillMB(eachData.totalUpload).whichBytes}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  mainView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  header: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: 'black',
    margin: Math.round(screenWidth * 0.03),
  },

  headerInner: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: 'black',
    marginVertical: Math.round(screenHeight * 0.02),
  },

  displaytmpView: {
    // position: 'absolute',
    left: 0,
    width: Math.round(screenWidth * 0.95),
    height: Math.round(screenHeight * 0.35),
    marginVertical: Math.round(screenHeight * 0.01),
    borderRadius: Math.round(screenHeight * 0.02),
    backgroundColor: '#eff3ff',
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo_image_arrow: {
    width: Math.round(screenHeight * 0.02),
    height: Math.round(screenHeight * 0.02),
    resizeMode: 'contain',
  },

  buttonView: {
    height: Math.round(screenHeight * 0.025),
    width: Math.round(screenWidth * 0.2),
    marginHorizontal: Math.round(screenWidth * 0.08),
    backgroundColor: '#05a9e1',
    // backgroundColor: '#E2B014',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },

  ModalInput: {
    width: Math.round(screenWidth * 0.65),
    height: Math.round(screenHeight * 0.05),
    borderColor: 'black',
    borderWidth: 1,
  },
  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: Math.round(screenHeight * 0.35),
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  logo_image: {
    width: Math.round(screenHeight * 0.07),
    height: Math.round(screenHeight * 0.07),
    resizeMode: 'contain',
  },
  logo_image_usage: {
    width: Math.round(screenHeight * 0.05),
    height: Math.round(screenHeight * 0.05),
    resizeMode: 'contain',
    // alignSelf: 'baseline',
    alignSelf: 'flex-start',
    marginRight: Math.round(screenWidth * 0.08),
    padding: Math.round(screenWidth * 0.03),
  },

  logo_image_close: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },
});

const policyColors = {
  AmazonPrime: '#0F79AF',
  Facebook: '#159BF3',
  Instagram: '#944195',
  Netflix: '#E2B014',
  Skype: '#FA76D1',
  Slack: '#4B154C',
  Vimeo: '#19B7EA',
  Youtube: '#F80100',
  Iperf3_Client_1: '#54D06E',
  Iperf3_Client_2: '#EE4266',
  Other: '#AA00F0',
  Zoom: '#C288F7',
  GoogleMeet: '#0B645A',
  CiscoWebex: '#014E72',
  GitHub: '#aaa',
  MicrosoftTeams: '#5255AA',
  Minecraft: '#B7ABAB',
  Dropbox: '#00FF00',
  FaceTime: '#0000FF',
};

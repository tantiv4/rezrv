import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {connectToSocket, sendMessage} from './socket';
// import {ScrollView} from 'react-native-gesture-handler';

import SideMenu from 'react-native-side-menu';

const {logoConst} = require('../../constant/Model');
import ModalHandlerCloud from './ModalHandlerCloud';

import CloudScreenMainViewHeader from './CloudScreenMainViewHeader';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

var timeOut;

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openModalCld: (val) =>
      dispatch({
        type: 'OPEN_MODAL_CLD',
        mode: val,
      }),
  };
}

class CloudScreen extends React.PureComponent {
  state = {
    isOpenVal: false,
    isonline: false,
    downloadRate: 0,
    uploadRate: 0,
    cloudDataRefresh: {},
    refreshing: false,
  };

  constructor(props) {
    super(props);
    console.log('constructor cloudScreen.js');

    // connectToSocket(this.props.token);

    timeOut = setTimeout(() => {
      this.setState({
        isonline: false,
      });
    }, 120 * 1000);

    this.socketConnect();
  }

  socketConnect = () => {
    connectToSocket(this.props.token, (value) => {
      clearTimeout(timeOut);

      this.setState({
        isonline: true,
      });

      if (value.device) {
        console.log('cloudScreen device update');
        this.setState({
          downloadRate: value.device.downloadRate,
          uploadRate: value.device.uploadRate,
        });
      } else {
        console.log('cloudScreen device data');
      }

      timeOut = setTimeout(() => {
        this.setState({
          isonline: false,
        });
      }, 120 * 1000);
    });
  };

  componentWillUnmount() {
    console.log('componentWillUnmount cloudScreen.js');
    sendMessage('forceDisconnect');
  }

  toggleSideMenu = () => {
    // console.log('toggleSideMenu');
    this.setState({
      isOpenVal: !this.state.isOpenVal,
    });
  };

  onChangeSideMenu = (isOpenCheck) => {
    // console.log('onChangeSideMenu isOpenCheck = ', isOpenCheck);
    if (isOpenCheck === false) {
      this.setState({
        isOpenVal: !this.state.isOpenVal,
      });
    }
  };

  refresh = () => {
    console.log('cloudScreen refresh');
    sendMessage('forceDisconnect');
    this.setState({
      refreshing: true,
    });

    this.callGetUserAPI(this.props.token);
  };

  callGetUserAPI = (token) => {
    console.log('callGetUserAPI');
    var myHeaders = new Headers();
    myHeaders.append('jwt-token', token);
    var raw = '';
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    var timeOut;
    timeOut = setTimeout(() => {
      this.setState({
        refreshing: false,
        cloudDataRefresh: {},
      });
    }, 8000);

    fetch('https://rezrvwebapi.tantiv4.com/users', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log('callGetUser result = ', result);
        var resultJSON = JSON.parse(result);
        if (resultJSON) {
          console.log('callGetUserAPI resultJSON success ');
          clearTimeout(timeOut);
          this.setState({
            refreshing: false,
            cloudDataRefresh: resultJSON,
          });
          this.socketConnect();
        } else {
          clearTimeout(timeOut);
          this.setState({
            refreshing: false,
            cloudDataRefresh: {},
          });
        }
      })
      .catch((error) => {
        console.log('callGetUserAPI error', error);
        clearTimeout(timeOut);
        this.setState({
          refreshing: false,
          cloudDataRefresh: {},
        });
      });
  };

  isEmptyObj = (obj) => {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  };

  render() {
    // console.log('cloudscreen this.props.cloudData = ', this.props.cloudData);

    const MenuComponent = (
      <View
        style={{
          flex: 1,
          backgroundColor: '#F0F0F0',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-around',
            // alignItems: 'center',
            marginTop: Math.round(screenHeight * 0.05),
            width: Math.round(screenWidth * 0.65),
          }}>
          <View
            style={{
              marginVertical: Math.round(screenHeight * 0.03),
              alignSelf: 'center',
            }}>
            <Text
              style={{
                fontSize: 36,
                color: 'black',
              }}></Text>
          </View>

          <TouchableOpacity onPress={() => this.props.openModalCld('logout')}>
            <View
              style={{
                backgroundColor: '#DCDCDC',
                marginTop: Math.round(screenHeight * 0.01),
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={logoConst['logout']}
                style={[styles.logo_image_sidemenu]}
              />
              <Text
                style={{
                  fontSize: 30,
                  color: 'black',
                  alignSelf: 'center',
                }}>
                Log Out
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );

    var cloudDataInfo;
    if (this.isEmptyObj(this.state.cloudDataRefresh)) {
      cloudDataInfo = this.props.cloudData;
    } else {
      cloudDataInfo = this.state.cloudDataRefresh;
    }

    return (
      <SideMenu
        onChange={(isOpen) => this.onChangeSideMenu(isOpen)}
        menuPosition={'right'}
        isOpen={this.state.isOpenVal}
        menu={MenuComponent}>
        <View style={styles.container}>
          <ModalHandlerCloud
            navigation={this.props.navigation}></ModalHandlerCloud>
          <View style={styles.headingView}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Home')}>
              <Image
                source={logoConst['arrow']}
                style={styles.logo_image_header}></Image>
            </TouchableOpacity>
            <Text style={{fontSize: 26, color: 'white'}}>Analytics</Text>
            <TouchableOpacity onPress={this.toggleSideMenu}>
              <Image
                source={logoConst['settings']}
                style={styles.logo_image_header}></Image>
            </TouchableOpacity>
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.refresh}
              />
            }
            style={styles.scrollViewMain}>
            <View style={styles.mainView}>
              <View style={styles.mainViewHeadSection}>
                <CloudScreenMainViewHeader
                  cloudData={cloudDataInfo}
                  isOnline={this.state.isonline}
                  downloadRate={this.state.downloadRate}
                  uploadRate={
                    this.state.uploadRate
                  }></CloudScreenMainViewHeader>
              </View>
              <View style={styles.mainViewBoxSection}>
                <View style={styles.mainViewBoxSectionCol}>
                  <View style={styles.mainViewBoxSectionRow}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('Usage', {
                          token: this.props.token,
                          deviceID: cloudDataInfo.device.deviceId,
                        })
                      }>
                      <View style={styles.mainViewBoxSectionBox}>
                        <Image
                          source={logoConst['box1']}
                          style={[
                            styles.logo_image_body,
                            {marginBottom: Math.round(screenHeight * 0.01)},
                          ]}></Image>
                        <Text style={{fontSize: 20, color: 'white'}}>
                          Usage
                        </Text>
                      </View>
                    </TouchableOpacity>

                    <View style={styles.mainViewBoxSectionBoxline}></View>

                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('ActiveClients', {
                          token: this.props.token,
                          deviceID: cloudDataInfo.device.deviceId,
                          // activeClients: [
                          //   '192.168.1.231',
                          //   '192.168.1.194',
                          //   '192.168.1.228',
                          //   '192.168.1.225',
                          //   '192.168.1.134',
                          // ],
                          activeClients: cloudDataInfo.device.activeClients,
                        })
                      }>
                      <View style={styles.mainViewBoxSectionBox}>
                        <Image
                          source={logoConst['box2']}
                          style={[
                            styles.logo_image_body,
                            {marginBottom: Math.round(screenHeight * 0.01)},
                          ]}></Image>
                        <Text style={{fontSize: 20, color: 'white'}}>
                          Clients
                        </Text>
                        <Text style={{fontSize: 12, color: 'white'}}>
                          Active:{cloudDataInfo.device.activeClients.length}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={[
                      styles.mainViewBoxSectionRow,
                      {borderTopColor: 'white'},
                    ]}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate('Stats', {
                          token: this.props.token,
                          deviceID: cloudDataInfo.device.deviceId,
                        })
                      }>
                      <View style={styles.mainViewBoxSectionBox}>
                        <Image
                          source={logoConst['box4']}
                          style={[
                            styles.logo_image_body,
                            {marginBottom: Math.round(screenHeight * 0.01)},
                          ]}></Image>
                        <Text style={{fontSize: 20, color: 'white'}}>
                          Statistics
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <View style={styles.mainViewBoxSectionBoxline}></View>

                    <TouchableOpacity
                      onPress={() => this.props.openModalCld('info')}>
                      <View style={[styles.mainViewBoxSectionBox]}>
                        <Image
                          source={logoConst['box3']}
                          style={[
                            styles.logo_image_body,
                            {marginBottom: Math.round(screenHeight * 0.01)},
                          ]}></Image>
                        <Text style={{fontSize: 20, color: 'white'}}>Info</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </SideMenu>
    );
  }
}

// export default CloudScreen;
export default connect(mapStateToProps, mapDispatchToProps)(CloudScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderWidth:1,
    // borderColor: "white"
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },
  scrollViewMain: {},
  mainView: {
    // height: Math.round(screenHeight * 0.86),
    width: Math.round(screenWidth * 0.95),
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // borderWidth:1,
    // borderColor: "white"
  },
  mainViewHeadSection: {
    width: Math.round(screenWidth * 0.95),
    height: Math.round(screenHeight * 0.3),
    backgroundColor: 'white',
    // justifyContent: 'space-around',
    // alignItems: 'center',
    borderRadius: Math.round(screenWidth * 0.1),
    // borderWidth:1,
    // borderColor: "white"
  },
  mainViewBoxSection: {
    width: Math.round(screenWidth * 0.95),
    //marginTop: Math.round(screenWidth * 0.05),
    backgroundColor: 'black',
    justifyContent: 'space-around',
    alignItems: 'center',

    // borderWidth:1,
    // borderTopColor: "white"
  },
  mainViewBoxSectionCol: {
    width: Math.round(screenWidth * 0.95),
    paddingTop: Math.round(screenWidth * 0.05),
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    // borderWidth:1,
    // borderColor: "white"
  },
  mainViewBoxSectionRow: {
    flexDirection: 'row',
    padding: Math.round(screenHeight * 0.02),
    borderWidth: 2,
  },
  mainViewBoxSectionBox: {
    width: Math.round(screenHeight * 0.18),
    height: Math.round(screenHeight * 0.15),
    borderWidth: 1,
    marginHorizontal: Math.round(screenWidth * 0.06),
    marginVertical: Math.round(screenHeight * 0.02),
    backgroundColor: 'black',
    borderRadius: Math.round(screenHeight * 0.02),
    justifyContent: 'center',
    alignItems: 'center',
    // borderColor: 'white',
  },
  mainViewBoxSectionBoxline: {
    height: Math.round(screenHeight * 0.2),
    width: 0.5,
    borderWidth: 1,
    borderColor: 'white',
  },

  logo_image_body: {
    width: Math.round(screenHeight * 0.07),
    height: Math.round(screenHeight * 0.07),
    resizeMode: 'contain',
  },

  logo_image_sidemenu: {
    width: Math.round(screenHeight * 0.05),
    height: Math.round(screenHeight * 0.05),
    resizeMode: 'contain',
  },
});

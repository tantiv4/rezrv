import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  TextInput,
} from 'react-native';

import LoadingSpinner from '../LoadingSpinner';
import CloudScreen from './CloudScreen';
import AsyncStorage from '@react-native-community/async-storage';

// import PTRView from 'react-native-pull-to-refresh';

const {ServicesConst, logoConst} = require('../../constant/Model');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const mainBoxHeight = Math.round(screenHeight * 0.4);

class CloudScreenDecider extends React.PureComponent {
  state = {
    loading: true,
    data: '',
    email: '',
    password: '',
    token: '',
    sendUserData: {},
  };

  componentDidMount() {
    // this.callGetAPI();
    this.checkTokenAPI();
  }

  checkTokenAPI = () => {
    (async () => {
      try {
        var jsonValue = await AsyncStorage.getItem('@storage_email');
        console.log('checkTokenAPI jsonValue= ', jsonValue);
        if (jsonValue != null) {
          this.setState({
            email: JSON.parse(jsonValue).email,
          });
        }
        jsonValue = await AsyncStorage.getItem('@storage_Key');
        console.log('checkTokenAPI jsonValue= ', jsonValue);
        if (jsonValue != null) {
          var token = JSON.parse(jsonValue).token;
          if (token == '') {
            this.setState({
              loading: false,
              data: '',
            });
          } else {
            //console.log('checkTokenAPI token= ', token);
            const userDetails = await this.callGetUser(token);
            console.log('checkTokenAPI userDetails.type = ', userDetails.type);
            if (userDetails.type === 'TOKEN_ERROR_EXPIRED') {
              this.setState({
                loading: false,
                data: 'Login Expired. please try again',
              });
            } else if (userDetails.type === 'success') {
              this.setState({
                loading: false,
                data: 'success',
                sendUserData: userDetails,
                token: token,
              });
            }
          }
        } else {
          this.setState({
            loading: false,
            data: '',
          });
        }
      } catch (e) {
        // error reading value
      }
    })();
  };

  cancelButtonPressed = () => {
    console.log('cancelButtonPressed');
    this.props.navigation.navigate('Home');
  };

  loginButtonPressed = () => {
    console.log('this.state.email = ', this.state.email);
    console.log('this.state.password = ', this.state.password);
    if (this.state.email === '' || this.state.password === '') {
      this.setState({data: 'Email or Password is empty'});
    } else if (!this.validateEmail(this.state.email)) {
      this.setState({data: 'Invalid Email'});
    } else if (this.state.password.length < 6) {
      this.setState({data: 'Password should be more than 6 charcters'});
    } else {
      this.setState({
        loading: true,
      });

      (async () => {
        // {"email": "demo.tantiv@gmail.com", "password": "foobar17"}
        var email = this.state.email;
        var password = this.state.password;

        const userDetails = await this.callLoginAPI(email, password);
        console.log('loginButtonPressed userDetails.type = ', userDetails.type);
        this.setState({
          loading: false,
        });
        if (userDetails.type === 'success') {
          if (userDetails.token && userDetails.token != '') {
            const getUserDetails = await this.callGetUser(userDetails.token);
            console.log(
              'loginButtonPressed getUserDetails.type = ',
              getUserDetails.type,
            );
            if (getUserDetails.type === 'TOKEN_ERROR_EXPIRED') {
              this.setState({
                loading: false,
                data: 'Login Expired. Please try again',
              });
            } else if (getUserDetails.type === 'success') {
              var jsonValue = '{"token":"' + userDetails.token + '"}';
              await AsyncStorage.setItem('@storage_Key', jsonValue);
              jsonValue = '{"email":"' + email + '"}';
              await AsyncStorage.setItem('@storage_email', jsonValue);
              this.setState({
                loading: false,
                data: 'success',
                sendUserData: getUserDetails,
                token: userDetails.token,
              });
            }
          }
        } else if (userDetails.type === 'error') {
          this.setState({
            loading: false,
            data: userDetails.message,
          });
        }
      })();
    }
  };

  callLoginAPI = (email, password) =>
    new Promise((resolve, reject) => {
      var myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');

      var raw = JSON.stringify({
        email: email,
        password: password,
      });

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
      };

      fetch('https://rezrvwebapi.tantiv4.com/auth/user/login', requestOptions)
        .then((response) => response.text())
        .then((result) => {
          // console.log(result);
          var resultJSON = JSON.parse(result);
          resolve(resultJSON);
        })
        .catch((error) => console.log('error', error));
    });

  callGetUser = (token) =>
    new Promise((resolve, reject) => {
      console.log('callGetUser');
      var myHeaders = new Headers();
      myHeaders.append('jwt-token', token);
      var raw = '';
      var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
      };

      fetch('https://rezrvwebapi.tantiv4.com/users', requestOptions)
        .then((response) => response.text())
        .then((result) => {
          // console.log('callGetUser result = ', result);
          var resultJSON = JSON.parse(result);
          resolve(resultJSON);
        })
        .catch((error) => {
          console.log('callGetUser error', error);
          var errorJSON = '{"type":"' + error + '"}';
          resolve(JSON.parse(errorJSON));
        });
    });

  validateEmail = (email) => {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
  };

  emailCheck = (text) => {
    this.setState({email: text});
  };

  passwordCheck = (pass) => {
    this.setState({password: pass});
  };

  render() {
    const {loading, data} = this.state;
    // console.log('loading = ', loading);
    // var testData = 'NULL';
    return (
      <View style={styles.container}>
        {/* <PTRView onRefresh={this.refresh}> */}
        {loading ? (
          <LoadingSpinner />
        ) : data === 'success' ? (
          <CloudScreen
            navigation={this.props.navigation}
            cloudData={this.state.sendUserData}
            token={this.state.token}
          />
        ) : (
          <View style={styles.boxContainer}>
            <Image source={logoConst['email']} style={styles.logo_image} />
            <Text style={styles.textView}>Enter Email ID and Password</Text>
            <Text style={styles.textView}>For Cloud Login</Text>
            <TextInput
              style={styles.ModalInput}
              underlineColorAndroid="transparent"
              placeholder="Email"
              placeholderTextColor="lightgrey"
              autoCapitalize="none"
              textAlign={'center'}
              keyboardType={'email-address'}
              onChangeText={this.emailCheck}
              value={this.state.email}
            />
            <TextInput
              style={styles.ModalInput}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="lightgrey"
              autoCapitalize="none"
              textAlign={'center'}
              secureTextEntry={true}
              onChangeText={this.passwordCheck}
            />

            {data !== '' ? (
              <Text style={{color: 'red'}}>{data}</Text>
            ) : (
              <Text style={{display: 'none'}}></Text>
            )}

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
                width: Math.round(screenWidth * 0.7),
              }}>
              <TouchableOpacity onPress={this.cancelButtonPressed}>
                <View style={styles.buttonView}>
                  <Text style={styles.textView}>Cancel</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.loginButtonPressed}>
                <View style={styles.buttonView}>
                  <Text style={styles.textView}>Login</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {/* </PTRView> */}
      </View>
    );
  }
}

export default CloudScreenDecider;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
  },
  textView: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  buttonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  boxContainer: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },

  logo_image: {
    width: Math.round(screenHeight * 0.06),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },

  ModalInput: {
    width: Math.round(screenWidth * 0.65),
    height: Math.round(screenHeight * 0.05),
    borderColor: 'black',
    borderWidth: 1,
  },
});

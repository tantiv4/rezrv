import React from 'react';
import {StyleSheet, View, Text, Dimensions, Image} from 'react-native';
import moment from 'moment';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const {logoConst} = require('../../constant/Model');

class CloudScreenMainViewHeader extends React.PureComponent {
  render() {
    var cloudData = this.props.cloudData;
    var downloadRate = this.props.downloadRate;
    var uploadRate = this.props.uploadRate;
    var isOnline = this.props.isOnline;

    if (isOnline === false) {
      if (moment().diff(moment(cloudData.device.updatedAt), 'seconds') > 120) {
        isOnline = false;
      } else {
        isOnline = true;
      }
    }

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          alignItems: 'center',
        }}>
        <View
          style={{
            // flex: 1,
            marginTop: Math.round(screenHeight * 0.01),
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignSelf: 'flex-start',
            marginHorizontal: Math.round(screenWidth * 0.05),
          }}>
          <Image
            source={logoConst['router']}
            style={[
              styles.logo_image_header,
              {
                alignSelf: 'center',
                marginRight: Math.round(screenWidth * 0.07),
              },
            ]}></Image>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'space-around',
              alignItems: 'flex-start',
              marginRight: Math.round(screenWidth * 0.07),
            }}>
            <Text style={{fontSize: 24, color: 'black'}}>
              {cloudData.device.ssid}
            </Text>
            <Text style={{fontSize: 14, color: 'black'}}>
              {cloudData.device.deviceId}
            </Text>
          </View>
          {isOnline ? (
            <Text
              style={{
                fontSize: 12,
                color: 'green',
              }}>
              ONLINE
            </Text>
          ) : (
            <Text
              style={{
                fontSize: 12,
                color: 'red',
              }}>
              OFFLINE
            </Text>
          )}
        </View>

        <View
          style={{
            width: Math.round(screenWidth * 0.9),
            height: 1,
            borderColor: 'black',
            borderWidth: 1,
          }}></View>

        {/* <View style={{justifyContent: 'space-evenly'}}> */}
        <View
          style={{
            width: Math.round(screenWidth * 0.85),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignSelf: 'flex-start',
            marginLeft: Math.round(screenWidth * 0.05),
          }}>
          <Text
            style={{
              fontSize: 14,
              color: 'black',
              fontWeight: 'bold',
            }}>
            Public IP
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: 'black',
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            {cloudData.device.publicIp}
          </Text>
        </View>

        <View
          style={{
            width: Math.round(screenWidth * 0.85),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignSelf: 'flex-start',
            marginLeft: Math.round(screenWidth * 0.05),
          }}>
          <Text
            style={{
              fontSize: 14,
              color: 'black',
              fontWeight: 'bold',
            }}>
            Current Download Rate (in use)
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: 'black',
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            {downloadRate === 0
              ? (cloudData.device.downloadRate / 125000).toFixed(2)
              : (downloadRate / 125000).toFixed(2)}
            Mbps
          </Text>
        </View>

        <View
          style={{
            width: Math.round(screenWidth * 0.85),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignSelf: 'flex-start',
            marginLeft: Math.round(screenWidth * 0.05),
            marginBottom: Math.round(screenHeight * 0.01),
          }}>
          <Text
            style={{
              fontSize: 14,
              color: 'black',
              fontWeight: 'bold',
            }}>
            Current Upload Rate (in use)
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: 'black',
              alignSelf: 'center',
              fontWeight: 'bold',
            }}>
            {uploadRate === 0
              ? (cloudData.device.uploadRate / 125000).toFixed(2)
              : (uploadRate / 125000).toFixed(2)}
            Mbps
          </Text>
        </View>
      </View>
    );
  }
}

export default CloudScreenMainViewHeader;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  mainView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

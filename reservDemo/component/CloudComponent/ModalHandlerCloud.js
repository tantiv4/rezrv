import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';

import Modal from 'react-native-modal';

import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

const {ServicesConst, logoConst} = require('../../constant/Model');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const mainBoxHeight = Math.round(screenHeight * 0.35);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
    modalNameCld: state.modalNameCld,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeModalCld: () =>
      dispatch({
        type: 'CLOSE_MODAL_CLD',
      }),
  };
}

class ModalHandlerCloud extends React.PureComponent {
  state = {
    isModalVisible: false,
    ModalName: '',
  };

  componentDidMount() {
    this.setToggleMode();
  }

  componentDidUpdate() {
    this.setToggleMode();
  }

  setToggleMode = () => {
    if (this.props.action == 'openModalCld') {
      this.setState({
        isModalVisible: true,
        ModalName: this.props.modalNameCld,
      });
    }
    if (this.props.action == 'closeModalCld') {
      this.setState({
        isModalVisible: false,
      });
    }
  };

  toggleModal = (value) => {
    console.log('modal cld toggleModal value = ', value);
    if (value === 'OK') {
      (async () => {
        var updatedjsonVal = '{"token":""}';
        await AsyncStorage.setItem('@storage_Key', updatedjsonVal);
      })();

      this.setState({
        isModalVisible: !this.state.isModalVisible,
      });
      this.props.closeModalCld();
      this.props.navigation.navigate('Home');
    } else {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
      });
      this.props.closeModalCld();
    }
  };

  handlePressOut = () => {
    // console.log('handlePressOut');
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
    this.props.closeModalCld();
  };

  handlePressOutSub = () => {
    // console.log('handlePressOutSub');
  };

  render() {
    // console.log('ModalName = ', this.state.ModalName);
    if (this.props.action === undefined) {
      return <Modal isVisible={false}></Modal>;
    } else {
      return (
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            {this.state.ModalName === 'logout' ? (
              <View
                onStartShouldSetResponder={(evt) => true}
                // onResponderRelease={this.handlePressOutSub}
                onResponderGrant={this.handlePressOutSub}
                style={styles.settingModalSubView}>
                <Image source={logoConst['logout']} style={styles.logo_image} />
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>Log Out</Text>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  Are You Sure ?
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Cancel
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.toggleModal('OK')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>OK</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View
                onStartShouldSetResponder={(evt) => true}
                // onResponderRelease={this.handlePressOutSub}
                onResponderGrant={this.handlePressOutSub}
                style={styles.settingModalSubView}>
                <Image source={logoConst['info']} style={styles.logo_image} />
                <Text style={{fontSize: 24, fontWeight: 'bold'}}>
                  App Version is
                </Text>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                  {logoConst['App_Version']}
                </Text>
                <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>Done</Text>
                  </View>
                </TouchableOpacity>
                <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                  Copyright Tantiv4.
                </Text>
              </View>
            )}
          </View>
        </Modal>
      );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalHandlerCloud);

const styles = StyleSheet.create({
  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  logo_image: {
    width: Math.round(screenHeight * 0.07),
    height: Math.round(screenHeight * 0.07),
    resizeMode: 'contain',
  },
});

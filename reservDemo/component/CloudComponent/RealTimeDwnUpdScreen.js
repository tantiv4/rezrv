import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const {logoConst} = require('../../constant/Model');

class RealTimeDwnUpdScreen extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headingView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Cloud')}>
            <Image
              source={logoConst['arrow']}
              style={styles.logo_image_header}></Image>
          </TouchableOpacity>
          <Text style={{fontSize: 32, color: 'white'}}>Real Time Data</Text>
          {/* <View></View> */}
          <Image
            source={logoConst['blackArrow']}
            style={[styles.logo_image_header]}></Image>
        </View>

        <View style={styles.mainView}>
          <Text style={{fontSize: 24, color: 'white'}}>
            Real Time Data Screen
          </Text>
        </View>
      </View>
    );
  }
}

export default RealTimeDwnUpdScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  mainView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

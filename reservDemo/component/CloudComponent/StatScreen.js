import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';

// import {BarChart} from 'react-native-chart-kit';
import {BarChart, Grid, XAxis, YAxis} from 'react-native-svg-charts';
// import {ScrollView} from 'react-native-gesture-handler';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const {logoConst} = require('../../constant/Model');
import LoadingSpinner from '../LoadingSpinner';

class GroupedBarChart extends React.PureComponent {
  render() {
    const data1 = this.props.data_dwn.map((value) => ({value}));
    const data2 = this.props.data_upd.map((value) => ({value}));

    const barData = [
      {
        data: data1,
        svg: {
          fill: 'rgb(134, 65, 244)',
        },
      },
      {
        data: data2,
        svg: {
          fill: '#05a9e1',
        },
      },
    ];

    return (
      <BarChart
        style={{height: Math.round(screenHeight * 0.35)}}
        data={barData}
        yAccessor={({item}) => item.value}
        contentInset={{
          left: Math.round(screenHeight * 0.01),
          right: Math.round(screenHeight * 0.01),
        }}>
        {/* <Grid /> */}
      </BarChart>
    );
  }
}

class ColorInicatoronCharts extends React.PureComponent {
  render() {
    var data = this.props.dataValue;

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          width: Math.round(screenWidth * 0.9),
          alignItems: 'flex-start',
          // borderWidth: 1,
          // borderColor: 'black',
        }}>
        <ColorIndicatorNew name={data}></ColorIndicatorNew>
      </View>
    );
  }
}

class ColorIndicatorNew extends React.PureComponent {
  convertBytes = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else if (Math.round(bytes / 1024 > 1024)) {
      whichBytes = 'MB';
    } else if (Math.round(bytes > 1024)) {
      whichBytes = 'KB';
    } else {
      whichBytes = 'B';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'MB') {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'KB') {
        resp = (bytes / 1024).toFixed(2);
      } else if (whichBytes == 'B') {
        resp = bytes;
      }
    }

    return resp + whichBytes;
  };

  render() {
    var policyColorHandle;
    if (policyColors[this.props.name.label] === undefined) {
      policyColorHandle = policyColors['Other'];
    } else {
      policyColorHandle = policyColors[this.props.name.label];
    }
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: Math.round(screenWidth * 0.85),
          alignItems: 'center',
          marginVertical: Math.round(screenHeight * 0.005),
          // borderWidth: 1,
          // borderColor: 'black',
        }}>
        <View style={{flexDirection: 'row'}}>
          <View
            style={{
              height: Math.round(screenHeight * 0.02),
              width: Math.round(screenHeight * 0.02),
              borderRadius: Math.round(screenHeight * 0.01),
              // backgroundColor: policyColors[this.props.name.label],
              backgroundColor: policyColorHandle,
              marginHorizontal: Math.round(screenWidth * 0.02),
            }}
          />
          <Text>{this.props.name.label}</Text>
        </View>
        <Text>{this.convertBytes(this.props.name.value)}</Text>
      </View>
    );
  }
}

class HorizontaBarChartWithYAxis extends React.PureComponent {
  convertBytes = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else if (Math.round(bytes / 1024 > 1024)) {
      whichBytes = 'MB';
    } else if (Math.round(bytes > 1024)) {
      whichBytes = 'KB';
    } else {
      whichBytes = 'B';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'MB') {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'KB') {
        resp = (bytes / 1024).toFixed(2);
      } else if (whichBytes == 'B') {
        resp = bytes;
      }
    }

    return {value: resp, label: whichBytes};
  };

  render() {
    // console.log(
    //   'HorizontaBarChartWithYAxis this.props.data = ',
    //   this.props.data,
    // );
    const data = this.props.data;

    var high = Math.max.apply(
      Math,
      data.map(function (o) {
        return o.value;
      }),
    );

    high = this.convertBytes(high);
    var xAxisData = [
      0,
      parseFloat(high.value / 4).toFixed(2),
      parseFloat(high.value / 3).toFixed(2),
      parseFloat(high.value / 2).toFixed(2),
      parseFloat(high.value).toFixed(2),
    ];

    return (
      <View>
        <View
          style={{
            flexDirection: 'column',
            height: Math.round(screenHeight * 0.3),
            width: Math.round(screenWidth * 0.9),
            padding: Math.round(screenHeight * 0.01),
            marginVertical: Math.round(screenHeight * 0.01),
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              width: Math.round(screenWidth * 0.9),
            }}>
            <View
              style={{
                width: 1,
                height: Math.round(screenHeight * 0.3),
                borderColor: 'black',
                borderWidth: 1,
              }}></View>
            <BarChart
              style={{
                flex: 1,
                marginRight: Math.round(screenWidth * 0.01),
              }}
              data={data}
              horizontal={true}
              yAccessor={({item}) => item.value}
              xAccessor={({item}) => item.value}
              svg={{fill: 'rgba(134, 65, 244, 0.8)'}}
              contentInset={{
                left: Math.round(screenHeight * 0.01),
                right: Math.round(screenHeight * 0.01),
              }}
              spacing={0.2}
              gridMin={0}>
              {/* <Grid direction={Grid.Direction.VERTICAL} /> */}
            </BarChart>
          </View>
          <View
            style={{
              width: Math.round(screenWidth * 0.98),
              justifyContent: 'flex-end',
              alignItems: 'flex-end',
            }}>
            <View
              style={{
                width: Math.round(screenWidth * 0.9),
                height: 1,
                borderWidth: 1,
                borderColor: 'black',
                marginRight: Math.round(screenWidth * 0.08),
              }}></View>
          </View>
        </View>

        <XAxis
          style={{
            marginHorizontal: Math.round(screenWidth * 0.06),
            marginVertical: Math.round(screenHeight * 0.01),
          }}
          data={xAxisData}
          // numberOfTicks={7}
          formatLabel={(_, index) => {
            if (index === 0) {
              return high.label + '  ' + xAxisData[index];
            } else {
              return xAxisData[index];
            }
          }}
          contentInset={{
            left: Math.round(screenHeight * 0.03),
            right: Math.round(screenHeight * 0.03),
          }}
          svg={{
            fill: 'grey',
            fontSize: 10,
          }}
        />

        <View
          style={{
            marginLeft: Math.round(screenWidth * 0.035),
            marginVertical: Math.round(screenHeight * 0.02),
          }}>
          {data.map((item, index) => (
            <View key={index}>
              <ColorInicatoronCharts dataValue={item}></ColorInicatoronCharts>
            </View>
          ))}
        </View>
      </View>
    );
  }
}

class StatScreen extends React.PureComponent {
  state = {
    dataByHost: [],
    dataByTime: [],
    isLoading: true,
    isToday: false,
    isLastMonth: false,
    isLastWeek: true,
  };
  constructor(props) {
    super(props);
    console.log('constructor StatScreen.js');

    const {token, deviceID} = this.props.route.params;
    // console.log('constructor StatScreen.js token = ', token);
    console.log('constructor StatScreen.js deviceID = ', deviceID);

    this.callFilterByDateAPI(deviceID, 'last1week', token);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount StatScreen.js');
  }

  buttonpressCallAPI = (deviceID, date, token) => {
    if (date === 'today') {
      this.setState({
        isToday: true,
        isLastMonth: false,
        isLastWeek: false,
      });
    } else if (date === 'last1week') {
      this.setState({
        isToday: false,
        isLastMonth: false,
        isLastWeek: true,
      });
    } else if (date === 'last1month') {
      this.setState({
        isToday: false,
        isLastMonth: true,
        isLastWeek: false,
      });
    }

    this.callFilterByDateAPI(deviceID, date, token);
  };

  callFilterByDateAPI = (deviceID, date, token) => {
    var myHeaders = new Headers();
    myHeaders.append('jwt-token', token);
    myHeaders.append('time-offset', new Date().getTimezoneOffset() * -1);
    myHeaders.append('Content-Type', 'application/json');

    var raw = JSON.stringify({deviceId: deviceID, filter: date});

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch('https://rezrvwebapi.tantiv4.com/graphs/filter', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        //console.log(result);
        var resultJSON = JSON.parse(result);
        if (resultJSON && resultJSON.dataByHost && resultJSON.dataByTime) {
          // console.log('resultJSON = ', resultJSON.dataByHost);
          this.setState({
            dataByHost: resultJSON.dataByHost,
            dataByTime: resultJSON.dataByTime,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
        this.setState({
          dataByHost: [],
          dataByTime: [],
          isLoading: false,
        });
      });
  };

  convertBytes = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else if (Math.round(bytes / 1024 > 1024)) {
      whichBytes = 'MB';
    } else if (Math.round(bytes > 1024)) {
      whichBytes = 'KB';
    } else {
      whichBytes = 'B';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'MB') {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'KB') {
        resp = (bytes / 1024).toFixed(2);
      } else if (whichBytes == 'B') {
        resp = bytes;
      }
    }

    return {value: resp, label: whichBytes};
  };

  render() {
    const data = [50, 10, 40, 100, 85];

    var data_dwn = this.state.dataByTime.map((item) => {
      return Math.round(parseFloat((item.sdnb / (1024 * 1024)).toFixed(2)));
    });
    var data_upd = this.state.dataByTime.map((item) => {
      return Math.round(parseFloat((item.sunb / (1024 * 1024)).toFixed(2)));
    });

    var TotalDownloadValue = 0;
    for (i = 0; i < this.state.dataByTime.length; i++) {
      TotalDownloadValue =
        TotalDownloadValue + parseInt(this.state.dataByTime[i].sdnb);
    }
    var TotalUploadValue = 0;
    for (i = 0; i < this.state.dataByTime.length; i++) {
      TotalUploadValue =
        TotalUploadValue + parseInt(this.state.dataByTime[i].sunb);
    }

    // console.log('TotalDownloadValue = ', TotalDownloadValue);
    // console.log('TotalUploadValue = ', TotalUploadValue);

    var downloadByHost = [];
    for (i = 0; i < this.state.dataByHost.length; i++) {
      var policyColorHandle;
      if (policyColors[this.state.dataByHost[i].hostName] === undefined) {
        policyColorHandle = policyColors['Other'];
      } else {
        policyColorHandle = policyColors[this.state.dataByHost[i].hostName];
      }
      // if (this.state.dataByHost[i].hostName !== 'Other') {
      var obj = {
        value: parseInt(this.state.dataByHost[i].sdnb),
        label: this.state.dataByHost[i].hostName,
        svg: {
          // fill: policyColors[this.state.dataByHost[i].hostName],
          fill: policyColorHandle,
        },
      };
      downloadByHost.push(obj);
      // }
    }

    var uploadByHost = [];
    for (i = 0; i < this.state.dataByHost.length; i++) {
      var policyColorHandle;
      if (policyColors[this.state.dataByHost[i].hostName] === undefined) {
        policyColorHandle = policyColors['Other'];
      } else {
        policyColorHandle = policyColors[this.state.dataByHost[i].hostName];
      }

      // if (this.state.dataByHost[i].hostName !== 'Other') {
      var obj = {
        value: parseInt(this.state.dataByHost[i].sunb),
        label: this.state.dataByHost[i].hostName,
        svg: {
          // fill: policyColors[this.state.dataByHost[i].hostName],
          fill: policyColorHandle,
        },
      };
      uploadByHost.push(obj);
      // }
    }

    var backgroundColorToday;
    if (this.state.isToday) {
      backgroundColorToday = {backgroundColor: '#05a9e1'};
    } else {
      backgroundColorToday = {backgroundColor: 'grey'};
    }

    var backgroundColorlastMonth;
    if (this.state.isLastMonth) {
      backgroundColorlastMonth = {backgroundColor: '#05a9e1'};
    } else {
      backgroundColorlastMonth = {backgroundColor: 'grey'};
    }

    var backgroundColorlastWeek;
    if (this.state.isLastWeek) {
      backgroundColorlastWeek = {backgroundColor: '#05a9e1'};
    } else {
      backgroundColorlastWeek = {backgroundColor: 'grey'};
    }

    // console.log('data_dwn = ', data_dwn);

    var ydataMax = this.convertBytes(
      Math.max.apply(null, data_dwn) * (1024 * 1024),
    );
    var YaxisData = [0, ydataMax.value];
    // console.log('data_upd = ', data_upd);

    return (
      <View style={styles.container}>
        <View style={styles.headingView}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Cloud')}>
            <Image
              source={logoConst['arrow']}
              style={styles.logo_image_header}></Image>
          </TouchableOpacity>
          <Text style={{fontSize: 32, color: 'white'}}>Statistics</Text>
          {/* <View></View> */}
          <Image
            source={logoConst['blackArrow']}
            style={[styles.logo_image_header]}></Image>
        </View>

        {this.state.isLoading ? (
          <LoadingSpinner></LoadingSpinner>
        ) : (
          <View style={styles.mainView}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                // borderWidth: 1,
                // borderColor: 'white',
                marginVertical: Math.round(screenHeight * 0.01),
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  width: Math.round(screenWidth * 0.98),
                  marginVertical: Math.round(screenHeight * 0.01),
                  // borderWidth: 1,
                  // borderColor: 'white',
                }}>
                <TouchableOpacity
                  onPress={() =>
                    this.buttonpressCallAPI(
                      this.props.route.params.deviceID,
                      'today',
                      this.props.route.params.token,
                    )
                  }>
                  <View style={[styles.ButtonView, backgroundColorToday]}>
                    <Text
                      style={{
                        fontSize: 16,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Today
                    </Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    this.buttonpressCallAPI(
                      this.props.route.params.deviceID,
                      'last1month',
                      this.props.route.params.token,
                    )
                  }>
                  <View style={[styles.ButtonView, backgroundColorlastMonth]}>
                    <Text
                      style={{
                        fontSize: 16,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Last Month
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.buttonpressCallAPI(
                    this.props.route.params.deviceID,
                    'last1week',
                    this.props.route.params.token,
                  )
                }>
                <View style={[styles.ButtonView, backgroundColorlastWeek]}>
                  <Text
                    style={{fontSize: 16, color: 'white', fontWeight: 'bold'}}>
                    Last Week
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <ScrollView showsVerticalScrollIndicator={false}>
              {/* <GroupedBarChart datatest={data}></GroupedBarChart> */}
              <View
                style={{
                  flexDirection: 'column',
                  marginVertical: Math.round(screenHeight * 0.02),
                  width: Math.round(screenWidth * 0.98),
                  // borderColor: 'red',
                  // borderWidth: 1,
                  backgroundColor: 'black',
                }}>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize: 20, color: 'white'}}>
                    Download/Upload Data
                  </Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'white',
                    marginVertical: Math.round(screenHeight * 0.01),
                    borderRadius: Math.round(screenHeight * 0.015),
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: Math.round(screenHeight * 0.35),
                      width: Math.round(screenWidth * 0.98),
                      padding: Math.round(screenHeight * 0.01),
                    }}>
                    <YAxis
                      style={{
                        height: Math.round(screenHeight * 0.35),
                        width: Math.round(screenWidth * 0.07),
                        marginRight: Math.round(screenHeight * 0.01),
                        borderWidth: 2,
                        borderRightColor: 'black',
                        borderTopColor: 'white',
                        borderBottomColor: 'white',
                        borderLeftColor: 'white',
                      }}
                      data={YaxisData}
                      contentInset={{
                        top: Math.round(screenHeight * 0.01),
                        bottom: Math.round(screenHeight * 0.04),
                      }}
                      numberOfTicks={5}
                      svg={{
                        fill: 'grey',
                        fontSize: 10,
                      }}
                      formatLabel={(data) => `${data}${ydataMax.label}`}
                    />

                    <View
                      style={{
                        flex: 1,
                      }}>
                      <GroupedBarChart
                        data_dwn={data_dwn}
                        data_upd={data_upd}></GroupedBarChart>
                    </View>
                  </View>
                  <XAxis
                    style={{
                      marginHorizontal: Math.round(screenWidth * 0.08),
                      marginTop: Math.round(screenHeight * 0.01),
                      width: Math.round(screenWidth * 0.84),
                      height: Math.round(screenHeight * 0.03),
                      paddingTop: Math.round(screenHeight * 0.01),
                      borderWidth: 2,
                      borderTopColor: 'black',
                      borderRightColor: 'white',
                      borderBottomColor: 'white',
                      borderLeftColor: 'white',
                    }}
                    data={data_dwn}
                    numberOfTicks={7}
                    formatLabel={(data) => `${data}`}
                    contentInset={{
                      left: Math.round(screenHeight * 0.01),
                      right: Math.round(screenHeight * 0.01),
                    }}
                    svg={{
                      fill: 'grey',
                      fontSize: 10,
                    }}
                  />
                  <View
                    style={{
                      flexDirection: 'column',
                      marginVertical: Math.round(screenHeight * 0.01),
                      marginHorizontal: Math.round(screenWidth * 0.07),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <ColorIndicator name={'Download'} />
                      <Text>
                        {this.convertBytes(TotalDownloadValue).value +
                          this.convertBytes(TotalDownloadValue).label}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <ColorIndicator name={'Upload'} />
                      <Text>
                        {this.convertBytes(TotalUploadValue).value +
                          this.convertBytes(TotalUploadValue).label}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: Math.round(screenHeight * 0.03),
                  }}>
                  <Text style={{fontSize: 20, color: 'black'}}>
                    Download/Upload Data Per Application
                  </Text>
                </View>
                <View
                  style={{
                    justifyContent: 'flex-start',

                    marginTop: Math.round(screenHeight * 0.03),
                  }}>
                  <Text style={{fontSize: 16, color: 'white'}}>Download</Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'white',
                    marginTop: Math.round(screenHeight * 0.03),
                    borderRadius: Math.round(screenHeight * 0.015),
                  }}>
                  <HorizontaBarChartWithYAxis
                    data={downloadByHost}></HorizontaBarChartWithYAxis>
                </View>

                <View
                  style={{
                    justifyContent: 'flex-start',

                    marginTop: Math.round(screenHeight * 0.03),
                  }}>
                  <Text style={{fontSize: 16, color: 'white'}}>Upload</Text>
                </View>
                <View
                  style={{
                    backgroundColor: 'white',
                    marginTop: Math.round(screenHeight * 0.03),
                    borderRadius: Math.round(screenHeight * 0.015),
                  }}>
                  <HorizontaBarChartWithYAxis
                    data={uploadByHost}></HorizontaBarChartWithYAxis>
                </View>
              </View>
            </ScrollView>

            {/* <Text style={{fontSize: 24, color: 'white'}}>Statistics Screen</Text> */}
          </View>
        )}
      </View>
    );
  }
}

export default StatScreen;

class ColorIndicator extends React.PureComponent {
  render() {
    var policyColorHandle;
    if (policyColors[this.props.name] === undefined) {
      policyColorHandle = policyColors['Other'];
    } else {
      policyColorHandle = policyColors[this.props.name];
    }

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          marginVertical: Math.round(screenHeight * 0.005),
        }}>
        <View
          style={{
            height: Math.round(screenHeight * 0.02),
            width: Math.round(screenHeight * 0.02),
            borderRadius: Math.round(screenHeight * 0.01),
            // backgroundColor: policyColors[this.props.name],
            backgroundColor: policyColorHandle,
            marginHorizontal: Math.round(screenWidth * 0.02),
          }}
        />
        <Text>{this.props.name}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  mainView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    // backgroundColor: 'white',
    flex: 1,
    width: Math.round(screenWidth * 0.98),
    // borderColor: 'white',
    // borderWidth: 1,
  },

  ButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'red',
  },
});

const policyColors = {
  AmazonPrime: '#0F79AF',
  Facebook: '#159BF3',
  Instagram: '#944195',
  Netflix: '#E2B014',
  Skype: '#FA76D1',
  Slack: '#4B154C',
  Vimeo: '#19B7EA',
  Youtube: '#F80100',
  Iperf3_Client_1: '#54D06E',
  Iperf3_Client_2: '#EE4266',
  Other: '#AA00F0',
  Zoom: '#C288F7',
  GoogleMeet: '#0B645A',
  CiscoWebex: '#014E72',
  GitHub: '#aaa',
  MicrosoftTeams: '#5255AA',
  Minecraft: '#B7ABAB',
  Download: 'rgb(134, 65, 244)',
  Upload: '#05a9e1',
  Dropbox: '#00FF00',
  FaceTime: '#0000FF',
};

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  RefreshControl,
  ScrollView,
} from 'react-native';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const {logoConst} = require('../../constant/Model');
import LoadingSpinner from '../LoadingSpinner';
// import {ScrollView} from 'react-native-gesture-handler';
import {ProgressCircle} from 'react-native-svg-charts';

class UsageScreen extends React.PureComponent {
  state = {
    refreshing: false,
    isloading: true,
    sqlData: [],
  };

  constructor(props) {
    super(props);
    console.log('constructor UsageScreen.js');
    const {token, deviceID} = this.props.route.params;
    console.log('constructor UsageScreen.js deviceID = ', deviceID);

    this.callAppUsageAPI(deviceID, token);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount UsageScreen.js');
  }

  refresh = () => {
    console.log('UsageScreen refresh');
    const {token, deviceID} = this.props.route.params;

    this.setState({
      refreshing: true,
    });

    this.callAppUsageAPI(deviceID, token);
  };

  callAppUsageAPI = (deviceID, token) => {
    var myHeaders = new Headers();
    myHeaders.append('jwt-token', token);
    var raw = '';

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    var timeOut;
    timeOut = setTimeout(() => {
      this.setState({
        refreshing: false,
      });
    }, 8000);

    fetch(
      'https://rezrvwebapi.tantiv4.com/modes/app-usage/?deviceId=' + deviceID,
      requestOptions,
    )
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var resultJSON = JSON.parse(result);
        if (resultJSON && resultJSON.sqlData && resultJSON.sqlData.length > 0) {
          // console.log('sqlData Len = ', resultJSON.sqlData.length);
          this.setState({
            isloading: false,
            sqlData: resultJSON.sqlData,
          });
          clearTimeout(timeOut);
          this.setState({
            refreshing: false,
          });
        } else {
          this.setState({
            refreshing: false,
            isloading: false,
            sqlData: [],
          });
        }
      })
      .catch((error) => {
        console.log('error', error);
        clearTimeout(timeOut);
        this.setState({
          refreshing: false,
          isloading: false,
          sqlData: [],
        });
      });
  };

  time_convert(num) {
    num = Math.round(num / 60);
    if (num < 60) {
      var minutes = (num % 60).toString();
      if (minutes.length < 2) minutes = '0' + minutes;
      return '00:' + minutes;
    }
    var hours = Math.floor(num / 60).toString();
    var minutes = (num % 60).toString();
    if (minutes.length < 2) minutes = '0' + minutes;
    return hours + ':' + minutes;
  }

  convertBytestillMB = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else {
      whichBytes = 'MB';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      }
    }

    if (resp < 0.01) resp = 0.01;

    return {resp: resp, whichBytes: whichBytes};
  };

  secondsToHms(d) {
    d = Number(d);

    if (d < 60) {
      var s = Math.floor((d % 3600) % 60);
      var sDisplay = s > 0 ? s + (s === 1 ? 's' : 's') : '';
      return sDisplay;
    }

    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    // var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h === 1 ? 'h ' : 'h ') : '';
    var mDisplay = m > 0 ? m + (m === 1 ? 'm ' : 'm ') : '';
    // var sDisplay = s > 0 ? s + (s === 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay;
  }

  convertBytes = (bytes) => {
    let resp = 0;
    var whichBytes;

    if (Math.round(bytes / (1024 * 1024) > 1024)) {
      whichBytes = 'GB';
    } else if (Math.round(bytes / 1024 > 1024)) {
      whichBytes = 'MB';
    } else if (Math.round(bytes > 1024)) {
      whichBytes = 'KB';
    } else {
      whichBytes = 'B';
    }

    if (!bytes) {
      resp = 0;
    } else {
      if (whichBytes == 'GB') {
        resp = (bytes / (1024 * 1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'MB') {
        resp = (bytes / (1024 * 1024)).toFixed(2);
      } else if (whichBytes == 'KB') {
        resp = (bytes / 1024).toFixed(2);
      } else if (whichBytes == 'B') {
        resp = bytes;
      }
    }

    return resp + whichBytes;
  };

  render() {
    var {isloading, sqlData} = this.state;

    sqlData = sqlData.filter(
      (item) =>
        item.hostName !== 'Other' &&
        item.hostName !== 'Iperf3_Client_1' &&
        item.hostName !== 'Iperf3_Client_2',
    );

    sqlData.sort(function (a, b) {
      return b.elapse - a.elapse;
    });

    console.log('sqlData Len = ', sqlData.length);
    // console.log('sqlData = ', sqlData);

    var sqlData = sqlData.map((obj) => {
      obj.elapse = parseInt(obj.elapse);
      obj.sdnb = parseInt(obj.sdnb);
      obj.sunb = parseInt(obj.sunb);
      return obj;
    });
    // console.log('sqlData = ', sqlData);

    var totalTime = 0,
      totalDownload = 0,
      totalUpload = 0;

    for (i = 0; i < sqlData.length; i++) {
      totalTime = totalTime + sqlData[i].elapse;
      totalDownload = totalDownload + sqlData[i].sdnb;
      totalUpload = totalUpload + sqlData[i].sunb;
    }

    // console.log('sqlData = ', sqlData);
    // console.log('totalTime = ', totalTime);
    // console.log('totalDownload = ', totalDownload);
    // console.log('totalUpload = ', totalUpload);

    return (
      <View style={styles.container}>
        {isloading ? (
          <LoadingSpinner></LoadingSpinner>
        ) : (
          <View>
            <View style={styles.headingView}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Cloud')}>
                <Image
                  source={logoConst['arrow']}
                  style={styles.logo_image_header}></Image>
              </TouchableOpacity>
              <Text style={{fontSize: 32, color: 'white'}}>Usage</Text>
              <Image
                source={logoConst['blackArrow']}
                style={[styles.logo_image_header]}></Image>
            </View>

            <ScrollView
              showsVerticalScrollIndicator={false}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.refresh}
                />
              }
              style={styles.scrollView}>
              {sqlData.length === 0 ? (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    height: Math.round(screenHeight * 0.8),
                  }}>
                  <Text style={{color: 'white', fontSize: 24}}>
                    No data available
                  </Text>
                </View>
              ) : (
                <View style={styles.mainView}>
                  {sqlData.map((eachData, index) => (
                    <View
                      key={index}
                      style={{
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        flexDirection: 'row',
                      }}>
                      {logoConst[eachData.hostName + '_usage'] === undefined ? (
                        <Image
                          source={logoConst['others_white']}
                          style={styles.logo_image_usage}></Image>
                      ) : (
                        <Image
                          source={logoConst[eachData.hostName + '_usage']}
                          style={styles.logo_image_usage}></Image>
                      )}
                      <View
                        style={{
                          justifyContent: 'space-around',
                          alignItems: 'center',
                          flexDirection: 'column',
                          width: Math.round(screenWidth * 0.8),
                          // borderWidth: 1,
                          // borderColor: 'white',
                        }}>
                        <View
                          style={{
                            width: Math.round(screenWidth * 0.8),
                            marginBottom: Math.round(screenHeight * 0.005),
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            flexDirection: 'row',
                            // borderWidth: 1,
                            // borderColor: 'white',
                          }}>
                          <Text style={{fontSize: 24, color: 'white'}}>
                            {eachData.hostName}
                          </Text>
                          {/* <Text style={{fontSize: 20, color: 'white'}}>
                            {this.secondsToHms(eachData.elapse)}
                          </Text> */}
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            width: Math.round(screenWidth * 0.8),
                            marginHorizontal: Math.round(screenWidth * 0.01),
                          }}>
                          {/* thre items in col*/}

                          <View
                            style={{
                              flexDirection: 'column',
                              justifyContent: 'space-around',
                              alignItems: 'center',
                              // width: Math.round(screenWidth * 0.2),
                              height: Math.round(screenHeight * 0.14),
                              // borderWidth: 1,
                              // borderColor: 'white',
                            }}>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              Time
                            </Text>
                            <View>
                              <ProgressCircle
                                style={{
                                  height: Math.round(screenHeight * 0.08),
                                  width: Math.round(screenWidth * 0.2),
                                }}
                                progress={parseFloat(
                                  eachData.elapse / totalTime,
                                )
                                  .toFixed(2)
                                  .toString()}
                                progressColor={'green'}
                                startAngle={-Math.PI * 0.8}
                                endAngle={Math.PI * 0.8}
                              />
                              <Text
                                style={{
                                  position: 'absolute',
                                  top: Math.round(screenHeight * 0.03),
                                  alignSelf: 'center',
                                  fontSize: 16,
                                  color: 'white',
                                }}>
                                {this.time_convert(eachData.elapse)}
                              </Text>
                            </View>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              HH:MM
                            </Text>
                          </View>

                          <View
                            style={{
                              flexDirection: 'column',
                              justifyContent: 'space-around',
                              alignItems: 'center',
                              // width: Math.round(screenWidth * 0.2),
                              height: Math.round(screenHeight * 0.14),
                              // borderWidth: 1,
                              // borderColor: 'white',
                            }}>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              Download
                            </Text>
                            <View>
                              <ProgressCircle
                                style={{
                                  height: Math.round(screenHeight * 0.08),
                                  width: Math.round(screenWidth * 0.2),
                                }}
                                progress={parseFloat(
                                  eachData.sdnb / totalDownload,
                                )
                                  .toFixed(2)
                                  .toString()}
                                progressColor={'blue'}
                                startAngle={-Math.PI * 0.8}
                                endAngle={Math.PI * 0.8}
                              />
                              <Text
                                style={{
                                  position: 'absolute',
                                  top: Math.round(screenHeight * 0.03),
                                  alignSelf: 'center',
                                  fontSize: 16,
                                  color: 'white',
                                }}>
                                {this.convertBytestillMB(eachData.sdnb).resp}
                              </Text>
                            </View>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              {
                                this.convertBytestillMB(eachData.sdnb)
                                  .whichBytes
                              }
                            </Text>
                          </View>

                          <View
                            style={{
                              flexDirection: 'column',
                              justifyContent: 'space-around',
                              alignItems: 'center',
                              // width: Math.round(screenWidth * 0.2),
                              height: Math.round(screenHeight * 0.14),
                              // borderWidth: 1,
                              // borderColor: 'white',
                            }}>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              Upload
                            </Text>
                            <View>
                              <ProgressCircle
                                style={{
                                  height: Math.round(screenHeight * 0.08),
                                  width: Math.round(screenWidth * 0.2),
                                }}
                                progress={parseFloat(
                                  eachData.sunb / totalUpload,
                                )
                                  .toFixed(2)
                                  .toString()}
                                progressColor={'red'}
                                startAngle={-Math.PI * 0.8}
                                endAngle={Math.PI * 0.8}
                              />
                              <Text
                                style={{
                                  position: 'absolute',
                                  top: Math.round(screenHeight * 0.03),
                                  alignSelf: 'center',
                                  fontSize: 16,
                                  color: 'white',
                                }}>
                                {this.convertBytestillMB(eachData.sunb).resp}
                              </Text>
                            </View>
                            <Text style={{fontSize: 16, color: 'white'}}>
                              {
                                this.convertBytestillMB(eachData.sunb)
                                  .whichBytes
                              }
                            </Text>
                          </View>
                        </View>

                        {index !== sqlData.length - 1 ? (
                          <View
                            style={{
                              width: Math.round(screenWidth * 0.85),
                              height: 1,
                              borderColor: 'white',
                              borderWidth: 1,
                              marginVertical: Math.round(screenHeight * 0.01),
                            }}></View>
                        ) : (
                          <View></View>
                        )}
                      </View>
                    </View>
                  ))}
                </View>
              )}
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}

export default UsageScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  headingView: {
    height: Math.round(screenHeight * 0.07),
    width: Math.round(screenWidth * 0.95),
    marginTop: Math.round(screenHeight * 0.04),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  logo_image_header: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  scrollView: {},
  mainView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginVertical: Math.round(screenHeight * 0.03),
  },

  logo_image_usage: {
    width: Math.round(screenHeight * 0.07),
    height: Math.round(screenHeight * 0.07),
    resizeMode: 'contain',
    // alignSelf: 'baseline',
    alignSelf: 'flex-start',
    marginRight: Math.round(screenWidth * 0.02),
    padding: Math.round(screenWidth * 0.03),
  },
});

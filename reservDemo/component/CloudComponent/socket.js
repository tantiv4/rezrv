import openSocket from 'socket.io-client';
let socket = null;

export function connectToSocket(tokenVal, cb) {
  console.log('connectToSocket');
  if (socket === null) {
    console.log('connectToSocket initialize when socket is null');
    socket = openSocket('https://rezrvwebapi.tantiv4.com/', {
      transportOptions: {
        polling: {
          extraHeaders: {
            token: tokenVal,
          },
        },
      },
      transports: ['polling'],
    });

    socket.on('connect', () => {
      console.log('socket connect');
      socket.emit('join_chat_group');
    });

    socket.on('DEVICE_UPDATE', function (data) {
      console.log('Socket DEVICE_UPDATE');
      cb(data);
    });

    socket.on('UPDATE_POLICY_NETWORK', function (data) {
      console.log('Socket UPDATE_POLICY_NETWORK');
    });

    socket.on('DEVICE_DATA', function (data) {
      console.log('Socket DEVICE_DATA');
      cb(data);
    });

    socket.on('connect_failed', function (data) {
      console.log('Socket connect_failed');
    });
    socket.on('error', function (data) {
      console.log('Socket connect_failed');
    });
  }
}

export default function () {
  return socket;
}

export function sendMessage(event, data) {
  if (socket) {
    console.log('sendMessage = ', event);
    socket.emit(event, data);
    socket = null;
  }
}

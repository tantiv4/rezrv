import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';

import Modal from 'react-native-modal';

import {ScrollView as GestureHandlerScrollView} from 'react-native-gesture-handler';

import {connect} from 'react-redux';

import ToggleSwitch from 'toggle-switch-react-native';

const {
  ServicesConst,
  logoConst,
  multiNameService,
  modifiedRESTResult,
} = require('../../constant/Model');

var CustomModeService = [];

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const mainBoxHeight = Math.round(screenHeight * 0.35);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeMenu: () =>
      dispatch({
        type: 'CLOSE_MENU',
      }),
    openModal: (val) =>
      dispatch({
        type: 'OPEN_MODAL',
        mode: val,
      }),
    ServiceUpdate: (val) =>
      dispatch({
        type: 'SERVICE_UPDATE',
        service: val,
      }),
  };
}

class CustomMode extends React.PureComponent {
  state = {
    isUpdated: false,
    isModalVisible: false,
    modalNameCustom: '',
  };

  updateServicesConstqos = (value, index, array) => {
    var qkey = value.qkey.split('//')[0];
    // console.log('qkey = ', qkey);
    var newState = ServicesConst.HomeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  callGetAPIqos = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(logoConst.URL + '/rest/q', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        var modifiedResult = this.tsvJSON(result);
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log(
            'custommode modifiedResult length qos= ',
            modifiedResult.length,
          );
          for (i = 0; i < ServicesConst.HomeModeService.length; i++) {
            ServicesConst.HomeModeService[i].isActive = false;
          }
          for (i = 0; i < ServicesConst.OfficeModeService.length; i++) {
            ServicesConst.OfficeModeService[i].isActive = false;
          }
          modifiedResult.forEach(this.updateServicesConstqos);
          modifiedRESTResult['modifiedRESTResQosArr'] = modifiedResult;

          this.setState({isUpdated: !this.state.isUpdated});
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  callGetAPI = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };
    var timeOut;

    timeOut = setTimeout(() => {
      this.setState({isUpdated: !this.state.isUpdated});
    }, 6000);

    fetch(logoConst.URL + '/rest/r', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var modifiedResult = this.makeUnique(this.tsvJSON(result));
        // console.log('modifiedResult = ', modifiedResult);
        // console.log('modifiedResult isArray = ', Array.isArray(modifiedResult));
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log(
            'custommode modifiedResult length = ',
            modifiedResult.length,
          );
          modifiedResult.forEach(this.updateServicesConst);
          this.extraUIupdate();
          this.callGetAPIqos();
          clearTimeout(timeOut);
          for (var item in multiNameService) {
            multiNameService[item].length = 0;
          }
          modifiedResult.forEach(this.updateMultiNameVal);
          // console.log('multiNameService = ', multiNameService);
          modifiedRESTResult['modifiedRESTResArr'] = modifiedResult;
        }
      })
      .catch((error) => {
        console.log('error', error);
        clearTimeout(timeOut);
        this.setState({isUpdated: !this.state.isUpdated});
      });
  };

  extraUIupdate = () => {
    var ifallFalseHomeMode = true;
    var ifallFalseOfficeMode = true;
    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseHomeMode = false;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseOfficeMode = false;
      }
    });

    if (ifallFalseHomeMode) {
      ServicesConst.HomeModeValue = false;
    } else {
      ServicesConst.HomeModeValue = true;
    }

    if (ifallFalseOfficeMode) {
      ServicesConst.OfficeModeValue = false;
    } else {
      ServicesConst.OfficeModeValue = true;
    }
  };

  updateMultiNameVal = (value, index, array) => {
    // console.log('updateMultiNameVal value[' + index + '].name = ', value.name);
    ServicesConst.HomeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });

    ServicesConst.OfficeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });
  };

  updateServicesConst = (value, index, array) => {
    // console.log('value[' + index + '].name = ', value.name);
    // console.log('value[' + index + '].dmin = ', value.dmin);

    var setValue = value.dmin !== '0' ? true : false;
    var setblockedVal = value.dmax === '10.24' ? true : false;
    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  tsvJSON = (tsv) => {
    const lines = tsv.split('\n');
    const headers = lines
      .shift()
      .split('\t')
      .map((el) => el.trim());
    return lines.map((line) => {
      const data = line.split('\t').map((el) => el.trim());
      return headers.reduce((obj, nextKey, index) => {
        obj[nextKey] =
          typeof data[index] === 'string' ? data[index].trim() : data[index];
        return obj;
      }, {});
    });
  };

  makeUnique = (data) => {
    let resp = [];
    data.forEach((company) => {
      let foundIndex = resp.findIndex((el) => el.name === company.name);
      if (foundIndex === -1) {
        resp.push(company);
      }
    });
    return resp;
  };

  postRestAPIServiceForBlock = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    if (value === true) {
      data += '&dmax=0.01K';
    } else {
      data += '&dmax=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);

    // this.callGetAPI();
  };

  postRestAPIService = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    var valueBandWidth = (parseFloat(logoConst.BandWidth) * 0.125).toString();
    if (value === true) {
      data += '&dmin=' + valueBandWidth + 'M&umin=2M';
    } else {
      data += '&dmin=0&umin=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);

    // this.callGetAPI();
  };

  toggleSwitchpress = (val, service) => {
    console.log('CustomMode toggleSwitchpress = ', val, service);

    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === service ? {...obj, value: val.isOn} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === service ? {...obj, value: val.isOn} : obj,
    );
    ServicesConst.OfficeModeService = newState;

    var ifallFalseHomeMode = true;
    var ifallFalseOfficeMode = true;
    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseHomeMode = false;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseOfficeMode = false;
      }
    });

    if (ifallFalseHomeMode) {
      ServicesConst.HomeModeValue = false;
    } else {
      ServicesConst.HomeModeValue = true;
    }
    if (ifallFalseOfficeMode) {
      ServicesConst.OfficeModeValue = false;
    } else {
      ServicesConst.OfficeModeValue = true;
    }

    if (multiNameService[service] !== undefined) {
      for (i = 0; i < multiNameService[service].length; i++) {
        this.postRestAPIService(multiNameService[service][i], val.isOn);
      }
    } else {
      for (i = 0; i < CustomModeService.length; i++) {
        if (CustomModeService[i].name === service) {
          for (j = 0; j < CustomModeService[i].extraVal.length; j++) {
            this.postRestAPIService(CustomModeService[i].extraVal[j], val.isOn);
          }
          break;
        }
      }
    }

    this.props.ServiceUpdate(service + val.isOn.toString());
    this.callGetAPI();
  };

  handlePressOut = () => {
    // console.log('CustomMode handlePressOut');
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  handlePressOutSub = () => {
    // console.log('CustomMode handlePressOutSub');
  };

  toggleBlock = (isBlockedVal, serviceName) => {
    // console.log('toggleBlock isBlockedVal = ', isBlockedVal);
    // console.log('toggleBlock serviceName = ', serviceName);

    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === serviceName ? {...obj, isBlocked: isBlockedVal} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === serviceName ? {...obj, isBlocked: isBlockedVal} : obj,
    );
    ServicesConst.OfficeModeService = newState;

    // console.log('toggleBlock ServicesConst = ', ServicesConst);

    // this.setState({randomVal: serviceName + isBlockedVal.toString()});

    if (multiNameService[serviceName] !== undefined) {
      for (i = 0; i < multiNameService[serviceName].length; i++) {
        this.postRestAPIServiceForBlock(
          multiNameService[serviceName][i],
          isBlockedVal,
        );
      }
    } else {
      for (i = 0; i < CustomModeService.length; i++) {
        if (CustomModeService[i].name === serviceName) {
          for (j = 0; j < CustomModeService[i].extraVal.length; j++) {
            this.postRestAPIServiceForBlock(
              CustomModeService[i].extraVal[j],
              isBlockedVal,
            );
          }
          break;
        }
      }
    }

    this.callGetAPI();
  };

  toggleModal = (value) => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  openModalCustomMode = (serviceName) => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      modalNameCustom: serviceName,
    });
  };

  updateCustomService = (value, index, array) => {
    var isServiceFound = false;
    for (i = 0; i < CustomModeService.length; i++) {
      if (
        value.name !== undefined &&
        value.name.indexOf(CustomModeService[i].name) > -1
      ) {
        // console.log(
        //   'updateCustomService Found value[' + index + '].name = ',
        //   value.name,
        // );
        isServiceFound = true;
        if (CustomModeService[i].isNew && CustomModeService[i].isNew === true) {
          // console.log('is New True value.name = ', value.name);
          CustomModeService[i].extraVal.push(value.name);
          if (CustomModeService[i].isActive === false) {
            for (
              j = 0;
              j < modifiedRESTResult['modifiedRESTResQosArr'].length;
              j++
            ) {
              var qkeyVal = modifiedRESTResult['modifiedRESTResQosArr'][
                j
              ].qkey.split('//')[0];
              if (qkeyVal === value.name) {
                CustomModeService[i].isActive = true;
                break;
              }
            }
          }
        }
      }
    }

    if (value.name !== undefined && isServiceFound === false) {
      // console.log(
      //   'updateCustomService Not Found value[' + index + '].name = ',
      //   value.name,
      // );

      var nameObj = '';
      if (value.name.indexOf('Iperf') > -1) {
        nameObj = value.name;
      } else {
        nameObj = value.name.split('_')[0];

        var isActiveVal = false;
        for (
          j = 0;
          j < modifiedRESTResult['modifiedRESTResQosArr'].length;
          j++
        ) {
          var qkeyVal = modifiedRESTResult['modifiedRESTResQosArr'][
            j
          ].qkey.split('//')[0];
          if (qkeyVal === value.name) {
            isActiveVal = true;
            break;
          }
        }

        var obj = {
          name: nameObj,
          value: value.dmin !== '0' ? true : false,
          isBlocked:
            value.dmax === '10.24' || value.dmax === 10.24 ? true : false,
          isActive: isActiveVal,
          isNew: true,
          extraVal: [value.name],
        };
        CustomModeService.push(obj);
      }
    }
  };

  render() {
    var arr1 = ServicesConst.HomeModeService;
    var arr2 = ServicesConst.OfficeModeService;
    CustomModeService = [...arr1, ...arr2];

    modifiedRESTResult['modifiedRESTResArr'].forEach(this.updateCustomService);

    var isBlockedService = undefined;
    var isNewCheck = false;
    var imageCtrl;
    CustomModeService.forEach((value, index, array) => {
      if (value.name === this.state.modalNameCustom) {
        isBlockedService = value.isBlocked;
        if (value.isNew && value.isNew === true) {
          isNewCheck = true;
        }
      }
    });

    if (isBlockedService) {
      imageCtrl = logoConst.blocked;
    } else {
      imageCtrl = logoConst.unblocked;
    }

    console.log('render of Custom Mode');
    // console.log(' Custom Mode CustomModeService = ', CustomModeService);

    return (
      <View style={styles.containerInner}>
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              // onResponderRelease={this.handlePressOutSub}
              onResponderGrant={this.handlePressOutSub}
              style={styles.settingModalSubViewLogos}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Parental Control
              </Text>
              {isBlockedService ? (
                <Text style={{fontSize: 22, fontWeight: 'bold', color: 'red'}}>
                  Blocked
                </Text>
              ) : (
                <Text
                  style={{fontSize: 22, fontWeight: 'bold', color: 'green'}}>
                  Unblocked
                </Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  width: Math.round(screenWidth * 0.7),
                  paddingLeft: Math.round(screenWidth * 0.07),
                }}>
                {isNewCheck ? (
                  <Text
                    style={{
                      fontSize: Math.round(screenWidth * 0.04),
                      alignSelf: 'center',
                      fontWeight: 'bold',
                      color: 'black',
                    }}>
                    {this.state.modalNameCustom}
                  </Text>
                ) : (
                  <Image
                    source={logoConst[this.state.modalNameCustom]}
                    style={styles.logo_image}
                  />
                )}

                <TouchableOpacity
                  onPress={() =>
                    this.toggleBlock(
                      !isBlockedService,
                      this.state.modalNameCustom,
                    )
                  }>
                  <Image source={imageCtrl} style={styles.logo_image} />
                </TouchableOpacity>
              </View>

              <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                <View style={styles.ModalButtonView}>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>Close</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            // borderWidth: 1,
            // borderColor: 'black',
            width: Math.round(screenWidth * 0.9),
            paddingHorizontal: Math.round(screenWidth * 0.06),
            marginTop: Math.round(screenHeight * 0.04),
          }}>
          <Text style={styles.ModeText}>CUSTOM MODE</Text>
        </View>

        <View
          style={{
            borderWidth: 1,
            // borderColor: 'black',
            // height: 1,
            width: Math.round(screenWidth * 0.85),
            marginTop: Math.round(screenHeight * 0.04),
          }}></View>

        <GestureHandlerScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{alignItems: 'center'}}
          nestedScrollEnabled={true}
          style={{
            // borderColor: 'black',
            // borderWidth: 1,
            width: Math.round(screenWidth * 0.8),
            height: Math.round(screenHeight * 0.58),
            marginBottom: Math.round(screenHeight * 0.003),
          }}>
          {CustomModeService.map((service, index) => (
            <View key={index} style={styles.containerInnerBox}>
              <View style={styles.eachIconBox}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {service.isActive ? (
                    <View
                      style={[
                        styles.circleDot,
                        {backgroundColor: 'green'},
                      ]}></View>
                  ) : (
                    <View style={[styles.circleDot]}></View>
                  )}
                  <TouchableOpacity
                    onLongPress={() => {
                      this.openModalCustomMode(service.name);
                    }}>
                    {service.isNew ? (
                      <View
                        style={{
                          // borderColor: 'black',
                          // borderWidth: 1,
                          width: Math.round(screenWidth * 0.32),
                          height: Math.round(screenHeight * 0.12),
                          marginLeft: Math.round(screenWidth * 0.013),
                          paddingTop: Math.round(screenHeight * 0.05),
                          alignItems: 'baseline',
                        }}>
                        <Text
                          style={{
                            fontSize: Math.round(screenWidth * 0.04),
                            alignSelf: 'center',
                            fontWeight: 'bold',
                            color: 'black',
                          }}>
                          {service.name}
                        </Text>
                      </View>
                    ) : (
                      <Image
                        source={logoConst[service.name]}
                        style={styles.imageView}></Image>
                    )}

                    {service.isBlocked ? (
                      <View style={styles.viewblockedOverlay}>
                        <Image
                          source={logoConst['blockedOverlay']}
                          style={styles.logo_image_blockedOverlay}
                        />
                      </View>
                    ) : (
                      <View></View>
                    )}
                  </TouchableOpacity>
                </View>
                <ToggleSwitch
                  isOn={service.value}
                  onColor="green"
                  offColor="grey"
                  labelStyle={{color: 'black', fontWeight: '900'}}
                  size="large"
                  onToggle={(isOn) =>
                    this.toggleSwitchpress({isOn}, service.name)
                  }
                />
              </View>
            </View>
          ))}
        </GestureHandlerScrollView>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomMode);

const styles = StyleSheet.create({
  containerInner: {
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderWidth: 1,
    // borderColor: 'black',
  },

  ModeText: {
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'italic',
    alignSelf: 'center',
  },

  //   boxInner: {
  //     marginTop: Math.round(screenHeight * 0.005),
  //   },

  containerInnerBox: {
    alignItems: 'center',
    width: Math.round(screenWidth * 0.9),
    height: Math.round(mainBoxHeight * 0.2),
    marginBottom: Math.round(mainBoxHeight * 0.065),
  },
  eachIconBox: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: Math.round(screenWidth * 0.8),
    // marginBottom: Math.round(mainBoxHeight * 0.02),
    // paddingBottom: Math.round(mainBoxHeight * 0.02),
  },
  textParam: {
    // display: 'flex',
    display: 'none',
    fontSize: 22,
    fontWeight: 'bold',
  },

  viewblockedOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo_image_blockedOverlay: {
    width: Math.round(screenHeight * 0.06),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },

  circleDot: {
    height: 14,
    width: 14,
    borderRadius: 7,
    marginHorizontal: Math.round(screenWidth * 0.01),
    borderWidth: 1,
  },

  imageView: {
    // width: Math.round(screenHeight * 0.05),
    // height: Math.round(screenHeight * 0.05),
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.12),
    marginLeft: Math.round(screenWidth * 0.04),
    resizeMode: 'contain',
  },

  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  settingModalSubViewLogos: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.12),
    resizeMode: 'contain',
  },
});

import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

import {connect} from 'react-redux';

import ToggleSwitchHandler from './ToggleSwitchHandler';

const {ServicesConst, logoConst} = require('../../constant/Model');

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const mainBoxHeight = Math.round(screenHeight * 0.35);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeMenu: () =>
      dispatch({
        type: 'CLOSE_MENU',
      }),
    openModal: (val) =>
      dispatch({
        type: 'OPEN_MODAL',
        mode: val,
      }),
  };
}

class HomeMode extends React.PureComponent {
  state = {
    HomeMode: false,
    OfficeMode: false,
    YoutubeMode: false,
    NetflixMode: false,
    ZoomMode: false,
    MicrosoftTeamMode: false,
  };

  render() {
    return (
      <View style={styles.containerInner}>
        {ServicesConst.HomeModeService.map((service, index) => (
          <View key={index} style={[styles.containerInnerbox, styles.boxInner]}>
            <View style={styles.eachIconBox}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {service.isActive ? (
                  <View
                    style={[
                      styles.circleDot,
                      {backgroundColor: 'green'},
                    ]}></View>
                ) : (
                  <View style={[styles.circleDot]}></View>
                )}
                <TouchableOpacity
                  onLongPress={() => {
                    this.props.openModal(service.name);
                  }}>
                  <Image
                    source={logoConst[service.name]}
                    style={styles.imageView}></Image>
                  {service.isBlocked ? (
                    <View style={styles.viewblockedOverlay}>
                      <Image
                        source={logoConst['blockedOverlay']}
                        style={styles.logo_image_blockedOverlay}
                      />
                    </View>
                  ) : (
                    <View></View>
                  )}
                </TouchableOpacity>
              </View>
              <Text style={styles.textParam}>Youtube</Text>
              <ToggleSwitchHandler
                isOnVal={service.value}
                serviceName={service.name}
                serviceMode="HomeMode"></ToggleSwitchHandler>
            </View>
          </View>
        ))}
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeMode);

const styles = StyleSheet.create({
  imageView: {
    // width: Math.round(screenHeight * 0.05),
    // height: Math.round(screenHeight * 0.05),
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.12),
    marginLeft: Math.round(screenWidth * 0.04),
    resizeMode: 'contain',
  },
  containerInner: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  containerInnerbox: {
    width: Math.round(screenWidth * 0.8),
    height: Math.round(mainBoxHeight * 0.3),
  },
  boxInner: {
    marginTop: Math.round(screenHeight * 0.005),
  },
  eachIconBox: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  textParam: {
    // display: 'flex',
    display: 'none',
    fontSize: 22,
    fontWeight: 'bold',
  },

  viewblockedOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo_image_blockedOverlay: {
    width: Math.round(screenHeight * 0.06),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },

  circleDot: {
    height: 14,
    width: 14,
    borderRadius: 7,
    marginHorizontal: Math.round(screenWidth * 0.01),
    borderWidth: 1,
  },
});

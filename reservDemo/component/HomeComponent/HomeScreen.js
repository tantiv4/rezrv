/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  // YellowBox,
  LogBox,
  Image,
  ScrollView,
  TouchableOpacity,
  Animated,
  Easing,
  RefreshControl,
  TextInput,
  AppState,
} from 'react-native';
import styled from 'styled-components';
import {connect} from 'react-redux';

import SideMenu from 'react-native-side-menu';

import {ScrollView as GestureHandlerScrollView} from 'react-native-gesture-handler';

import ModalHandler from './ModalHandler';
import CustomMode from './CustomMode';

import ToggleSwitchHandler from './ToggleSwitchHandler';

const {
  ServicesConst,
  logoConst,
  multiNameService,
  modifiedRESTResult,
} = require('../../constant/Model');

import Menu from './Menu';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const mainBoxHeight = Math.round(screenHeight * 0.35);

var homeScreenHomeMode = [];
var homeScreenOfficeMode = [];

// YellowBox.ignoreWarnings(['Animated: `useNativeDriver` was not specified.']);
LogBox.ignoreLogs(['Animated: `useNativeDriver` was not specified.']);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openMenu: (val) =>
      dispatch({
        type: 'OPEN_MENU',
        mode: val,
      }),
    openModal: (val) =>
      dispatch({
        type: 'OPEN_MODAL',
        mode: val,
      }),
    ServiceUpdate: (val) =>
      dispatch({
        type: 'SERVICE_UPDATE',
        service: val,
      }),
  };
}

class HomeScreen extends React.PureComponent {
  state = {
    scale: new Animated.Value(1),
    opacity: new Animated.Value(1),
    refreshing: false,
    isLeftHomeMode: true,
    isLeftOfficeMode: true,
    isLeftCustomMode: true,
    appState: AppState.currentState,
    isOpenVal: false,
  };

  constructor(props) {
    super(props);
    console.log('HomeScreen constructor');
  }

  componentDidMount() {
    console.log('screenWidth = ', screenWidth);
    console.log('screenHeight = ', screenHeight);

    StatusBar.setBarStyle('dark-content', true);
    if (Platform.OS == 'android') {
      StatusBar.setBarStyle('dark-content');
    }
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    this.setState({appState: nextAppState});
    if (nextAppState === 'background') {
      console.log('App is in Background Mode.');
    }

    if (nextAppState === 'active') {
      console.log('App is in Active Foreground Mode.');
      this.callGetAPI();
    }

    if (nextAppState === 'inactive') {
      console.log('App is in inactive Mode.');
    }
  };

  componentDidUpdate() {
    this.toggleMenu();
  }

  toggleMenu = () => {
    // console.log('toggleMenu Homescreen this.props.action = ', this.props);
    if (this.props.action == 'openMenu') {
      Animated.timing(this.state.scale, {
        toValue: 0.9,
        duration: 300,
        easing: Easing.in(),
      }).start();
      Animated.spring(this.state.opacity, {
        toValue: 0.5,
      }).start();

      StatusBar.setBarStyle('light-content', true);
    }

    if (this.props.action == 'closeMenu') {
      Animated.timing(this.state.scale, {
        toValue: 1,
        duration: 300,
        easing: Easing.in(),
      }).start();
      Animated.spring(this.state.opacity, {
        toValue: 1,
      }).start();

      StatusBar.setBarStyle('dark-content', true);
    }
  };

  updateServicesConstqos = (value, index, array) => {
    var qkey = value.qkey.split('//')[0];
    // console.log('homescreen qkey = ', qkey);
    var newState = ServicesConst.HomeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  callGetAPIqos = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(logoConst.URL + '/rest/q', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        var modifiedResult = this.tsvJSON(result);
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length qos= ', modifiedResult.length);
          for (i = 0; i < ServicesConst.HomeModeService.length; i++) {
            ServicesConst.HomeModeService[i].isActive = false;
          }
          for (i = 0; i < ServicesConst.OfficeModeService.length; i++) {
            ServicesConst.OfficeModeService[i].isActive = false;
          }
          modifiedResult.forEach(this.updateServicesConstqos);
          modifiedRESTResult['modifiedRESTResQosArr'] = modifiedResult;
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  refresh = () => {
    console.log('HomeScreen refresh');
    this.setState({
      refreshing: true,
    });
    this.callGetAPI();
  };

  callGetAPI = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };
    var timeOut;

    timeOut = setTimeout(() => {
      this.setState({
        refreshing: false,
      });
      this.props.navigation.navigate('Home');
    }, 4000);

    fetch(logoConst.URL + '/rest/r', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var modifiedResult = this.makeUnique(this.tsvJSON(result));
        // console.log('modifiedResult = ', modifiedResult);
        // console.log('modifiedResult isArray = ', Array.isArray(modifiedResult));
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length = ', modifiedResult.length);
          modifiedResult.forEach(this.updateServicesConst);
          this.extraUIupdate();
          this.callGetAPIqos();
          clearTimeout(timeOut);
          for (var item in multiNameService) {
            multiNameService[item].length = 0;
          }
          modifiedResult.forEach(this.updateMultiNameVal);
          // console.log('multiNameService = ', multiNameService);
          modifiedRESTResult['modifiedRESTResArr'] = modifiedResult;
          this.setState({
            refreshing: false,
          });
        } else {
          console.log('modifiedResult length = ', modifiedResult.length);
          this.props.navigation.navigate('Home');
        }
      })
      .catch((error) => {
        console.log('error', error);
        clearTimeout(timeOut);
        this.setState({
          refreshing: false,
        });
      });
  };

  extraUIupdate = () => {
    var ifallFalseHomeMode = true;
    var ifallFalseOfficeMode = true;
    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseHomeMode = false;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseOfficeMode = false;
      }
    });

    if (ifallFalseHomeMode) {
      ServicesConst.HomeModeValue = false;
    } else {
      ServicesConst.HomeModeValue = true;
    }

    if (ifallFalseOfficeMode) {
      ServicesConst.OfficeModeValue = false;
    } else {
      ServicesConst.OfficeModeValue = true;
    }
  };

  updateMultiNameVal = (value, index, array) => {
    // console.log('updateMultiNameVal value[' + index + '].name = ', value.name);
    ServicesConst.HomeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });

    ServicesConst.OfficeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });
  };

  updateServicesConst = (value, index, array) => {
    // console.log('value[' + index + '].name = ', value.name);
    // console.log('value[' + index + '].dmin = ', value.dmin);

    var setValue = value.dmin !== '0' ? true : false;
    var setblockedVal = value.dmax === '10.24' ? true : false;
    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  tsvJSON = (tsv) => {
    const lines = tsv.split('\n');
    const headers = lines
      .shift()
      .split('\t')
      .map((el) => el.trim());
    return lines.map((line) => {
      const data = line.split('\t').map((el) => el.trim());
      return headers.reduce((obj, nextKey, index) => {
        obj[nextKey] =
          typeof data[index] === 'string' ? data[index].trim() : data[index];
        return obj;
      }, {});
    });
  };

  makeUnique = (data) => {
    let resp = [];
    data.forEach((company) => {
      let foundIndex = resp.findIndex((el) => el.name === company.name);
      if (foundIndex === -1) {
        resp.push(company);
      }
    });
    return resp;
  };

  handleScrollHomeMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.3)) {
      this.setState({isLeftHomeMode: false});
    } else {
      this.setState({isLeftHomeMode: true});
    }
  };

  handleScrollOfficeMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.3)) {
      this.setState({isLeftOfficeMode: false});
    } else {
      this.setState({isLeftOfficeMode: true});
    }
  };

  handleScrollCustomMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.5)) {
      this.setState({isLeftCustomMode: false});
    } else {
      this.setState({isLeftCustomMode: true});
    }
  };

  updateHomeScreenView = () => {
    var homemodeCount = 0;
    var officemodeCount = 0;

    homeScreenHomeMode.length = 0;
    homeScreenOfficeMode.length = 0;

    // ServicesConst.HomeModeService.forEach((value, index, array) => {
    //   if (homemodeCount < 2) {
    //     homeScreenHomeMode.push(value);
    //     homemodeCount = homemodeCount + 1;
    //   }
    // });
    // ServicesConst.OfficeModeService.forEach((value, index, array) => {
    //   if (officemodeCount < 2) {
    //     homeScreenOfficeMode.push(value);
    //     officemodeCount = officemodeCount + 1;
    //   }
    // });

    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true && homemodeCount < 2) {
        homeScreenHomeMode.push(value);
        homemodeCount = homemodeCount + 1;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true && officemodeCount < 2) {
        homeScreenOfficeMode.push(value);
        officemodeCount = officemodeCount + 1;
      }
    });
    // console.log('homeScreenHomeMode length= ', homeScreenHomeMode.length);
    // console.log('homeScreenOfficeMode length= ', homeScreenOfficeMode.length);

    homemodeCount = 0;
    if (homeScreenHomeMode.length == 0) {
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.value === false && homemodeCount < 2) {
          homeScreenHomeMode.push(value);
          homemodeCount = homemodeCount + 1;
        }
      });
    } else if (homeScreenHomeMode.length == 1) {
      homemodeCount = 0;
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.value === false && homemodeCount < 1) {
          homeScreenHomeMode.push(value);
          homemodeCount = homemodeCount + 1;
        }
      });
    }

    // console.log('homeScreenHomeMode = ', homeScreenHomeMode);

    officemodeCount = 0;
    if (homeScreenOfficeMode.length == 0) {
      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.value === false && officemodeCount < 2) {
          homeScreenOfficeMode.push(value);
          officemodeCount = officemodeCount + 1;
        }
      });
    } else if (homeScreenOfficeMode.length == 1) {
      officemodeCount = 0;
      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.value === false && officemodeCount < 1) {
          homeScreenOfficeMode.push(value);
          officemodeCount = officemodeCount + 1;
        }
      });
    }
    // console.log('homeScreenOfficeMode = ', homeScreenOfficeMode);
  };

  toggleSideMenu = () => {
    // console.log('toggleSideMenu');
    this.setState({
      isOpenVal: !this.state.isOpenVal,
    });
  };

  onChangeSideMenu = (isOpenCheck) => {
    // console.log('onChangeSideMenu isOpenCheck = ', isOpenCheck);
    if (isOpenCheck === false) {
      this.setState({
        isOpenVal: !this.state.isOpenVal,
      });
    }
  };

  postRestAPIServiceDMin = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    if (value === true) {
      data += '&dmin=2M&umin=2M';
    } else {
      data += '&dmin=0&umin=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  postRestAPIServiceDMax = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    if (value === true) {
      data += '&dmax=0.01K';
    } else {
      data += '&dmax=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  setDefaultBeforeTransition = () => {
    console.log('setDefaultBeforeTransition HomeScreen');

    ServicesConst.HomeModeValue = false;
    ServicesConst.OfficeModeValue = false;

    var newState = ServicesConst.HomeModeService.map((obj) => {
      if (obj.value === true) {
        for (i = 0; i < multiNameService[obj.name].length; i++) {
          this.postRestAPIServiceDMin(multiNameService[obj.name][i], false);
        }
        this.props.ServiceUpdate(obj.name + 'false');
      }
      if (obj.isBlocked === true) {
        for (i = 0; i < multiNameService[obj.name].length; i++) {
          this.postRestAPIServiceDMax(multiNameService[obj.name][i], false);
        }
      }
      return {...obj, value: false, isBlocked: false};
    });
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) => {
      if (obj.value === true) {
        for (i = 0; i < multiNameService[obj.name].length; i++) {
          this.postRestAPIServiceDMin(multiNameService[obj.name][i], false);
        }
        this.props.ServiceUpdate(obj.name + 'false');
      }
      if (obj.isBlocked === true) {
        for (i = 0; i < multiNameService[obj.name].length; i++) {
          this.postRestAPIServiceDMax(multiNameService[obj.name][i], false);
        }
      }
      return {...obj, value: false, isBlocked: false};
    });
    ServicesConst.OfficeModeService = newState;
  };

  render() {
    // console.log('Home screen.js  before ServicesConst = ', ServicesConst);
    // console.log('ServicesConst after= ', ServicesConst);
    this.updateHomeScreenView();

    const MenuComponent = (
      <View
        style={{
          flex: 1,
          // flexDirection: 'row',
          backgroundColor: '#F0F0F0',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-around',
            // alignItems: 'center',
            marginTop: Math.round(screenHeight * 0.05),
            width: Math.round(screenWidth * 0.65),
          }}>
          <View
            style={{
              marginVertical: Math.round(screenHeight * 0.03),
              alignSelf: 'center',
            }}>
            <Text
              style={{
                fontSize: 36,
                color: 'black',
              }}></Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              this.setDefaultBeforeTransition();
              setTimeout(() => {
                this.callGetAPI();
                setTimeout(() => {
                  logoConst['isDemoMode'] = true;
                  this.props.navigation.navigate('Home', {isDemo: false});
                }, 250);
              }, 250);
            }}>
            <View
              style={{
                backgroundColor: '#DCDCDC',
                marginTop: Math.round(screenHeight * 0.01),
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={logoConst['demo_enter']}
                style={[styles.logo_image_sidemenu]}
              />
              <Text
                style={{
                  fontSize: 20,
                  color: 'black',
                  alignSelf: 'center',
                }}>
                Enter Demo Mode
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-around',
            // alignItems: 'center',
            // marginTop: Math.round(screenHeight * 0.05),
            width: Math.round(screenWidth * 0.65),
          }}>
          <View
            style={{
              marginVertical: Math.round(screenHeight * 0.01),
              alignSelf: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: 'black',
              }}></Text>
          </View>

          <TouchableOpacity onPress={() => this.props.openModal('settings')}>
            <View
              style={{
                backgroundColor: '#DCDCDC',
                marginTop: Math.round(screenHeight * 0.01),
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={logoConst['bandwidth']}
                style={[styles.logo_image_sidemenu]}
              />
              <Text
                style={{
                  fontSize: 20,
                  color: 'black',
                  alignSelf: 'center',
                }}>
                Set Priority Bandwidth
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );

    return (
      <SideMenu
        onChange={(isOpen) => this.onChangeSideMenu(isOpen)}
        menuPosition={'right'}
        isOpen={this.state.isOpenVal}
        menu={MenuComponent}>
        <View style={styles.container}>
          <Menu></Menu>
          <ModalHandler></ModalHandler>
          <AnimatedContainer
            style={{
              transform: [{scale: this.state.scale}],
              opactiy: this.state.opacity,
            }}>
            {/* <ImageBackground
            source={require('../assets/Wave04.png')}></ImageBackground> */}
            <SafeAreaView>
              <View style={styles.headingView}>
                <View style={styles.headingSubView}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Cloud')}>
                    <Image
                      source={logoConst['cloud']}
                      style={styles.rezrv_heading_image_side}
                    />
                  </TouchableOpacity>
                  <Image
                    source={require('../../Icons/rezrvHeading.png')}
                    style={styles.rezrv_heading_image}
                  />
                  {/* <TouchableOpacity
                    onPress={() => this.props.openModal('settings')}> */}
                  <TouchableOpacity onPress={this.toggleSideMenu}>
                    <Image
                      source={require('../../Icons/settings.png')}
                      style={styles.rezrv_heading_image_side}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              {/* <NestedScrollView ></NestedScrollView> */}

              <ScrollView
                onScroll={this.handleScrollCustomMode}
                scrollEventThrottle={16}
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this.refresh}
                    />
                  }
                  style={styles.scrollView}>
                  <View style={[styles.box, styles.boxHome]}>
                    {/* <ImageBackgroundBox
                  source={require('../assets/background2.jpg')}></ImageBackgroundBox> */}
                    <View style={styles.seperateModeContainer}>
                      <Text style={styles.ModeText}>HOME MODE</Text>
                      <ToggleSwitchHandler
                        isOnVal={ServicesConst.HomeModeValue}
                        serviceName="HomeMode"
                        serviceMode="HomeMode"></ToggleSwitchHandler>
                    </View>

                    <View style={styles.containerInner}>
                      <GestureHandlerScrollView
                        onScroll={this.handleScrollHomeMode}
                        scrollEventThrottle={16}
                        horizontal={true}
                        nestedScrollEnabled={true}
                        style={styles.scrollInnerContainer}
                        showsHorizontalScrollIndicator={false}>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          {/* <ImageBackgroundBox
                        source={require('../assets/background2.jpg')}></ImageBackgroundBox> */}
                          {homeScreenHomeMode[0].isActive ? (
                            <View
                              style={[
                                styles.circleDotEmpty,
                                {backgroundColor: 'green'},
                              ]}></View>
                          ) : (
                            <View style={[styles.circleDotEmpty]}></View>
                          )}
                          <View style={styles.eachIconBox}>
                            <TouchableOpacity
                              onLongPress={() =>
                                this.props.openModal(homeScreenHomeMode[0].name)
                              }>
                              <Image
                                source={logoConst[homeScreenHomeMode[0].name]}
                                style={styles.logo_image}
                              />
                              {homeScreenHomeMode[0].isBlocked ? (
                                <View style={styles.viewblockedOverlay}>
                                  <Image
                                    source={logoConst['blockedOverlay']}
                                    style={styles.logo_image_blockedOverlay}
                                  />
                                </View>
                              ) : (
                                <View></View>
                              )}
                            </TouchableOpacity>
                            <ToggleSwitchHandler
                              isOnVal={homeScreenHomeMode[0].value}
                              serviceName={homeScreenHomeMode[0].name}
                              serviceMode="HomeMode"></ToggleSwitchHandler>
                          </View>
                        </View>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          {/* <ImageBackgroundBox
                        source={require('../assets/background2.jpg')}></ImageBackgroundBox> */}
                          {homeScreenHomeMode[1].isActive ? (
                            <View
                              style={[
                                styles.circleDotEmpty,
                                {backgroundColor: 'green'},
                              ]}></View>
                          ) : (
                            <View style={[styles.circleDotEmpty]}></View>
                          )}
                          <View style={styles.eachIconBox}>
                            <TouchableOpacity
                              onLongPress={() =>
                                this.props.openModal(homeScreenHomeMode[1].name)
                              }>
                              <Image
                                source={logoConst[homeScreenHomeMode[1].name]}
                                style={styles.logo_image}
                              />
                              {homeScreenHomeMode[1].isBlocked ? (
                                <View style={styles.viewblockedOverlay}>
                                  <Image
                                    source={logoConst['blockedOverlay']}
                                    style={styles.logo_image_blockedOverlay}
                                  />
                                </View>
                              ) : (
                                <View></View>
                              )}
                            </TouchableOpacity>
                            <ToggleSwitchHandler
                              isOnVal={homeScreenHomeMode[1].value}
                              serviceName={homeScreenHomeMode[1].name}
                              serviceMode="HomeMode"></ToggleSwitchHandler>
                          </View>
                        </View>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          <TouchableOpacity
                            onPress={() => this.props.openMenu('Home Mode')}
                            style={styles.IconBoxMore}>
                            <View>
                              <Text style={styles.MoreText}>More...</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </GestureHandlerScrollView>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingBottom: Math.round(screenHeight * 0.01),
                      }}>
                      {this.state.isLeftHomeMode ? (
                        <View
                          style={[
                            styles.circleDot,
                            styles.circleDotBackGroud,
                          ]}></View>
                      ) : (
                        <View style={styles.circleDot}></View>
                      )}
                      <View
                        style={[
                          styles.circleDot,
                          styles.circleDotBackGroud,
                        ]}></View>
                      {this.state.isLeftHomeMode ? (
                        <View style={styles.circleDot}></View>
                      ) : (
                        <View
                          style={[
                            styles.circleDot,
                            styles.circleDotBackGroud,
                          ]}></View>
                      )}
                    </View>
                  </View>

                  <View style={[styles.box, styles.boxOffice]}>
                    <View style={styles.seperateModeContainer}>
                      <Text style={styles.ModeText}>OFFICE MODE</Text>
                      <ToggleSwitchHandler
                        isOnVal={ServicesConst.OfficeModeValue}
                        serviceName="OfficeMode"
                        serviceMode="OfficeMode"></ToggleSwitchHandler>
                    </View>
                    <View style={styles.containerInner}>
                      <GestureHandlerScrollView
                        onScroll={this.handleScrollOfficeMode}
                        scrollEventThrottle={16}
                        style={styles.scrollInnerContainer}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          {homeScreenOfficeMode[0].isActive ? (
                            <View
                              style={[
                                styles.circleDotEmpty,
                                {backgroundColor: 'green'},
                              ]}></View>
                          ) : (
                            <View style={[styles.circleDotEmpty]}></View>
                          )}
                          <View style={styles.eachIconBox}>
                            <TouchableOpacity
                              onLongPress={() =>
                                this.props.openModal(
                                  homeScreenOfficeMode[0].name,
                                )
                              }>
                              <Image
                                source={logoConst[homeScreenOfficeMode[0].name]}
                                style={styles.logo_image}
                              />
                              {homeScreenOfficeMode[0].isBlocked ? (
                                <View style={styles.viewblockedOverlay}>
                                  <Image
                                    source={logoConst['blockedOverlay']}
                                    style={styles.logo_image_blockedOverlay}
                                  />
                                </View>
                              ) : (
                                <View></View>
                              )}
                            </TouchableOpacity>
                            <ToggleSwitchHandler
                              isOnVal={homeScreenOfficeMode[0].value}
                              serviceName={homeScreenOfficeMode[0].name}
                              serviceMode="OfficeMode"></ToggleSwitchHandler>
                          </View>
                        </View>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          {homeScreenOfficeMode[1].isActive ? (
                            <View
                              style={[
                                styles.circleDotEmpty,
                                {backgroundColor: 'green'},
                              ]}></View>
                          ) : (
                            <View style={[styles.circleDotEmpty]}></View>
                          )}
                          <View style={styles.eachIconBox}>
                            <TouchableOpacity
                              onLongPress={() =>
                                this.props.openModal(
                                  homeScreenOfficeMode[1].name,
                                )
                              }>
                              <Image
                                source={logoConst[homeScreenOfficeMode[1].name]}
                                style={styles.logo_image}
                              />
                              {homeScreenOfficeMode[1].isBlocked ? (
                                <View style={styles.viewblockedOverlay}>
                                  <Image
                                    source={logoConst['blockedOverlay']}
                                    style={styles.logo_image_blockedOverlay}
                                  />
                                </View>
                              ) : (
                                <View></View>
                              )}
                            </TouchableOpacity>
                            <ToggleSwitchHandler
                              isOnVal={homeScreenOfficeMode[1].value}
                              serviceName={homeScreenOfficeMode[1].name}
                              serviceMode="OfficeMode"></ToggleSwitchHandler>
                          </View>
                        </View>
                        <View
                          style={[styles.containerInnerbox, styles.boxInner]}>
                          <TouchableOpacity
                            onPress={() => this.props.openMenu('Office Mode')}
                            style={styles.IconBoxMore}>
                            <View>
                              <Text style={styles.MoreText}>More...</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </GestureHandlerScrollView>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingBottom: Math.round(screenHeight * 0.01),
                      }}>
                      {this.state.isLeftOfficeMode ? (
                        <View
                          style={[
                            styles.circleDot,
                            styles.circleDotBackGroud,
                          ]}></View>
                      ) : (
                        <View style={styles.circleDot}></View>
                      )}
                      <View
                        style={[
                          styles.circleDot,
                          styles.circleDotBackGroud,
                        ]}></View>
                      {this.state.isLeftOfficeMode ? (
                        <View style={styles.circleDot}></View>
                      ) : (
                        <View
                          style={[
                            styles.circleDot,
                            styles.circleDotBackGroud,
                          ]}></View>
                      )}
                    </View>
                  </View>
                </ScrollView>
                <GestureHandlerScrollView
                  showsVerticalScrollIndicator={false}
                  // nestedScrollEnabled={true}

                  nestedScrollEnabled={false}
                  refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this.refresh}
                    />
                  }
                  style={styles.scrollView}>
                  <View
                    style={{
                      marginTop: Math.round(screenHeight * 0.03),
                      marginBottom: Math.round(screenHeight * 0.01),
                      alignItems: 'center',
                      marginHorizontal: Math.round(screenWidth * 0.05),
                    }}>
                    <View
                      style={{
                        width: Math.round(screenWidth * 0.9),
                        height: Math.round(screenHeight * 0.73),
                        backgroundColor: 'white',
                        borderRadius: Math.round(screenHeight * 0.03),
                      }}>
                      <CustomMode data={this.state.refreshing}></CustomMode>
                    </View>
                  </View>
                </GestureHandlerScrollView>
              </ScrollView>

              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: Math.round(screenWidth * 0.98),
                }}>
                {this.state.isLeftCustomMode ? (
                  <View
                    style={{
                      marginVertical: Math.round(screenHeight * 0.005),
                      flexDirection: 'row',
                    }}>
                    <View
                      style={[
                        styles.circleDotCustom,
                        styles.circleDotCustomBackGroud,
                      ]}></View>
                    <View style={styles.circleDotCustom}></View>
                  </View>
                ) : (
                  <View
                    style={{
                      marginVertical: Math.round(screenHeight * 0.005),
                      flexDirection: 'row',
                    }}>
                    <View style={styles.circleDotCustom}></View>
                    <View
                      style={[
                        styles.circleDotCustom,
                        styles.circleDotCustomBackGroud,
                      ]}></View>
                  </View>
                )}

                <View style={styles.footerView}>
                  <Image
                    source={require('../../Icons/rezrv.png')}
                    style={styles.rezrv_logo_image}
                  />
                  <View style={styles.footerTextView}>
                    <Text style={styles.footerSubText}> POWERED BY</Text>
                    <Image
                      source={require('../../Icons/tantiv4.png')}
                      style={styles.tantiv4_logo_image}
                    />
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </AnimatedContainer>
        </View>
      </SideMenu>
    );
  }
}

// const ImageBackground = styled.Image`
//   position: absolute;
//   width: 100%;
//   height: 100%;
// `;

// const ImageBackgroundBox = styled.Image`
//   position: absolute;
//   width: 100%;
//   height: 100%;
//   border-radius: ${Math.round(mainBoxHeight * 0.05)};
// `;

const BelowMainContainer = styled.View`
  flex: 1;
  background-color: #000000;
  justify-content: space-around;
  /* border-top-left-radius: 10px;
  border-top-right-radius: 10px; */
`;
const AnimatedContainer = Animated.createAnimatedComponent(BelowMainContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#F0F0F0',
  },
  headingView: {
    // flexDirection: 'row',
    // justifyContent: 'space-between',
    // alignItems: 'flex-end',
    // marginTop: Math.round(screenHeight * 0.01),
    marginHorizontal: Math.round(screenWidth * 0.04),
    // borderColor: 'white',
    // borderWidth: 1,
  },
  headingSubView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  rezrv_heading_image: {
    width: Math.round(screenWidth * 0.45),
    height: Math.round(screenHeight * 0.05),
    resizeMode: 'contain',
    // borderColor: 'white',
    // borderWidth: 1,
    // marginRight: Math.round(screenWidth * 0.14),
  },
  rezrv_heading_image_side: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  ModalInput: {
    width: Math.round(screenWidth * 0.25),
    height: Math.round(screenHeight * 0.05),
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  scrollView: {
    width: screenWidth,
  },
  footerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: Math.round(screenHeight * 0.01),
    width: Math.round(screenWidth * 0.98),
  },
  footerTextView: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerSubText: {
    color: '#F0F0F0',
    fontSize: 12,
  },
  seperateModeContainer: {
    flexDirection: 'row',
    marginHorizontal: Math.round(screenWidth * 0.05),
    marginTop: Math.round(screenHeight * 0.04),
    justifyContent: 'space-between',
  },
  ModeText: {
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'italic',
    alignSelf: 'center',
  },
  box: {
    height: mainBoxHeight,
    marginHorizontal: Math.round(screenWidth * 0.05),
  },
  boxHome: {
    // backgroundColor: '#F0F0F0',
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  boxOffice: {
    // backgroundColor: '#E0E0E0',
    // backgroundColor: '#F0F0F0',
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginBottom: Math.round(screenHeight * 0.01),
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },

  logo_image: {
    width: Math.round(screenHeight * 0.13),
    height: Math.round(screenHeight * 0.13),
    resizeMode: 'contain',
  },
  rezrv_logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },
  tantiv4_logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  containerInner: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerInnerbox: {
    width: Math.round(screenWidth * 0.4),
    height: Math.round(mainBoxHeight * 0.5),
    borderRadius: Math.round(mainBoxHeight * 0.05),
  },
  boxInner: {
    borderWidth: 1,
    marginLeft: Math.round(screenWidth * 0.015),
  },
  eachIconBox: {
    flex: 1,
    margin: Math.round(screenWidth * 0.01),
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  IconBoxMore: {
    flex: 1,
    margin: Math.round(screenWidth * 0.01),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MoreText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  scrollInnerContainer: {
    flexDirection: 'row',
    marginHorizontal: Math.round(screenWidth * 0.03),
  },

  circleDot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: Math.round(screenWidth * 0.01),
  },

  circleDotBackGroud: {
    backgroundColor: 'black',
  },

  circleDotCustom: {
    height: 10,
    width: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'white',
    marginHorizontal: Math.round(screenWidth * 0.01),
  },

  circleDotCustomBackGroud: {
    backgroundColor: 'white',
  },

  circleDotEmpty: {
    position: 'absolute',
    top: Math.round(screenHeight * 0.01),
    right: Math.round(screenWidth * 0.01),
    height: 14,
    width: 14,
    borderRadius: 7,
    marginHorizontal: Math.round(screenWidth * 0.01),
    borderWidth: 1,
  },

  viewblockedOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo_image_blockedOverlay: {
    width: Math.round(screenHeight * 0.06),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },

  logo_image_sidemenu: {
    width: Math.round(screenHeight * 0.03),
    height: Math.round(screenHeight * 0.03),
    resizeMode: 'contain',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

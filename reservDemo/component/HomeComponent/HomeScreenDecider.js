import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import Modal from 'react-native-modal';

import LoadingSpinner from '../LoadingSpinner';
import HomeScreen from './HomeScreen';
import HomeScreenDemo from './HomeScreenDemo';
import ModalHandler from './ModalHandler';

// import PTRView from 'react-native-pull-to-refresh';

const {
  ServicesConst,
  logoConst,
  multiNameService,
  modifiedRESTResult,
} = require('../../constant/Model');
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const mainBoxHeight = Math.round(screenHeight * 0.55);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openModal: (val) =>
      dispatch({
        type: 'OPEN_MODAL',
        mode: val,
      }),
  };
}

class HomeScreenDecider extends React.PureComponent {
  state = {
    loading: true,
    data: 'empty',
    // isModalVisibleForDemoSelection: true,
    isModalVisibleForDemoSelection: false,
    isDemoMode: false,
  };

  componentDidMount() {
    this.callGetAPI('http://10.3.12.1:8008');
    this.callGetAPI('http://192.168.1.1:8008');
  }

  updateServicesConstqos = (value, index, array) => {
    var qkey = value.qkey.split('//')[0];
    // console.log('homescreen decider qkey = ', qkey);
    var newState = ServicesConst.HomeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  callGetAPIqos = (URLVal) => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(URLVal + '/rest/q', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        var modifiedResult = this.tsvJSON(result);
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length qos= ', modifiedResult.length);
          for (i = 0; i < ServicesConst.HomeModeService.length; i++) {
            ServicesConst.HomeModeService[i].isActive = false;
          }
          for (i = 0; i < ServicesConst.OfficeModeService.length; i++) {
            ServicesConst.OfficeModeService[i].isActive = false;
          }
          modifiedResult.forEach(this.updateServicesConstqos);
          modifiedRESTResult['modifiedRESTResQosArr'] = modifiedResult;
        }
        this.setState({
          loading: false,
          data: result,
        });
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  callGetAPI = (URLVal) => {
    this.setState({
      loading: true,
    });

    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };
    var timeOut;

    timeOut = setTimeout(() => {
      if (this.state.data === 'empty' || this.state.data === 'NULL') {
        this.setState({
          loading: false,
          data: 'NULL',
        });
      }
    }, 4000);

    fetch(URLVal + '/rest/r', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var modifiedResult = this.makeUnique(this.tsvJSON(result));
        // console.log('modifiedResult = ', modifiedResult);
        // console.log('modifiedResult isArray = ', Array.isArray(modifiedResult));
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length = ', modifiedResult.length);
          modifiedResult.forEach(this.updateServicesConst);
          this.extraUIupdate();
          logoConst['URL'] = URLVal;
          this.callGetAPIqos(URLVal);
          clearTimeout(timeOut);
          for (var item in multiNameService) {
            multiNameService[item].length = 0;
          }
          modifiedResult.forEach(this.updateMultiNameVal);
          // console.log('multiNameService = ', multiNameService);
          modifiedRESTResult['modifiedRESTResArr'] = modifiedResult;
          // this.setState({
          //   loading: false,
          //   data: result,
          // });
        }
      })
      .catch((error) => {
        console.log('error', error);
        clearTimeout(timeOut);
        // this.setState({
        //   loading: false,
        //   data: 'NULL',
        // });
        if (this.state.data === 'empty' || this.state.data === 'NULL') {
          this.setState({
            loading: false,
            data: 'NULL',
          });
        }
      });
  };

  extraUIupdate = () => {
    var ifallFalseHomeMode = true;
    var ifallFalseOfficeMode = true;
    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseHomeMode = false;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseOfficeMode = false;
      }
    });

    if (ifallFalseHomeMode) {
      ServicesConst.HomeModeValue = false;
    } else {
      ServicesConst.HomeModeValue = true;
    }

    if (ifallFalseOfficeMode) {
      ServicesConst.OfficeModeValue = false;
    } else {
      ServicesConst.OfficeModeValue = true;
    }
  };

  updateMultiNameVal = (value, index, array) => {
    // console.log('updateMultiNameVal value[' + index + '].name = ', value.name);
    ServicesConst.HomeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });

    ServicesConst.OfficeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });
  };

  updateServicesConst = (value, index, array) => {
    // console.log('value[' + index + '].name = ', value.name);
    // console.log('value[' + index + '].dmin = ', value.dmin);

    var setValue = value.dmin !== '0' ? true : false;
    var setblockedVal = value.dmax === '10.24' ? true : false;
    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  tsvJSON = (tsv) => {
    const lines = tsv.split('\n');
    const headers = lines
      .shift()
      .split('\t')
      .map((el) => el.trim());
    return lines.map((line) => {
      const data = line.split('\t').map((el) => el.trim());
      return headers.reduce((obj, nextKey, index) => {
        obj[nextKey] =
          typeof data[index] === 'string' ? data[index].trim() : data[index];
        return obj;
      }, {});
    });
  };

  makeUnique = (data) => {
    let resp = [];
    data.forEach((company) => {
      let foundIndex = resp.findIndex((el) => el.name === company.name);
      if (foundIndex === -1) {
        resp.push(company);
      }
    });
    return resp;
  };

  // refresh = () => {
  //   console.log('HomeScreenDecider refresh');
  // };

  handlePressOut = () => {
    this.setState({
      isModalVisibleForDemoSelection: !this.state
        .isModalVisibleForDemoSelection,
      isDemoMode: false,
    });
  };

  handlePressOutSub = () => {};

  toggleModalisDemo = (val) => {
    if (val === 'Continue') {
      this.setState({
        isModalVisibleForDemoSelection: !this.state
          .isModalVisibleForDemoSelection,
        isDemoMode: false,
      });
    } else {
      this.setState({
        isModalVisibleForDemoSelection: !this.state
          .isModalVisibleForDemoSelection,
        isDemoMode: true,
      });
    }
  };

  updateDemoMode = (val) => {
    this.setState({
      isDemoMode: val,
    });
  };

  render() {
    const {loading, data} = this.state;
    console.log('Homescreendecider render ', logoConst['isDemoMode']);
    this.updateDemoMode(logoConst['isDemoMode']);
    console.log('loading = ', loading);
    var IPAddress = logoConst['URL'];
    IPAddress = IPAddress.split('://')[1];
    IPAddress = IPAddress.split(':')[0];
    // var testData = 'NULL';
    return (
      <View style={styles.container}>
        <Modal
          backdropOpacity={1}
          isVisible={this.state.isModalVisibleForDemoSelection}>
          <View
            onStartShouldSetResponder={(evt) => true}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              onResponderGrant={this.handlePressOutSub}
              style={styles.settingModalSubView}>
              <Text style={{color: 'black', fontSize: 16}}>
                Select Normal to Proceed in
              </Text>
              <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>
                Normal Mode
              </Text>

              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  marginTop: Math.round(screenHeight * 0.02),
                }}>
                Select Demo to Proceed in
              </Text>
              <Text style={{color: 'black', fontSize: 16, fontWeight: 'bold'}}>
                Demo Mode
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => this.toggleModalisDemo('Continue')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                      Normal Mode
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.toggleModalisDemo('Demo')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                      Demo Mode
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <ModalHandler></ModalHandler>
        {/* <PTRView onRefresh={this.refresh}> */}
        {loading ? (
          <LoadingSpinner />
        ) : data !== 'NULL' && !this.state.isModalVisibleForDemoSelection ? (
          this.state.isDemoMode ? (
            <HomeScreenDemo navigation={this.props.navigation} />
          ) : (
            <HomeScreen navigation={this.props.navigation} />
          )
        ) : (
          <View style={styles.boxContainer}>
            <View
              style={{
                position: 'absolute',
                top: Math.round(screenHeight * 0.02),
                right: Math.round(screenWidth * 0.03),
              }}>
              <TouchableOpacity
                onPress={() => this.props.openModal('settings_IPset')}>
                <Image
                  source={logoConst['settings_black']}
                  style={{
                    resizeMode: 'contain',
                    height: Math.round(screenHeight * 0.05),
                    width: Math.round(screenHeight * 0.05),
                  }}
                />
              </TouchableOpacity>
            </View>

            <Image
              source={logoConst['wrongNetwork']}
              style={styles.logo_image}
            />
            <Text style={styles.textView}>You are not connected to</Text>
            <Text style={styles.textView}>REZRV Network</Text>
            <Text style={styles.textView}></Text>
            <Text style={styles.textView}>REZRV Gateway IP is {IPAddress}</Text>
            <Text style={styles.textView}>Click on Settings to change</Text>
            <Text style={styles.textView}>Default REZRV IP</Text>
            <Text style={styles.textView}></Text>
            <Text style={styles.textView}>Please connect to REZRV router</Text>
            <Text style={styles.textView}>and retry or check Cloud</Text>
            <Text style={styles.textView}>Analytics</Text>
            <Text style={styles.textView}></Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: Math.round(screenWidth * 0.7),
              }}>
              <TouchableOpacity
                onPress={() => this.callGetAPI(logoConst['URL'])}>
                <View style={styles.buttonView}>
                  <Text style={styles.textView}>Retry</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Cloud')}>
                <View style={styles.buttonView}>
                  <Text style={styles.textView}>Cloud</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
        {/* </PTRView> */}
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreenDecider);
// export default HomeScreenDecider;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000000',
  },
  textView: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  buttonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  boxContainer: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },

  logo_image: {
    width: Math.round(screenHeight * 0.13),
    height: Math.round(screenHeight * 0.13),
    resizeMode: 'contain',
  },

  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: Math.round(screenHeight * 0.3),
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
});

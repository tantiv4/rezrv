/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  Dimensions,
  // YellowBox,
  LogBox,
  Image,
  ScrollView,
  TouchableOpacity,
  Animated,
  Easing,
  RefreshControl,
  TextInput,
  AppState,
  Button,
} from 'react-native';
import styled from 'styled-components';
import {connect} from 'react-redux';
import ToggleSwitch from 'toggle-switch-react-native';
import Modal from 'react-native-modal';
import RadioForm from 'react-native-simple-radio-button';

import SideMenu from 'react-native-side-menu';

import AsyncStorage from '@react-native-community/async-storage';

import {ScrollView as GestureHandlerScrollView} from 'react-native-gesture-handler';

import ModalHandler from './ModalHandler';
import CustomMode from './CustomMode';

import ToggleSwitchHandler from './ToggleSwitchHandler';

const {
  ServicesConst,
  logoConst,
  multiNameService,
  modifiedRESTResult,
} = require('../../constant/Model');

import Menu from './Menu';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const mainBoxHeight = Math.round(screenHeight * 0.225);

var homeScreenHomeMode = [];
var homeScreenOfficeMode = [];
var lastTap = null;

// YellowBox.ignoreWarnings(['Animated: `useNativeDriver` was not specified.']);
LogBox.ignoreLogs(['Animated: `useNativeDriver` was not specified.']);

var testingmodeDivider = [
  {name: 'Youtube', mode: 'HOME MODE'},
  {name: 'Zoom', mode: 'OFFICE MODE'},
  {name: 'Netflix', mode: 'LEISURE MODE'},
];

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openMenu: (val) =>
      dispatch({
        type: 'OPEN_MENU',
        mode: val,
      }),
    openModal: (val) =>
      dispatch({
        type: 'OPEN_MODAL',
        mode: val,
      }),
    ServiceUpdate: (val) =>
      dispatch({
        type: 'SERVICE_UPDATE',
        service: val,
      }),
  };
}

class HomeScreenDemo extends React.PureComponent {
  state = {
    scale: new Animated.Value(1),
    opacity: new Animated.Value(1),
    refreshing: false,
    isLeftHomeMode: true,
    isLeftOfficeMode: true,
    isLeftCustomMode: true,
    appState: AppState.currentState,
    isModalVisible: false,
    modalName: '',
    modeUpdate: '',
    isUpdated: false,
    isOpenVal: false,
  };

  constructor(props) {
    super(props);
    console.log('HomeScreenDemo constructor');
    this.getDmaxFromAsyncStorage();
  }

  getDmaxFromAsyncStorage = () => {
    (async () => {
      var dmaxValueAsyncStorage = await AsyncStorage.getItem('@dmaxDemo');
      if (dmaxValueAsyncStorage !== null) {
        console.log(
          'getDmaxFromAsyncStorage dmaxValueAsyncStorage = ',
          dmaxValueAsyncStorage,
        );
        logoConst['dmaxDemoMode'] = dmaxValueAsyncStorage;
      } else {
        await AsyncStorage.setItem('@dmaxDemo', logoConst['dmaxDemoMode']);
      }
    })();
  };

  componentDidMount() {
    console.log('screenWidth = ', screenWidth);
    console.log('screenHeight = ', screenHeight);

    StatusBar.setBarStyle('dark-content', true);
    if (Platform.OS == 'android') {
      StatusBar.setBarStyle('dark-content');
    }
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    this.setState({appState: nextAppState});
    if (nextAppState === 'background') {
      console.log('App is in Background Mode.');
    }

    if (nextAppState === 'active') {
      console.log('App is in Active Foreground Mode.');
      this.callGetAPI();
    }

    if (nextAppState === 'inactive') {
      console.log('App is in inactive Mode.');
    }
  };

  componentDidUpdate() {
    this.toggleMenu();
  }

  toggleMenu = () => {
    // console.log('toggleMenu Homescreen this.props.action = ', this.props);
    if (this.props.action == 'openMenu') {
      Animated.timing(this.state.scale, {
        toValue: 0.9,
        duration: 300,
        easing: Easing.in(),
      }).start();
      Animated.spring(this.state.opacity, {
        toValue: 0.5,
      }).start();

      StatusBar.setBarStyle('light-content', true);
    }

    if (this.props.action == 'closeMenu') {
      Animated.timing(this.state.scale, {
        toValue: 1,
        duration: 300,
        easing: Easing.in(),
      }).start();
      Animated.spring(this.state.opacity, {
        toValue: 1,
      }).start();

      StatusBar.setBarStyle('dark-content', true);
    }
  };

  updateServicesConstqos = (value, index, array) => {
    var qkey = value.qkey.split('//')[0];
    // console.log('homescreen Demo qkey = ', qkey);
    var newState = ServicesConst.HomeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      // obj.name === qkey ? {...obj, isActive: true} : obj,
      qkey.indexOf(obj.name) > -1 ? {...obj, isActive: true} : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  callGetAPIqos = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(logoConst.URL + '/rest/q', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        var modifiedResult = this.tsvJSON(result);
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length qos= ', modifiedResult.length);
          for (i = 0; i < ServicesConst.HomeModeService.length; i++) {
            ServicesConst.HomeModeService[i].isActive = false;
          }
          for (i = 0; i < ServicesConst.OfficeModeService.length; i++) {
            ServicesConst.OfficeModeService[i].isActive = false;
          }
          modifiedResult.forEach(this.updateServicesConstqos);
          modifiedRESTResult['modifiedRESTResQosArr'] = modifiedResult;
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  refresh = () => {
    console.log('HomeScreenDemo refresh');
    this.setState({
      refreshing: true,
    });
    this.callGetAPI();
  };

  callGetAPI = () => {
    var requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };
    var timeOut;

    timeOut = setTimeout(() => {
      this.setState({
        refreshing: false,
      });
      this.props.navigation.navigate('Home');
    }, 4000);

    fetch(logoConst.URL + '/rest/r', requestOptions)
      .then((response) => response.text())
      .then((result) => {
        // console.log(result);
        var modifiedResult = this.makeUnique(this.tsvJSON(result));
        // console.log('modifiedResult = ', modifiedResult);
        // console.log('modifiedResult isArray = ', Array.isArray(modifiedResult));
        if (Array.isArray(modifiedResult) && modifiedResult.length !== 0) {
          console.log('modifiedResult length = ', modifiedResult.length);
          modifiedResult.forEach(this.updateServicesConst);
          this.extraUIupdate();
          this.callGetAPIqos();
          clearTimeout(timeOut);
          for (var item in multiNameService) {
            multiNameService[item].length = 0;
          }
          modifiedResult.forEach(this.updateMultiNameVal);
          // console.log('multiNameService = ', multiNameService);
          modifiedRESTResult['modifiedRESTResArr'] = modifiedResult;
          this.setState({
            refreshing: false,
            isUpdated: !this.state.isUpdated,
          });
        } else {
          console.log('modifiedResult length = ', modifiedResult.length);
          this.props.navigation.navigate('Home');
        }
      })
      .catch((error) => {
        console.log('error', error);
        clearTimeout(timeOut);
        this.setState({
          refreshing: false,
        });
      });
  };

  extraUIupdate = () => {
    var ifallFalseHomeMode = true;
    var ifallFalseOfficeMode = true;
    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseHomeMode = false;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true) {
        ifallFalseOfficeMode = false;
      }
    });

    if (ifallFalseHomeMode) {
      ServicesConst.HomeModeValue = false;
    } else {
      ServicesConst.HomeModeValue = true;
    }

    if (ifallFalseOfficeMode) {
      ServicesConst.OfficeModeValue = false;
    } else {
      ServicesConst.OfficeModeValue = true;
    }
  };

  updateMultiNameVal = (value, index, array) => {
    // console.log('updateMultiNameVal value[' + index + '].name = ', value.name);
    ServicesConst.HomeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });

    ServicesConst.OfficeModeService.forEach((item, index) => {
      if (value.name !== undefined && value.name.indexOf(item.name) > -1) {
        multiNameService[item.name].push(value.name);
      }
    });
  };

  updateServicesConst = (value, index, array) => {
    // console.log('value[' + index + '].name = ', value.name);
    // console.log('value[' + index + '].dmin = ', value.dmin);

    var setValue = value.dmin !== '0' ? true : false;
    var setblockedVal = value.dmax === '10.24' ? true : false;
    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === value.name
        ? {...obj, value: setValue, isBlocked: setblockedVal}
        : obj,
    );
    ServicesConst.OfficeModeService = newState;
  };

  tsvJSON = (tsv) => {
    const lines = tsv.split('\n');
    const headers = lines
      .shift()
      .split('\t')
      .map((el) => el.trim());
    return lines.map((line) => {
      const data = line.split('\t').map((el) => el.trim());
      return headers.reduce((obj, nextKey, index) => {
        obj[nextKey] =
          typeof data[index] === 'string' ? data[index].trim() : data[index];
        return obj;
      }, {});
    });
  };

  makeUnique = (data) => {
    let resp = [];
    data.forEach((company) => {
      let foundIndex = resp.findIndex((el) => el.name === company.name);
      if (foundIndex === -1) {
        resp.push(company);
      }
    });
    return resp;
  };

  handleScrollHomeMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.3)) {
      this.setState({isLeftHomeMode: false});
    } else {
      this.setState({isLeftHomeMode: true});
    }
  };

  handleScrollOfficeMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.3)) {
      this.setState({isLeftOfficeMode: false});
    } else {
      this.setState({isLeftOfficeMode: true});
    }
  };

  handleScrollCustomMode = (event) => {
    if (event.nativeEvent.contentOffset.x > Math.round(screenWidth * 0.5)) {
      this.setState({isLeftCustomMode: false});
    } else {
      this.setState({isLeftCustomMode: true});
    }
  };

  updateHomeScreenDemoView = () => {
    var homemodeCount = 0;
    var officemodeCount = 0;

    homeScreenHomeMode.length = 0;
    homeScreenOfficeMode.length = 0;

    ServicesConst.HomeModeService.forEach((value, index, array) => {
      if (value.value === true && homemodeCount < 2) {
        homeScreenHomeMode.push(value);
        homemodeCount = homemodeCount + 1;
      }
    });
    ServicesConst.OfficeModeService.forEach((value, index, array) => {
      if (value.value === true && officemodeCount < 2) {
        homeScreenOfficeMode.push(value);
        officemodeCount = officemodeCount + 1;
      }
    });
    // console.log('homeScreenHomeMode length= ', homeScreenHomeMode.length);
    // console.log('homeScreenOfficeMode length= ', homeScreenOfficeMode.length);

    homemodeCount = 0;
    if (homeScreenHomeMode.length == 0) {
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.value === false && homemodeCount < 2) {
          homeScreenHomeMode.push(value);
          homemodeCount = homemodeCount + 1;
        }
      });
    } else if (homeScreenHomeMode.length == 1) {
      homemodeCount = 0;
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.value === false && homemodeCount < 1) {
          homeScreenHomeMode.push(value);
          homemodeCount = homemodeCount + 1;
        }
      });
    }

    // console.log('homeScreenHomeMode = ', homeScreenHomeMode);

    officemodeCount = 0;
    if (homeScreenOfficeMode.length == 0) {
      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.value === false && officemodeCount < 2) {
          homeScreenOfficeMode.push(value);
          officemodeCount = officemodeCount + 1;
        }
      });
    } else if (homeScreenOfficeMode.length == 1) {
      officemodeCount = 0;
      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.value === false && officemodeCount < 1) {
          homeScreenOfficeMode.push(value);
          officemodeCount = officemodeCount + 1;
        }
      });
    }
    // console.log('homeScreenOfficeMode = ', homeScreenOfficeMode);
  };

  toggleModal = (value) => {
    if (value === 'cancel') {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
      });
    } else {
      console.log('modeUpdate = ', this.state.modeUpdate);
      var modeUpdateSplit = this.state.modeUpdate;
      modeUpdateSplit = modeUpdateSplit.split('::');
      for (i = 0; i < testingmodeDivider.length; i++) {
        if (testingmodeDivider[i].mode === modeUpdateSplit[1]) {
          testingmodeDivider[i].name = modeUpdateSplit[0];
        }
      }
      console.log('testingmodeDivider = ', testingmodeDivider);
      this.setState({
        isModalVisible: !this.state.isModalVisible,
      });
    }
  };

  handlePressOut = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  handlePressOutSub = () => {};

  openModalforSel = (modeVal) => {
    console.log('openModalforSel modeVal = ', modeVal);

    this.setState({
      isModalVisible: !this.state.isModalVisible,
      modalName: modeVal,
    });
  };

  postRestAPIServiceDMin = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    var valueBandWidth = (parseFloat(logoConst.BandWidth) * 0.125).toString();
    if (value === true) {
      // data += '&dmin=' + valueBandWidth + 'M&umin=2M';
      // data += '&dmin=10M&umin=2M';
      // data += '&dmin=2M&umin=2M';
      data += '&dmin=5M&umin=5M';
    } else {
      data += '&dmin=0&umin=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  postRestAPIServiceDMax = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    if (value === true) {
      // data += '&dmax=0.01K';
      // data += '&dmax=50K';
      // data += '&dmax=' + logoConst['dmaxDemoMode'] + 'K';
      data +=
        '&dmax=' +
        logoConst['dmaxDemoMode'] +
        'K&umax=' +
        logoConst['dmaxDemoMode'] +
        'K';
    } else {
      // data += '&dmax=0';
      data += '&dmax=0&umax=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  toggleSwitchpress = (val, service) => {
    console.log('toggleSwitchpress = ', val, service);

    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === service ? {...obj, value: val.isOn} : {...obj, value: false},
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === service ? {...obj, value: val.isOn} : {...obj, value: false},
    );
    ServicesConst.OfficeModeService = newState;

    for (i = 0; i < multiNameService[service].length; i++) {
      this.postRestAPIServiceDMax(multiNameService[service][i], false);
    }

    for (i = 0; i < multiNameService[service].length; i++) {
      this.postRestAPIServiceDMin(multiNameService[service][i], val.isOn);
    }

    for (i = 0; i < testingmodeDivider.length; i++) {
      if (testingmodeDivider[i].name !== service) {
        if (val.isOn) {
          for (
            j = 0;
            j < multiNameService[testingmodeDivider[i].name].length;
            j++
          ) {
            this.postRestAPIServiceDMin(
              multiNameService[testingmodeDivider[i].name][j],
              false,
            );
          }
        }
        for (
          j = 0;
          j < multiNameService[testingmodeDivider[i].name].length;
          j++
        ) {
          this.postRestAPIServiceDMax(
            multiNameService[testingmodeDivider[i].name][j],
            val.isOn,
          );
        }
      }
    }

    // this.callGetAPI();
    this.props.ServiceUpdate(service + val.isOn.toString());
  };

  toggleSideMenu = () => {
    // console.log('toggleSideMenu');
    this.setState({
      isOpenVal: !this.state.isOpenVal,
    });
  };

  onChangeSideMenu = (isOpenCheck) => {
    // console.log('onChangeSideMenu isOpenCheck = ', isOpenCheck);
    if (isOpenCheck === false) {
      this.setState({
        isOpenVal: !this.state.isOpenVal,
      });
    }
  };

  handleDoubletap = () => {
    // console.log('handleDoubletap ');
    const now = Date.now();
    const DOUBLE_PRESS_DELAY = 300;
    if (lastTap && now - lastTap < DOUBLE_PRESS_DELAY) {
      console.log('handleDoubletap proper');
      this.props.openModal('settings_dmax');
    } else {
      lastTap = now;
    }
  };

  setDefaultBeforeTransition = () => {
    console.log('setDefaultBeforeTransition HomeScreenDemo');
    for (i = 0; i < testingmodeDivider.length; i++) {
      for (
        j = 0;
        j < multiNameService[testingmodeDivider[i].name].length;
        j++
      ) {
        this.postRestAPIServiceDMin(
          multiNameService[testingmodeDivider[i].name][j],
          false,
        );
        this.postRestAPIServiceDMax(
          multiNameService[testingmodeDivider[i].name][j],
          false,
        );
      }
    }
    // this.callGetAPI();
    ServicesConst.HomeModeValue = false;
    ServicesConst.OfficeModeValue = false;
    var newState = ServicesConst.HomeModeService.map((obj) => {
      if (obj.value === true) {
        this.props.ServiceUpdate(obj.name + 'false');
      }
      return {...obj, value: false, isBlocked: false};
    });
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) => {
      if (obj.value === true) {
        this.props.ServiceUpdate(obj.name + 'false');
      }
      return {...obj, value: false, isBlocked: false};
    });
    ServicesConst.OfficeModeService = newState;
  };

  render() {
    // console.log('Home screen.js  before ServicesConst = ', ServicesConst);
    // console.log('ServicesConst after= ', ServicesConst);
    this.updateHomeScreenDemoView();

    var arr1 = ServicesConst.HomeModeService;
    var arr2 = ServicesConst.OfficeModeService;
    var CustomModeService = [...arr1, ...arr2];

    // console.log('CustomModeService = ', CustomModeService);

    CustomModeService = CustomModeService.map((obj) => {
      var wentinside = false;
      testingmodeDivider.forEach((value, index, array) => {
        if (obj.name === value.name) {
          obj.mode = value.mode;
          wentinside = true;
        }
      });
      if (obj.mode && wentinside) {
        return obj;
      } else {
        obj.mode = '';
        return obj;
      }
    });

    // console.log('CustomModeService = ', CustomModeService);
    var homeModeObj = CustomModeService.find((obj) => obj.mode === 'HOME MODE');
    var officeModeObj = CustomModeService.find(
      (obj) => obj.mode === 'OFFICE MODE',
    );
    var leisureModeObj = CustomModeService.find(
      (obj) => obj.mode === 'LEISURE MODE',
    );

    // console.log('homeModeObj = ', homeModeObj);
    // console.log('officeModeObj = ', officeModeObj);
    // console.log('leisureModeObj = ', leisureModeObj);

    var radio_props = [];
    var initialVal = 0;
    var serviceSelMode = '';

    for (i = 0; i < testingmodeDivider.length; i++) {
      if (testingmodeDivider[i].mode === this.state.modalName) {
        serviceSelMode = testingmodeDivider[i].name;
      }
    }
    console.log('serviceSelMode = ', serviceSelMode);

    var tempAllSer = [...CustomModeService];
    tempAllSer = tempAllSer.filter((obj) => {
      if (obj.mode === this.state.modalName || obj.mode === '') {
        return obj;
      }
    });
    // console.log('tempAllSer = ', tempAllSer);
    for (i = 0; i < tempAllSer.length; i++) {
      radio_props.push({
        label: tempAllSer[i].name,
        value: tempAllSer[i].name,
      });

      if (serviceSelMode === tempAllSer[i].name) {
        initialVal = i;
      }
    }
    // console.log('radio_props = ', radio_props);

    const MenuComponent = (
      <View
        style={{
          flex: 1,
          backgroundColor: '#F0F0F0',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-around',
            // alignItems: 'center',
            marginTop: Math.round(screenHeight * 0.05),
            width: Math.round(screenWidth * 0.65),
          }}>
          <View
            style={{
              marginVertical: Math.round(screenHeight * 0.03),
              alignSelf: 'center',
            }}>
            <Text
              style={{
                fontSize: 36,
                color: 'black',
              }}></Text>
          </View>

          <TouchableOpacity
            onPress={() => {
              this.setDefaultBeforeTransition();
              setTimeout(() => {
                logoConst['isDemoMode'] = false;
                this.props.navigation.navigate('Home', {isDemo: false});
              }, 250);
            }}>
            <View
              style={{
                backgroundColor: '#DCDCDC',
                marginTop: Math.round(screenHeight * 0.01),
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                source={logoConst['demo_exit']}
                style={[styles.logo_image_sidemenu]}
              />
              <Text
                style={{
                  fontSize: 20,
                  color: 'black',
                  alignSelf: 'center',
                }}>
                Exit Demo Mode
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginTop: Math.round(screenHeight * 0.01),
            width: Math.round(screenWidth * 0.65),
            height: Math.round(screenHeight * 0.75),
          }}>
          <View
            style={{
              width: Math.round(screenWidth * 0.65),
              height: Math.round(screenHeight * 0.35),
            }}></View>
          <TouchableOpacity onPress={this.handleDoubletap}>
            <View
              style={{
                // borderColor: 'black',
                // borderWidth: 1,
                width: Math.round(screenWidth * 0.65),
                height: Math.round(screenHeight * 0.35),
              }}></View>
          </TouchableOpacity>
        </View>
      </View>
    );

    return (
      <SideMenu
        onChange={(isOpen) => this.onChangeSideMenu(isOpen)}
        menuPosition={'right'}
        isOpen={this.state.isOpenVal}
        menu={MenuComponent}>
        <View style={styles.container}>
          <Modal isVisible={this.state.isModalVisible}>
            <View
              onStartShouldSetResponder={(evt) => true}
              onResponderGrant={this.handlePressOut}
              style={styles.settingModalView}>
              <View
                onStartShouldSetResponder={(evt) => true}
                onResponderGrant={this.handlePressOutSub}
                style={styles.settingModalSubView}>
                <View style={{marginTop: Math.round(screenHeight * 0.02)}}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      alignSelf: 'center',
                    }}>
                    {this.state.modalName}
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      marginTop: Math.round(screenHeight * 0.01),
                    }}>
                    Please Select or Change the Service
                  </Text>
                </View>

                <GestureHandlerScrollView
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  style={{
                    // borderColor: 'black',
                    // borderWidth: 1,
                    width: Math.round(screenWidth * 0.8),
                    height: Math.round(screenHeight * 0.58),
                    marginVertical: Math.round(screenHeight * 0.01),
                    paddingVertical: Math.round(screenHeight * 0.01),
                  }}>
                  <RadioForm
                    radio_props={radio_props}
                    initial={initialVal}
                    animation={true}
                    buttonSize={18} // size of radiobutton
                    buttonOuterSize={27}
                    selectedButtonColor={'#2196f3'}
                    selectedLabelColor={'#2196f3'}
                    labelStyle={{
                      fontSize: 18,
                      color: 'black',
                      marginHorizontal: Math.round(screenWidth * 0.05),
                    }}
                    onPress={(value) => {
                      this.setState({
                        modeUpdate: value + '::' + this.state.modalName,
                      });
                    }}></RadioForm>
                </GestureHandlerScrollView>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginVertical: Math.round(screenHeight * 0.02),
                  }}>
                  <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Cancel
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.toggleModal('OK')}>
                    <View style={styles.ModalButtonView}>
                      <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                        Save
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <AnimatedContainer
            style={{
              transform: [{scale: this.state.scale}],
              opactiy: this.state.opacity,
            }}>
            <SafeAreaView>
              <View style={styles.headingView}>
                <View style={styles.headingSubView}>
                  <View
                    style={{
                      height: Math.round(screenHeight * 0.04),
                      width: Math.round(screenHeight * 0.04),
                      backgroundColor: 'black',
                    }}></View>
                  <Image
                    source={require('../../Icons/rezrvHeading.png')}
                    style={styles.rezrv_heading_image}
                  />
                  {/* <View
                  style={{
                    height: Math.round(screenHeight * 0.04),
                    width: Math.round(screenHeight * 0.04),
                    backgroundColor: 'black',
                  }}></View> */}
                  {/* <TouchableOpacity
                    onPress={() => this.props.openModal('settings')}> */}
                  <TouchableOpacity onPress={this.toggleSideMenu}>
                    <Image
                      source={require('../../Icons/settings.png')}
                      style={styles.rezrv_heading_image_side}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.refresh}
                  />
                }
                style={styles.scrollView}>
                <TouchableOpacity
                  activeOpacity={0.7}
                  onLongPress={() => {
                    if (!homeModeObj.value) {
                      this.openModalforSel('HOME MODE');
                    }
                  }}>
                  <View style={[styles.box, styles.boxHome]}>
                    <View style={styles.seperateModeContainer}>
                      <Text style={styles.ModeText}>HOME MODE</Text>
                      {/* {homeModeObj.value ? (
                      <Text style={styles.ModeText}>HOME MODE</Text>
                    ) : (
                      <TouchableOpacity
                        onLongPress={() => {
                          this.openModalforSel('HOME MODE');
                        }}>
                        <Text style={styles.ModeText}>HOME MODE</Text>
                      </TouchableOpacity>
                    )} */}
                    </View>

                    <View style={[styles.containerInner]}>
                      <View style={styles.containerInnerBox}>
                        <View style={styles.eachIconBoxNew}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {homeModeObj.isActive ? (
                              <View
                                style={[
                                  styles.circleDot,
                                  {backgroundColor: 'green'},
                                ]}></View>
                            ) : (
                              <View style={[styles.circleDot]}></View>
                            )}
                            <Image
                              source={logoConst[homeModeObj.name]}
                              style={styles.imageView}></Image>
                          </View>
                          <ToggleSwitch
                            isOn={homeModeObj.value}
                            onColor="green"
                            offColor="grey"
                            labelStyle={{color: 'black', fontWeight: '900'}}
                            size="large"
                            onToggle={(isOn) =>
                              this.toggleSwitchpress({isOn}, homeModeObj.name)
                            }
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.7}
                  onLongPress={() => {
                    if (!officeModeObj.value) {
                      this.openModalforSel('OFFICE MODE');
                    }
                  }}>
                  <View style={[styles.box, styles.boxHome]}>
                    <View style={styles.seperateModeContainer}>
                      <Text style={styles.ModeText}>OFFICE MODE</Text>
                      {/* {officeModeObj.value ? (
                      <Text style={styles.ModeText}>OFFICE MODE</Text>
                    ) : (
                      <TouchableOpacity
                        onLongPress={() => {
                          this.openModalforSel('OFFICE MODE');
                        }}>
                        <Text style={styles.ModeText}>OFFICE MODE</Text>
                      </TouchableOpacity>
                    )} */}
                    </View>

                    <View style={[styles.containerInner]}>
                      <View style={styles.containerInnerBox}>
                        <View style={styles.eachIconBoxNew}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {officeModeObj.isActive ? (
                              <View
                                style={[
                                  styles.circleDot,
                                  {backgroundColor: 'green'},
                                ]}></View>
                            ) : (
                              <View style={[styles.circleDot]}></View>
                            )}
                            <Image
                              source={logoConst[officeModeObj.name]}
                              style={styles.imageView}></Image>
                          </View>
                          <ToggleSwitch
                            isOn={officeModeObj.value}
                            onColor="green"
                            offColor="grey"
                            labelStyle={{color: 'black', fontWeight: '900'}}
                            size="large"
                            onToggle={(isOn) =>
                              this.toggleSwitchpress({isOn}, officeModeObj.name)
                            }
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.7}
                  onLongPress={() => {
                    if (!leisureModeObj.value) {
                      this.openModalforSel('LEISURE MODE');
                    }
                  }}>
                  <View style={[styles.box, styles.boxHome]}>
                    <View style={styles.seperateModeContainer}>
                      <Text style={styles.ModeText}>LEISURE MODE</Text>
                      {/* {leisureModeObj.value ? (
                      <Text style={styles.ModeText}>LEISURE MODE</Text>
                    ) : (
                      <TouchableOpacity
                        onLongPress={() => {
                          this.openModalforSel('LEISURE MODE');
                        }}>
                        <Text style={styles.ModeText}>LEISURE MODE</Text>
                      </TouchableOpacity>
                    )} */}
                    </View>

                    <View style={[styles.containerInner]}>
                      <View style={styles.containerInnerBox}>
                        <View style={styles.eachIconBoxNew}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            {leisureModeObj.isActive ? (
                              <View
                                style={[
                                  styles.circleDot,
                                  {backgroundColor: 'green'},
                                ]}></View>
                            ) : (
                              <View style={[styles.circleDot]}></View>
                            )}
                            <Image
                              source={logoConst[leisureModeObj.name]}
                              style={styles.imageView}></Image>
                          </View>
                          <ToggleSwitch
                            isOn={leisureModeObj.value}
                            onColor="green"
                            offColor="grey"
                            labelStyle={{color: 'black', fontWeight: '900'}}
                            size="large"
                            onToggle={(isOn) =>
                              this.toggleSwitchpress(
                                {isOn},
                                leisureModeObj.name,
                              )
                            }
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </ScrollView>

              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: Math.round(screenWidth * 0.98),
                }}>
                <View style={styles.footerView}>
                  <Image
                    source={require('../../Icons/rezrv.png')}
                    style={styles.rezrv_logo_image}
                  />
                  <View style={styles.footerTextView}>
                    <Text style={styles.footerSubText}> POWERED BY</Text>
                    <Image
                      source={require('../../Icons/tantiv4.png')}
                      style={styles.tantiv4_logo_image}
                    />
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </AnimatedContainer>
        </View>
      </SideMenu>
    );
  }
}

const BelowMainContainer = styled.View`
  flex: 1;
  background-color: #000000;
  justify-content: space-around;
`;
const AnimatedContainer = Animated.createAnimatedComponent(BelowMainContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#F0F0F0',
  },
  headingView: {
    marginHorizontal: Math.round(screenWidth * 0.04),
  },
  headingSubView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  rezrv_heading_image: {
    width: Math.round(screenWidth * 0.45),
    height: Math.round(screenHeight * 0.05),
    resizeMode: 'contain',
    // borderColor: 'white',
    // borderWidth: 1,
    // marginRight: Math.round(screenWidth * 0.14),
  },
  rezrv_heading_image_side: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
    // borderColor: 'white',
    // borderWidth: 1,
  },
  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: Math.round(screenHeight * 0.7),
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  ModalInput: {
    width: Math.round(screenWidth * 0.25),
    height: Math.round(screenHeight * 0.05),
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  scrollView: {
    width: screenWidth,
  },
  footerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: Math.round(screenHeight * 0.01),
    width: Math.round(screenWidth * 0.98),
  },
  footerTextView: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerSubText: {
    color: '#F0F0F0',
    fontSize: 12,
  },
  seperateModeContainer: {
    flexDirection: 'row',
    marginHorizontal: Math.round(screenWidth * 0.05),
    marginTop: Math.round(screenHeight * 0.04),
    // justifyContent: 'space-between',
    justifyContent: 'center',
    // borderWidth: 1,
    // borderColor: 'black',
    // backgroundColor: '#339CFF',
  },
  ModeText: {
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'italic',
    alignSelf: 'center',
    color: 'black',
    // color: '#339CFF',
  },
  box: {
    height: mainBoxHeight,
    marginHorizontal: Math.round(screenWidth * 0.05),
  },
  boxHome: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  boxOffice: {
    // backgroundColor: '#E0E0E0',
    backgroundColor: '#F0F0F0',
    borderWidth: 1,
    marginBottom: Math.round(screenHeight * 0.01),
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },

  logo_image: {
    width: Math.round(screenHeight * 0.13),
    height: Math.round(screenHeight * 0.13),
    resizeMode: 'contain',
  },
  rezrv_logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },
  tantiv4_logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },

  containerInner: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerInnerbox: {
    width: Math.round(screenWidth * 0.4),
    height: Math.round(mainBoxHeight * 0.5),
    borderRadius: Math.round(mainBoxHeight * 0.05),
  },
  boxInner: {
    borderWidth: 1,
    marginLeft: Math.round(screenWidth * 0.015),
  },
  eachIconBox: {
    flex: 1,
    margin: Math.round(screenWidth * 0.01),
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  IconBoxMore: {
    flex: 1,
    margin: Math.round(screenWidth * 0.01),
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MoreText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  scrollInnerContainer: {
    flexDirection: 'row',
    marginHorizontal: Math.round(screenWidth * 0.03),
  },

  circleDot: {
    height: 10,
    width: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: Math.round(screenWidth * 0.01),
  },

  circleDotBackGroud: {
    backgroundColor: 'black',
  },

  circleDotCustom: {
    height: 10,
    width: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'white',
    marginHorizontal: Math.round(screenWidth * 0.01),
  },

  circleDotCustomBackGroud: {
    backgroundColor: 'white',
  },

  circleDotEmpty: {
    position: 'absolute',
    top: Math.round(screenHeight * 0.01),
    right: Math.round(screenWidth * 0.01),
    height: 14,
    width: 14,
    borderRadius: 7,
    marginHorizontal: Math.round(screenWidth * 0.01),
    borderWidth: 1,
  },

  viewblockedOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo_image_blockedOverlay: {
    width: Math.round(screenHeight * 0.06),
    height: Math.round(screenHeight * 0.06),
    resizeMode: 'contain',
  },

  containerInnerBox: {
    alignItems: 'center',
    width: Math.round(screenWidth * 0.9),
    height: Math.round(mainBoxHeight * 0.2),
    marginBottom: Math.round(mainBoxHeight * 0.3),
  },
  eachIconBoxNew: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: Math.round(screenWidth * 0.8),
  },

  imageView: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.12),
    marginLeft: Math.round(screenWidth * 0.04),
    resizeMode: 'contain',
  },
  logo_image_sidemenu: {
    width: Math.round(screenHeight * 0.035),
    height: Math.round(screenHeight * 0.035),
    resizeMode: 'contain',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreenDemo);

import React from 'react';
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Animated,
  Image,
} from 'react-native';

import styled from 'styled-components';
import {connect} from 'react-redux';

import ClosePng from '../../Icons/close.png';

import HomeMode from './HomeMode';
import OfficeMode from './OfficeMode';

const {ServicesConst, logoConst} = require('../../constant/Model');

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeMenu: () =>
      dispatch({
        type: 'CLOSE_MENU',
      }),
  };
}

class Menu extends React.PureComponent {
  state = {
    top: new Animated.Value(screenHeight),
    Title: '',
  };

  componentDidMount() {
    // Animated.spring(this.state.top, {
    //   toValue: 0
    // }).start();

    this.toggleMenu();
  }

  componentDidUpdate() {
    this.toggleMenu();
  }

  toggleMenu = () => {
    // console.log('toggleMenu Menu this.props.action = ', this.props);
    if (this.props.action == 'openMenu') {
      this.setState({
        Title: this.props.name,
      });
      Animated.spring(this.state.top, {
        toValue: 54,
      }).start();
    }

    if (this.props.action == 'closeMenu') {
      Animated.spring(this.state.top, {
        toValue: screenHeight,
      }).start();
    }

    // Animated.spring(this.state.top, {
    //   toValue: screenHeight
    // }).start();
  };

  render() {
    // console.log('Menu.js ServicesConst = ', ServicesConst);

    return (
      <AnimatedContainer style={{top: this.state.top}}>
        <Cover>
          {/* <ImageCustom
            source={require('../assets/DarkMode0002-0600.png')}></ImageCustom> */}
          <Title>{this.state.Title.toLocaleUpperCase()}</Title>
        </Cover>
        <TouchableOpacity
          onPress={this.props.closeMenu}
          style={{
            position: 'absolute',
            top: 120,
            left: '50%',
            marginLeft: -22,
            zIndex: 1,
          }}>
          <CloseView>
            <Image source={ClosePng} style={styles.logo_image} />
          </CloseView>
        </TouchableOpacity>
        <ScrollView>
          <Content>
            {(() => {
              if (this.state.Title == 'Home Mode') {
                return <HomeMode></HomeMode>;
              } else if (this.state.Title == 'Office Mode') {
                return <OfficeMode></OfficeMode>;
              }
            })()}
          </Content>
        </ScrollView>
      </AnimatedContainer>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);

const styles = StyleSheet.create({
  logo_image: {
    width: Math.round(screenHeight * 0.04),
    height: Math.round(screenHeight * 0.04),
    resizeMode: 'contain',
  },
});

const Container = styled.View`
  position: absolute;
  background: #f0f3f5;
  width: ${screenWidth};
  align-self: center;
  height: 100%;
  z-index: 100;
  border-radius: 10px;
  overflow: hidden;
`;

const AnimatedContainer = Animated.createAnimatedComponent(Container);

const ImageCustom = styled.Image`
  position: absolute;
  width: 100%;
  height: 100%;
`;

const Title = styled.Text`
  color: white;
  font-size: 24px;
  font-weight: 600;
`;

const CloseView = styled.View`
  width: 44px;
  height: 44px;
  border-radius: 22px;
  background: white;
  justify-content: center;
  align-items: center;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.15);
`;

const Cover = styled.View`
  height: 142px;
  background: #101010;
  /* background: #484f79; */
  justify-content: center;
  align-items: center;
`;

const Content = styled.View`
  /* height: ${screenHeight}; */
  /* background: #f0f3f5; */
  background: #ffffff;
  padding: ${Math.round(screenWidth * 0.05)}px;
  margin-bottom: ${Math.round(screenWidth * 0.1)}px;
`;

import React from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

import Modal from 'react-native-modal';

import {connect} from 'react-redux';

import AsyncStorage from '@react-native-community/async-storage';

import ToggleSwitchHandler from './ToggleSwitchHandler';

const {
  ServicesConst,
  logoConst,
  multiNameService,
} = require('../../constant/Model');

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const mainBoxHeight = Math.round(screenHeight * 0.35);

function mapStateToProps(state) {
  return {
    action: state.action,
    name: state.name,
    service: state.service,
    modalName: state.modalName,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeModal: () =>
      dispatch({
        type: 'CLOSE_MODAL',
      }),
    ServiceUpdate: (val) =>
      dispatch({
        type: 'SERVICE_UPDATE',
        service: val,
      }),
  };
}

class ModalHandler extends React.PureComponent {
  state = {
    isModalVisible: false,
    textMsg: '',
    errorMsg: '',
    randomVal: '',
    isOthers: false,
  };

  componentDidMount() {
    this.setToggleMode();
  }

  componentDidUpdate() {
    this.setToggleMode();
  }

  setToggleMode = () => {
    if (this.props.action == 'openModal') {
      this.setState({
        isModalVisible: true,
      });
    }
    if (this.props.action == 'closeModal') {
      this.setState({
        isModalVisible: false,
      });
    }
  };

  toggleModaldmax = (value) => {
    if (value === 'OK') {
      if (this.state.textMsg !== '') {
        var value = parseFloat(this.state.textMsg);
        logoConst.dmaxDemoMode = this.state.textMsg;
        this.setState({
          isModalVisible: !this.state.isModalVisible,
          errorMsg: '',
          textMsg: '',
        });

        (async () => {
          await AsyncStorage.setItem('@dmaxDemo', logoConst['dmaxDemoMode']);
        })();

        this.props.closeModal();
      } else {
        this.setState({
          errorMsg: 'Please enter dmax Value',
          textMsg: '',
        });
      }
    } else {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
        errorMsg: '',
        textMsg: '',
      });
      this.props.closeModal();
    }
  };

  toggleModal = (value) => {
    if (value === 'OK') {
      if (this.state.textMsg !== '') {
        var value = parseFloat(this.state.textMsg);
        logoConst.BandWidth = this.state.textMsg;
        this.setState({
          isModalVisible: !this.state.isModalVisible,
          errorMsg: '',
          textMsg: '',
        });
        this.props.closeModal();
      } else {
        this.setState({
          errorMsg: 'Please enter Bandwidth Value',
          textMsg: '',
        });
      }
    } else {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
        errorMsg: '',
        textMsg: '',
      });
      this.props.closeModal();
    }
  };

  ValidateIPaddress = (ipaddress) => {
    if (
      /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
        ipaddress,
      )
    ) {
      return true;
    }
    return false;
  };

  toggleModalIpAddress = (value) => {
    if (value === 'OK') {
      if (this.state.textMsg !== '') {
        // var value = parseFloat(this.state.textMsg);
        if (this.ValidateIPaddress(this.state.textMsg)) {
          logoConst.URL = 'http://' + this.state.textMsg + ':8008';
          this.setState({
            isModalVisible: !this.state.isModalVisible,
            errorMsg: '',
            textMsg: '',
          });
          this.props.closeModal();
        } else {
          this.setState({
            errorMsg: 'Please enter Valid IP Address',
          });
        }
      } else {
        this.setState({
          errorMsg: 'Please enter IP Address',
          textMsg: '',
        });
      }
    } else {
      this.setState({
        isModalVisible: !this.state.isModalVisible,
        errorMsg: '',
        textMsg: '',
        isOthers: false,
      });
      this.props.closeModal();
    }
  };

  inputText = (text) => {
    this.setState({textMsg: text});
  };

  handlePressOut = () => {
    // console.log('handlePressOut');
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      errorMsg: '',
      textMsg: '',
      isOthers: false,
    });

    this.props.closeModal();
  };

  handlePressOutSub = () => {
    // console.log('handlePressOutSub');
  };

  postRestAPIService = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    if (value === true) {
      data += '&dmax=0.01K';
    } else {
      data += '&dmax=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  toggleBlock = (isBlockedVal, serviceName) => {
    // console.log('toggleBlock isBlockedVal = ', isBlockedVal);
    // console.log('toggleBlock serviceName = ', serviceName);

    var newState = ServicesConst.HomeModeService.map((obj) =>
      obj.name === serviceName ? {...obj, isBlocked: isBlockedVal} : obj,
    );
    ServicesConst.HomeModeService = newState;

    newState = ServicesConst.OfficeModeService.map((obj) =>
      obj.name === serviceName ? {...obj, isBlocked: isBlockedVal} : obj,
    );
    ServicesConst.OfficeModeService = newState;

    // console.log('toggleBlock ServicesConst = ', ServicesConst);

    this.setState({randomVal: serviceName + isBlockedVal.toString()});

    for (i = 0; i < multiNameService[serviceName].length; i++) {
      this.postRestAPIService(multiNameService[serviceName][i], isBlockedVal);
    }
  };

  render() {
    console.log('ModalHandler this.props.modalName = ', this.props.modalName);

    // if (this.props.modalName === undefined) {
    //   return <Modal isVisible={false}></Modal>;
    // }
    if (this.props.modalName === 'settings_IPset') {
      var IPAddress = logoConst['URL'];
      IPAddress = IPAddress.split('://')[1];
      IPAddress = IPAddress.split(':')[0];

      var radio_props = [
        {label: '10.3.12.1', value: '10.3.12.1'},
        {label: '192.168.1.1', value: '192.168.1.1'},
        {label: 'Others', value: 'Others'},
      ];

      return (
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              // onResponderRelease={this.handlePressOutSub}
              onResponderGrant={this.handlePressOutSub}
              style={[
                styles.settingModalSubView,
                {height: Math.round(screenHeight * 0.4)},
              ]}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Current Default IP is : {IPAddress}
              </Text>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Set IP Address
              </Text>
              <RadioForm
                radio_props={radio_props}
                initial={0}
                animation={true}
                buttonSize={18} // size of radiobutton
                buttonOuterSize={27}
                selectedButtonColor={'#2196f3'}
                selectedLabelColor={'#2196f3'}
                labelStyle={{
                  fontSize: 18,
                  color: 'black',
                  marginHorizontal: Math.round(screenWidth * 0.05),
                }}
                onPress={(value) => {
                  if (value === 'Others') {
                    this.setState({textMsg: ''});
                    this.setState({isOthers: true});
                  } else {
                    this.setState({isOthers: false});
                    this.setState({textMsg: value});
                    this.setState({errorMsg: ''});
                  }
                }}></RadioForm>

              {this.state.isOthers ? (
                <TextInput
                  style={styles.ModalInput}
                  underlineColorAndroid="transparent"
                  placeholder="IP Address"
                  placeholderTextColor="lightgrey"
                  autoCapitalize="none"
                  textAlign={'center'}
                  numeric
                  keyboardType={'numeric'}
                  onChangeText={this.inputText}
                />
              ) : (
                <View style={{display: 'none'}}></View>
              )}

              {this.state.errorMsg !== '' ? (
                <Text style={{color: 'red'}}>{this.state.errorMsg}</Text>
              ) : (
                <Text style={{display: 'none'}}></Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => this.toggleModalIpAddress('cancel')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                      Cancel
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.toggleModalIpAddress('OK')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>Save</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      );
    } else if (this.props.modalName === 'settings') {
      return (
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              // onResponderRelease={this.handlePressOutSub}
              onResponderGrant={this.handlePressOutSub}
              style={styles.settingModalSubView}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Current Bandwidth : {logoConst.BandWidth} Mbps
              </Text>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Set Bandwidth
              </Text>
              <TextInput
                style={styles.ModalInput}
                underlineColorAndroid="transparent"
                placeholder="in Mbps"
                placeholderTextColor="lightgrey"
                autoCapitalize="none"
                textAlign={'center'}
                numeric
                keyboardType={'numeric'}
                onChangeText={this.inputText}
              />

              {this.state.errorMsg !== '' ? (
                <Text style={{color: 'red'}}>{this.state.errorMsg}</Text>
              ) : (
                <Text style={{display: 'none'}}></Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                      Cancel
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleModal('OK')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>OK</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      );
    } else if (this.props.modalName === 'settings_dmax') {
      return (
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              // onResponderRelease={this.handlePressOutSub}
              onResponderGrant={this.handlePressOutSub}
              style={styles.settingModalSubView}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Current DMAX : {logoConst.dmaxDemoMode} K
              </Text>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Set New Value
              </Text>
              <TextInput
                style={styles.ModalInput}
                underlineColorAndroid="transparent"
                placeholder="in K"
                placeholderTextColor="lightgrey"
                autoCapitalize="none"
                textAlign={'center'}
                numeric
                keyboardType={'numeric'}
                onChangeText={this.inputText}
              />

              {this.state.errorMsg !== '' ? (
                <Text style={{color: 'red'}}>{this.state.errorMsg}</Text>
              ) : (
                <Text style={{display: 'none'}}></Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => this.toggleModaldmax('cancel')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                      Cancel
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleModaldmax('OK')}>
                  <View style={styles.ModalButtonView}>
                    <Text style={{fontSize: 16, fontWeight: 'bold'}}>OK</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      );
    } else {
      var isBlockedService = undefined;
      var imageCtrl;
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.name === this.props.modalName) {
          isBlockedService = value.isBlocked;
        }
      });

      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.name === this.props.modalName) {
          isBlockedService = value.isBlocked;
        }
      });

      if (isBlockedService) {
        imageCtrl = logoConst.blocked;
      } else {
        imageCtrl = logoConst.unblocked;
      }

      return (
        <Modal isVisible={this.state.isModalVisible}>
          <View
            onStartShouldSetResponder={(evt) => true}
            // onResponderRelease={this.handlePressOut}
            onResponderGrant={this.handlePressOut}
            style={styles.settingModalView}>
            <View
              onStartShouldSetResponder={(evt) => true}
              // onResponderRelease={this.handlePressOutSub}
              onResponderGrant={this.handlePressOutSub}
              style={styles.settingModalSubViewLogos}>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>
                Parental Control
              </Text>
              {isBlockedService ? (
                <Text style={{fontSize: 22, fontWeight: 'bold', color: 'red'}}>
                  Blocked
                </Text>
              ) : (
                <Text
                  style={{fontSize: 22, fontWeight: 'bold', color: 'green'}}>
                  Unblocked
                </Text>
              )}
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  width: Math.round(screenWidth * 0.7),
                  paddingLeft: Math.round(screenWidth * 0.07),
                }}>
                <Image
                  source={logoConst[this.props.modalName]}
                  style={styles.logo_image}
                />
                <TouchableOpacity
                  onPress={() =>
                    this.toggleBlock(!isBlockedService, this.props.modalName)
                  }>
                  <Image source={imageCtrl} style={styles.logo_image} />
                </TouchableOpacity>
              </View>

              <TouchableOpacity onPress={() => this.toggleModal('cancel')}>
                <View style={styles.ModalButtonView}>
                  <Text style={{fontSize: 16, fontWeight: 'bold'}}>Close</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      );
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalHandler);

const styles = StyleSheet.create({
  settingModalView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  settingModalSubView: {
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  ModalButtonView: {
    height: Math.round(screenHeight * 0.05),
    width: Math.round(screenWidth * 0.3),
    marginHorizontal: Math.round(screenWidth * 0.03),
    backgroundColor: '#05a9e1',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 4,
  },
  ModalInput: {
    width: Math.round(screenWidth * 0.4),
    height: Math.round(screenHeight * 0.05),
    borderColor: 'black',
    borderWidth: 1,
  },

  settingModalSubViewLogos: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: mainBoxHeight,
    width: screenWidth - 2 * Math.round(screenWidth * 0.07),
    marginHorizontal: Math.round(screenWidth * 0.07),
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    marginTop: Math.round(screenHeight * 0.03),
    borderRadius: Math.round(screenHeight * 0.03),
  },
  logo_image: {
    width: Math.round(screenHeight * 0.12),
    height: Math.round(screenHeight * 0.12),
    resizeMode: 'contain',
  },
});

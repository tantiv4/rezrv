import React from 'react';

import {connect} from 'react-redux';

import ToggleSwitch from 'toggle-switch-react-native';

const {
  ServicesConst,
  logoConst,
  multiNameService,
} = require('../../constant/Model');

function mapStateToProps(state) {
  return {action: state.action, name: state.name, service: state.service};
}

function mapDispatchToProps(dispatch) {
  return {
    openMenu: (val) =>
      dispatch({
        type: 'OPEN_MENU',
        mode: val,
      }),
    ServiceUpdate: (val) =>
      dispatch({
        type: 'SERVICE_UPDATE',
        service: val,
      }),
  };
}

class ToggleSwitchHandler extends React.PureComponent {
  postRestAPIService = (serviceName, value) => {
    var data = 'rule=' + serviceName;
    var valueBandWidth = (parseFloat(logoConst.BandWidth) * 0.125).toString();
    if (value === true) {
      // data += '&dmin=' + logoConst.BandWidth + 'M&umin=2M';
      data += '&dmin=' + valueBandWidth + 'M&umin=2M';
    } else {
      data += '&dmin=0&umin=0';
    }

    console.log('data = ', data);

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener('readystatechange', function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });

    xhr.open('POST', logoConst.URL + '/rest/r');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.send(data);
  };

  toggleSwitchpress = (val, serviceName, serviceMode) => {
    console.log('ToggleSwitchHandler val = ', val.isOn);
    console.log('ToggleSwitchHandler serviceName = ', serviceName);
    console.log('ToggleSwitchHandler serviceMode = ', serviceMode);

    if (serviceName !== 'HomeMode' && serviceName !== 'OfficeMode') {
      var newState = ServicesConst.HomeModeService.map((obj) =>
        obj.name === serviceName ? {...obj, value: val.isOn} : obj,
      );
      ServicesConst.HomeModeService = newState;

      newState = ServicesConst.OfficeModeService.map((obj) =>
        obj.name === serviceName ? {...obj, value: val.isOn} : obj,
      );
      ServicesConst.OfficeModeService = newState;

      // console.log('ToggleSwitchHandler ServicesConst  = ', ServicesConst);

      if (val.isOn === true && serviceMode === 'HomeMode') {
        ServicesConst.HomeModeValue = true;
        ServicesConst.OfficeModeValue = false;

        ServicesConst.OfficeModeService.forEach((value, index, array) => {
          // console.log('OfficeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });

        newState = ServicesConst.OfficeModeService.map((obj) =>
          serviceMode === 'HomeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.OfficeModeService = newState;
      }

      if (val.isOn === true && serviceMode === 'OfficeMode') {
        ServicesConst.HomeModeValue = false;
        ServicesConst.OfficeModeValue = true;

        ServicesConst.HomeModeService.forEach((value, index, array) => {
          // console.log('OfficeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });

        newState = ServicesConst.HomeModeService.map((obj) =>
          serviceMode === 'OfficeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.HomeModeService = newState;
      }

      for (i = 0; i < multiNameService[serviceName].length; i++) {
        this.postRestAPIService(multiNameService[serviceName][i], val.isOn);
      }

      var ifallFalseHomeMode = true;
      var ifallFalseOfficeMode = true;
      ServicesConst.HomeModeService.forEach((value, index, array) => {
        if (value.value === true) {
          ifallFalseHomeMode = false;
        }
      });
      ServicesConst.OfficeModeService.forEach((value, index, array) => {
        if (value.value === true) {
          ifallFalseOfficeMode = false;
        }
      });

      if (ifallFalseHomeMode) {
        ServicesConst.HomeModeValue = false;
      }
      if (ifallFalseOfficeMode) {
        ServicesConst.OfficeModeValue = false;
      }
    }

    if (serviceName === 'HomeMode') {
      if (val.isOn === true) {
        ServicesConst.HomeModeValue = true;
        ServicesConst.OfficeModeValue = false;
        newState = ServicesConst.HomeModeService.map((obj) =>
          serviceMode === 'HomeMode' ? {...obj, value: true} : obj,
        );
        ServicesConst.HomeModeService = newState;

        ServicesConst.HomeModeService.forEach((value, index, array) => {
          // console.log('HomeModeService value [' + index + '] = ', value);
          for (i = 0; i < multiNameService[value.name].length; i++) {
            this.postRestAPIService(multiNameService[value.name][i], true);
          }
        });
        ServicesConst.OfficeModeService.forEach((value, index, array) => {
          // console.log('OfficeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });

        newState = ServicesConst.OfficeModeService.map((obj) =>
          serviceMode === 'HomeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.OfficeModeService = newState;
      } else {
        ServicesConst.HomeModeValue = false;

        ServicesConst.HomeModeService.forEach((value, index, array) => {
          // console.log('HomeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });

        newState = ServicesConst.HomeModeService.map((obj) =>
          serviceMode === 'HomeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.HomeModeService = newState;
      }
    }

    if (serviceName === 'OfficeMode') {
      if (val.isOn === true) {
        ServicesConst.HomeModeValue = false;
        ServicesConst.OfficeModeValue = true;

        newState = ServicesConst.OfficeModeService.map((obj) =>
          serviceMode === 'OfficeMode' ? {...obj, value: true} : obj,
        );
        ServicesConst.OfficeModeService = newState;

        ServicesConst.OfficeModeService.forEach((value, index, array) => {
          // console.log('HomeModeService value [' + index + '] = ', value);
          for (i = 0; i < multiNameService[value.name].length; i++) {
            this.postRestAPIService(multiNameService[value.name][i], true);
          }
        });

        ServicesConst.HomeModeService.forEach((value, index, array) => {
          // console.log('OfficeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });

        newState = ServicesConst.HomeModeService.map((obj) =>
          serviceMode === 'OfficeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.HomeModeService = newState;
      } else {
        ServicesConst.OfficeModeValue = false;

        ServicesConst.OfficeModeService.forEach((value, index, array) => {
          // console.log('HomeModeService value [' + index + '] = ', value);
          if (value.value === true) {
            for (i = 0; i < multiNameService[value.name].length; i++) {
              this.postRestAPIService(multiNameService[value.name][i], false);
            }
          }
        });
        newState = ServicesConst.OfficeModeService.map((obj) =>
          serviceMode === 'OfficeMode' ? {...obj, value: false} : obj,
        );
        ServicesConst.OfficeModeService = newState;
      }
    }

    this.props.ServiceUpdate(serviceName + val.isOn.toString());
  };

  render() {
    return (
      <ToggleSwitch
        isOn={this.props.isOnVal}
        onColor="green"
        offColor="grey"
        labelStyle={{color: 'black', fontWeight: '900'}}
        size="large"
        onToggle={(isOn) =>
          this.toggleSwitchpress(
            {isOn},
            this.props.serviceName,
            this.props.serviceMode,
          )
        }
      />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ToggleSwitchHandler);
